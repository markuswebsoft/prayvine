<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('/landing/index');
});

Route::get('/about', function () {

	$data = array(
		'title' => "PrayVine | About",
		'description' => "test"
	);

    return view('/landing/about', $data);
});

Route::get('/privacy', function () {

	$data = array(
		'title' => "PrayVine | Privacy Policy",
		'description' => ""
	);

    return view('/landing/privacy', $data);
});

Route::get('/terms', function () {

	$data = array(
		'title' => "PrayVine | Terms of Use",
		'description' => ""
	);

    return view('/landing/terms', $data);
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/contact', function(){
	$data = array(
		'title' => 'Pray Vine',
		'page' => 'login'
		);
    return view('/landing/contact', $data);
});

Route::get('/about', function(){
	$data = array(
		'title' => 'Pray Vine',
		'page' => 'login'
		);
    return view('/landing/about', $data);
});

Route::post('/report/{pid}/{cat}', 'FlagController@index');

Route::get('/feed', 'HomeController@feed');

Route::get('/category/{cat}', 'HomeController@cat');

Route::get('/prayers', 'HomeController@prayers');

Route::post('/search/{query}', 'SearchController@index');
Route::post('/searchusers/{query}', 'SearchController@users');

// Post Route Groups
include('posts.php');

// Group Routes
include('groups.php');

// Profile Route Groups
include('profile.php');

// Admin Route Groups
include('admin.php');

// Inbox Route Groups
include('inbox.php');

// Testing Route Groups
include('tests.php');
