<?php

Route::get('/groups', 'GroupsController@index');
Route::get('/groups/{group}', 'GroupsController@getGroup');
Route::get('/edit/group/{group}', 'GroupsController@manageGroup');
Route::get('/members/group/{group}','GroupsController@membersGroup');
Route::get('/admin/group/{group}', 'GroupsController@adminGroup');
Route::get('/inbox/group/{group}', 'GroupsController@inbox');
Route::get('/invites/group/{group}', 'GroupsController@invites');

Route::post('/group/create', 'GroupsController@createGroup');
Route::post('/group/changeProfile', 'GroupsController@changeProfile');
Route::post('/group/changeIcon', 'GroupsController@changeIcon');
Route::post('/group/create/post', 'GroupsController@createPost');
Route::post('/group/join/{group}', 'GroupsController@joinGroup');
Route::post('/group/leave/{group}', 'GroupsController@leaveGroup');
Route::post('/group/post/comment', 'GroupsController@postComment');
Route::post('/group/invite', 'GroupsController@sendInvite');
// AJAX
Route::post('/group/like/{post}/{group}', 'GroupsController@likePost');
Route::post('/group/delete/{id}/{group}', 'GroupsController@deletePost');
Route::post('/group/decide/{decide}/{user}/{group}', 'GroupsController@decideUser');
Route::post('/group/role/{status}/{user}/{group}', 'GroupsController@decideRole');