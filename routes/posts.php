<?php

Route::post('/post', 'PostsController@newPost');
Route::post('/post/comment', 'PostsController@newComment');
Route::post('/post/edit', 'PostsController@editPost');
Route::post('/post/delete/{id}', 'PostsController@deletePost');

Route::post('/like/{post}', 'PostsController@likePost');