<?php

Route::get('/inbox', 'InboxController@index');
Route::get('/outbox', 'InboxController@outbox');
Route::get('/invitations', 'InboxController@invitations');
Route::get('/draft', 'InboxController@draft');

// this will make it easy to boot up a conversation with someone
Route::get('/draft/to/{username}/{subject}', 'InboxController@draftByUsername');

Route::get('/inbox/read/{id}', 'InboxController@readThread');

Route::post('/inbox/send', 'InboxController@sendMessage');
Route::post('/inbox/reply', 'InboxController@sendReply');

Route::post('/inbox/decide/{decision}/{id}', 'InboxController@decideInvitation');

Route::delete('/inbox/delete/convo/{id}', 'InboxController@deleteConversation');
Route::delete('/inbox/delete/message/{id}', 'InboxController@deleteMessage');