<?php

Route::get('/admin', 'AdminController@index');
Route::get('/admin/flags', 'AdminController@flags');
Route::get('/admin/groups', 'AdminController@groups');
Route::get('/admin/stats', 'AdminController@stats');

Route::post('/admin/activate/user/{id}', 'AdminController@activateUser');
Route::post('/admin/block/user/{id}', 'AdminController@blockUser');
Route::post('/admin/suspend/user/{id}', 'AdminController@suspendUser');
Route::post('/admin/delete/user/{id}', 'AdminController@deleteUser');

Route::post('/admin/activate/circle/{id}', 'AdminController@activateCircle');
Route::post('/admin/block/circle/{id}', 'AdminController@blockCircle');
Route::post('/admin/suspend/circle/{id}', 'AdminController@suspendCircle');