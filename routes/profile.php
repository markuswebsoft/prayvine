<?php

Route::post('/profile', 'ProfileController@finish');
Route::post('/profile/check/{user}', 'ProfileController@checkUser');
Route::get('/profile/{user}', 'ProfileController@getProfile');
Route::get('/edit/profile', function(){
	$data = array(
		'title' => 'Pray Vine',
		'page' => 'login',
		'description' => ""
		);
    return view('/profile/edit', $data);
});
Route::post('/edit/profile', 'ProfileController@editProfile');
Route::post('/edit/profile/pic', 'ProfileController@editPic');
Route::post('/edit/profile/background', 'ProfileController@editBackground');
Route::post('/edit/profile/general', 'ProfileController@editGeneral');
Route::post('/edit/profile/password', 'ProfileController@editPassword');