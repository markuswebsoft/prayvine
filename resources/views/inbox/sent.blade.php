@extends('layouts.social')

@section('css')
    @parent
        <style>
            .admin-nav > a{
                padding:20px 0;
                background:white;
                text-align: center;
                font-size: 20px;
                text-decoration: none !important;
            }
            .admin-nav > a.active, .admin-nav > a:hover{
                background: #F5F8FA;
                cursor: pointer;
            }
            select.btn{
                background-image:none !important;
            }
            img.avatar-img{
        width:75px;
        border-radius:50px;
      }
        </style>
@endsection

@section('content')
    @parent
        @include('inbox.nav')
        <div class="row" style="padding:15px;">
            <div class="col-md-12">
                <div class="post-filters">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sent Messages</h3>
                        </div>
                        <div class="panel-body">
                                    @if(session('success') !== null)
                                        <div class="alert alert-success">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                            <table class="table socialite">
                                <thead>
                                    <tr>
                                        <th>Sent To</th>
                                        <th>Message</th>
                                        <th class="text-center">Delivered</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $convos = \App\InboxConvos::where('author', Auth::user()->id)->orderBy('id', 'DESC')->get(); 
                                        foreach ($convos as $convo) {
                                        $user = App\User::where('id', $convo->to)->first();
                                        $profile = \App\Profiles::where('user','=',$convo->to)->first(); 
                                    ?>
                                        <tr class="@if($convo->read == 0)unread @endif">
                                            <td><img src="/uploads/pics/{{ $profile->pic }}" class="avatar-img">
                                            <br> <a href="{{ URL('/profile/' . $profile->username) }}">{{ $user->name }}</a></td>
                                            <td>{{ $convo->content }}</td>
                                            <td class="text-center">{{ convertHTMLTime($convo->date) }}</td>
                                            <td class="text-right"><button class="btn btn-danger delete" message-id="{{ $convo->id }}" title="Delete Conversation"><i class="fa fa-times"></i></button></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('js')
    @parent
        <script> var SessionToken = "{{ csrf_token() }}"; </script>
        <script src="{{ URL::asset('js/admin.js') }}"></script>
@endsection