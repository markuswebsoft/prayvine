@extends('layouts.social')

@section('css')
    @parent
        <style>
            .admin-nav > a{
                padding:20px 0;
                background:white;
                text-align: center;
                font-size: 20px;
                text-decoration: none !important;
            }
            .admin-nav > a.active, .admin-nav > a:hover{
                background: #F5F8FA;
                cursor: pointer;
            }
            select.btn{
                background-image:none !important;
            }
                  img.avatar-img{
        width:75px;
        border-radius:50px;
      }
        </style>
@endsection

@section('content')
    @parent
        @include('inbox.nav')
        <div class="row" style="padding:15px;">
            <div class="col-md-12">
                <div class="post-filters">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Received Messages</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table socialite">
                                <thead>
                                    <tr>
                                        <th>From</th>
                                        <th>Message</th>
                                        <th class="text-center">Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $convos = \App\InboxConvos::where('to', Auth::user()->id)->orderBy('id', 'DESC')->get(); 
                                        foreach ($convos as $convo) {
                                            $user = App\User::where('id', $convo->author)->first();
                                            $profile = \App\Profiles::where('user','=',$convo->author)->first(); 
                                    ?>
                                        <tr id="c-{{ $convo->id }}">
                                            <td><img src="/uploads/pics/{{ $profile->pic }}" class="avatar-img">
                                            <br> <a href="{{ URL('/profile/' . $profile->username) }}" target="_blank">{{ $user->name }}</a></td>
                                            <td><a href="{{ URL('/inbox/read/' . $convo->thread) }}">{{ $convo->content }}</a></td>
                                            <td class="text-center">{{ convertHTMLTime($convo->date) }}</td>
                                            <td class="text-right"><button class="btn btn-danger delete-convo" convo-id="{{ $convo->id }}" title="Delete Conversation"><i class="fa fa-times"></i></button></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('js')
    @parent
        <script> var SessionToken = "{{ csrf_token() }}"; </script>
        <script src="{{ URL::asset('js/ajax.js') }}"></script>
@endsection