@extends('layouts.social')

@section('css')
    @parent
        <style>
            .admin-nav > a{
                padding:20px 0;
                background:white;
                text-align: center;
                font-size: 20px;
                text-decoration: none !important;
            }
            .admin-nav > a.active, .admin-nav > a:hover{
                background: #F5F8FA;
                cursor: pointer;
            }
            select.btn{
                background-image:none !important;
            }
        </style>
@endsection

@section('content')
    @parent
        @include('inbox.nav')
        <div class="row" style="padding:15px;">
            <div class="col-md-12">
                <div class="post-filters">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Received Invitations</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table socialite">
                                <thead>
                                    <tr>
                                        <th>Group</th>
                                        <th>Invited By</th>
                                        <th>Message</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $invites = \App\Invites::where('invitee', Auth::user()->email)->orWhere('user',Auth::user()->id)->where('status', 0)->orderBy('id', 'DESC')->get();
                                        foreach ($invites as $invite) {
                                        $group = \App\Groups::where('id','=',$invite->group)->first();
                                    ?>
                                        <tr id="u-{{ $invite->id }}">
                                            <td>{{ $group->name }}</td>
                                            <td>{{ App\User::where('id', $invite->inviter)->value('name') }}</td>
                                            <td>{{ $invite->note }}</td>
                                            <td>
                                                <button class="btn btn-success invite-decide" invite-id="{{ $invite->id }}" decide="accept" title="Accept Invite"><i class="fa fa-check"></i></button>
                                            <button class="btn btn-danger invite-decide" invite-id="{{ $invite->id }}" decide="deny" title="Deny Invite"><i class="fa fa-times"></i></button></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('js')
    @parent
        <script> var SessionToken = "{{ csrf_token() }}"; </script>
        <script src="{{ URL::asset('js/ajax.js') }}"></script>
@endsection