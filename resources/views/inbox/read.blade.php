@extends('layouts.social')

@section('css')
    @parent
        <style>
            .admin-nav > a{
                padding:20px 0;
                background:white;
                text-align: center;
                font-size: 20px;
                text-decoration: none !important;
            }
            .admin-nav > a.active, .admin-nav > a:hover{
                background: #F5F8FA;
                cursor: pointer;
            }
            select.btn{
                background-image:none !important;
            }
            img.avatar-img{
        width:75px;
        border-radius:50px;
      }
        </style>
@endsection

@section('content')
    @parent
        @include('inbox.nav')
        <div class="row" style="padding:15px">
            <div class="col-md-12">
                <div class="post-filters">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Reply to {{ \App\User::where('id', $thread)->value("name") }}</h3>
                        </div>
                        <div class="panel-body">
                            <form method="POST" action="{{ URL('/inbox/reply') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="POST">   
                                <input type="hidden" name="thread" value="{{ $thread }}">
                                <div class="form-group">
                                    <textarea class="form-control" name="content" placeholder="Your Response.." required=""></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success" value="Send Response">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding:15px;">
            <div class="col-md-12">
                <div class="post-filters">
                    <div class="panel panel-default" id="convo">
                        <div class="panel-heading">
                            <h3 class="panel-title">Private Messages Between You and {{ \App\User::where('id', $thread)->value("name") }}</h3>
                        </div>
                        <div class="panel-body">
                                    <?php
                                        $convos = \App\InboxConvos::where('thread', $thread)->orderBy('id', 'DESC')->get(); 
                                        foreach ($convos as $convo) {
                                            $user = App\User::where('id', $convo->author)->first();
                                            $profile = \App\Profiles::where('user','=',$convo->author)->first(); 
                                    ?>
                                        <div class="row">
                                            <div class="col-md-12 @if($convo->author != Auth::user()->id) text-left @else text-right @endif">
                                                <img src="/uploads/pics/{{ $profile->pic }}" class="avatar-img">
                                            <br>
                                            <a href="{{ URL('/profile/' . $profile->username) }}">{{ $user->name }}</a>
                                            <p>{{ $convo->content }}</p>
                                            {{ convertHTMLTime($convo->date) }}
                                            </div>
                                        </div>
                                    <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('js')
    @parent
        <script> var SessionToken = "{{ csrf_token() }}"; </script>
        <script src="{{ URL::asset('js/admin.js') }}"></script>
@endsection