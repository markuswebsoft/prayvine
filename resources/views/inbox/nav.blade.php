    <div class="row" style="padding:15px;">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body admin-nav">
                    <a href="{{ URL('/inbox') }}" class="col-md-3 col-xs-12 @if($page =='index')active @endif"><i class="fa fa-envelope-o"></i> Inbox</a>
                    <a href="{{ URL('/draft') }}" class="col-md-3 col-xs-12 @if($page =='draft')active @endif"><i class="fa fa-plus"></i> New Message</a>
                    <a href="{{ URL('/outbox') }}" class="col-md-3 col-xs-12 @if($page =='sent')active @endif"><i class="fa fa-paper-plane"></i> Sent</a>
                    <a href="{{ URL('/invitations') }}" class="col-md-3 col-xs-12 @if($page =='invites')active @endif"><i class="fa fa-download"></i> Invitations</a>
                </div>
            </div>
        </div>
    </div>