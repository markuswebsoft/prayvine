@extends('layouts.social')

@section('css')
    @parent
        <style>
            .admin-nav > a{
                padding:20px 0;
                background:white;
                text-align: center;
                font-size: 20px;
                text-decoration: none !important;
            }
            .admin-nav > a.active, .admin-nav > a:hover{
                background: #F5F8FA;
                cursor: pointer;
            }
            select.btn{
                background-image:none !important;
            }
            .selectize-dropdown{
                display: none;
            }
            .big-search-dropdown{
                padding:10px !important;
                margin-bottom: 0 !important;
            }
            .big-search-dropdown a{
                color:#777777;
            }
            .big-search-dropdown:hover{
                background-color: #F5F8FA;
            }
        </style>
@endsection

@section('content')
    @parent
        @include('inbox.nav')
        <div class="row" style="padding:15px;">
            <div class="col-md-12">
                <div class="post-filters">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-paper-plane"></i> Create Message: @if(!empty($subject)) Replying to "{{ $subject }}" @endif</h3>
                        </div>
                        <div class="panel-body">
                            <form method="POST" action="{{ URL('/inbox/send') }}"> 
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="POST">   
                                <div class="form-group" id="usersearch">
                                    <label>Send To</label>
                                    <input type="hidden" name="to" id="to" value="{{ $to }}">
                                    <input type="hidden" name="subject" value="{{ $subject }}">
                                    <input type="text" class="form-control" id="username-search" placeholder="Enter their name" required="" value="{{ $to }}" @if(!empty($to)) disabled="" @endif>
                                    <div class="selectize-dropdown multi form-control" id="users" style="width: 360px; position: relative;; top: 10px; left: 0px;">
                                        <div class="selectize-dropdown-content" style=""></div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top:10px">
                                    <label>Your Message</label>
                                    <textarea class="form-control" name="content" required="">@if(!empty($subject))RE: {{ $subject}} 
                                    @endif</textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success" type="submit">Send Message</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('js')
    @parent
        <script> var SessionToken = "{{ csrf_token() }}"; </script>
        <script src="{{ URL::asset('js/admin.js') }}"></script>
@endsection