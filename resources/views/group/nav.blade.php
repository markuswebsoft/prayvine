		    <div class="list-group list-group-navigation socialite-group">
				<a href="{{ URL('/edit/group/' . $group->id) }}" class="list-group-item @if($page == 'settings')active @endif">
					<div class="list-icon socialite-icon active">
						<i class="fa fa-gear"></i>
					</div>
					<div class="list-text">
						General Group Settings
					</div>
					<div class="clearfix"></div>
				</a>
				<a href="{{ URL('/members/group/' . $group->id) }}" class="list-group-item @if($page == 'members')active @endif">
					<div class="list-icon socialite-icon active">
						<i class="fa fa-users"></i>
					</div>
					<div class="list-text">
						Manage Group Members
					</div>
					<div class="clearfix"></div>
				</a>
				<a href="{{ URL('/inbox/group/' . $group->id) }}" class="list-group-item @if($page == 'inbox')active @endif">
					<div class="list-icon socialite-icon active">
						<i class="fa fa-envelope"></i>
					</div>
					<div class="list-text">
						Group Inbox
					</div>
					<div class="clearfix"></div>
				</a>
				<a href="{{ URL('/invites/group/' . $group->id) }}" class="list-group-item @if($page == 'invites')active @endif">
					<div class="list-icon socialite-icon active">
						<i class="fa fa-paper-plane"></i>
					</div>
					<div class="list-text">
						Manage Invites
					</div>
					<div class="clearfix"></div>
				</a>
			</div>