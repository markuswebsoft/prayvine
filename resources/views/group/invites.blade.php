@extends('layouts.social')

@section('css')
    @parent
    <style>
    	.list-group-item.active, .list-group-item:hover{
    		background:#E6EAEE !important;

    	}
    	.list-group-item{
    		border:none !important;
    	}
    </style>
@endsection

@section('content')
    @parent
    	<div class="row" style="margin-top:10px;">
			<div class="col-md-4">
				<div class="post-filters">
					<div class="panel panel-default">
	<div class="panel-body nopadding">
		<div class="mini-profile">
			<div class="background">
				<div class="avatar-img">
					<img src='/uploads/pics/{{ App\Groups::where("id","=", $group->id)->value("icon") }}' alt="Admin" title="Admin">
				</div>
			</div>
		    <div class="avatar-profile">
		        <div class="avatar-details">
		            <h2 class="avatar-name"><a href="#">{{ App\Groups::where("id","=",$group->id)->value("name") }}</a></h2>
		            <h4 class="avatar-mail">
		            <br><br>
		            </h4>
		        </div>      
		    </div><!-- /avatar-profile -->
		    <!-- menu -->

		    @include('group.nav')

		</div>
	</div><!-- /panel-body -->
</div><!-- /panel -->
	</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							Send Invite
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">
						<form method="post" action="{{ URL('/group/invite') }}">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="POST">
							<input type="hidden" name="group" value="{{ $group->id }}">
							<div class="form-group">
								<input type="text" class="form-control" name="invitees" placeholder="Enter emails separeted by comma">
							</div>
							<div class="form-group">
								<textarea class="form-control" name="message" placeholder="Send a custom message"></textarea>
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-success" value="Send Invites">
								<br><br>
								@if(session('error') !== null)
          							<div class="alert alert-danger">
				                        {{ session('error') }}
				                    </div>
				                @endif
				                @if(session('success') !== null)
          							<div class="alert alert-success">
				                        {{ session('success') }}
				                    </div>
				                @endif
							</div>
						</form>
						</div><!-- /Socialite-form -->
					</div>
				</div>
				<!-- End of first panel -->
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							Invites Sent
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">
						<table class="table socialite">
	                        <thead>
	                            <tr>
	                                <th class="text-center">To</th>
	                                <th width="15%" class="text-center">Status</th>
	                                <th width="15%" class="text-center">Date</th>
	                            </tr>
	                        </thead>		
							<tbody>				
								<?php
	                                $invites = \App\Invites::where('group','=', $group->id)->orderBy('created_at', 'DESC')->get(); 
	                                foreach ($invites as $invite) {
	                                
	                            ?>
	                                <tr>
	                                  	<td>{{ $invite->invitee}}</td>
	                                  	<td>@if($invite->status == 0) Pending @else Accepted @endif</td> 
	                                  	<td>{{ convertHTMLTime($invite->created_at) }}</td>  
	                                </tr>
	                            <?php } ?>
	                            </tbody>
                            </table>
						</div><!-- /Socialite-form -->
					</div>
				</div>
				<!-- End of second panel -->

			</div>
		</div>
@endsection

@section('js')
    @parent
    	<script type="text/javascript">
    	$(function() {
    		$(".pic").change(function(){
    			$("#pic").submit();
    		});
    		$(".roleChange").change(function(){
    			var group = $(this).attr("data-group");
				var user = $(this).attr("data-group-user");
				var status = $(this).val();
				$.ajax({
			        url: "/group/role/" + status + "/" + user + "/" + group,
			        type: "POST"
			    }).done(function(msg){

				});
			});
    	});
    	var myusername = "{{ App\Profiles::where("user","=",Auth::user()->id)->value("username") }}";
    	</script>
@endsection