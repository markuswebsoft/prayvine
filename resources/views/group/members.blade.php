@extends('layouts.social')

@section('css')
    @parent
    <style>
    	.list-group-item.active, .list-group-item:hover{
    		background:#E6EAEE !important;

    	}
    	.list-group-item{
    		border:none !important;
    	}
    </style>
@endsection

@section('content')
    @parent
    	<div class="row" style="margin-top:10px;">
			<div class="col-md-4">
				<div class="post-filters">
					<div class="panel panel-default">
	<div class="panel-body nopadding">
		<div class="mini-profile">
			<div class="background">
				<div class="avatar-img">
					<img src='/uploads/pics/{{ App\Groups::where("id","=", $group->id)->value("icon") }}' alt="Admin" title="Admin">
				</div>
			</div>
		    <div class="avatar-profile">
		        <div class="avatar-details">
		            <h2 class="avatar-name"><a href="#">{{ App\Groups::where("id","=",$group->id)->value("name") }}</a></h2>
		            <h4 class="avatar-mail">
		            <br><br>
		            </h4>
		        </div>      
		    </div><!-- /avatar-profile -->
		    <!-- menu -->

		    @include('group.nav')

		</div>
	</div><!-- /panel-body -->
</div><!-- /panel -->
	</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							Join Requests
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">
						<table class="table socialite">
	                        <thead>
	                            <tr>
	                                <th>ID</th>
	                                <th>Name</th>
	                                <th>Username</th>
	                                <th></th>
	                            </tr>
	                        </thead>		
							<tbody>				
								<?php
	                                $users = \App\GroupMembers::where('group','=', $group->id)->where('status','=', 0)->orderBy('id', 'ASC')->get(); 
	                                foreach ($users as $user) {
	                                $profile = \App\Profiles::where('user','=',$user->user)->first(); 
	                                $account = \App\User::where('id','=',$user->user)->first();
	                            ?>
	                                <tr id="u-{{ $account->id}}">
	                                    <td>{{ $account->id }}</td>
	                                    <td>{{ $account->name }}</td>
	                                    <td>@if(isset($profile->username)) {{ "@" . $profile->username }} @endif</td>
	                                    <td class="text-right">
	                                    <button class="btn btn-success decide-member" data-group="{{ $group->id }}" data-group-user="{{ $account->id }}" data-decision="approve" title="Approve User"><i class="fa fa-check"></i></button> &nbsp; 
	                                    <button class="btn btn-danger decide-member" data-group="{{ $group->id }}" data-group-user="{{ $account->id }}" data-decision="deny" title="Delete User"><i class="fa fa-times"></i></button></td>
	                                </tr>
	                            <?php } ?>
	                            </tbody>
                            </table>
						</div><!-- /Socialite-form -->
					</div>
				</div>
				<!-- End of first panel -->
				<div class="panel panel-default">
				
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							Current Members
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">
						<table class="table socialite">
	                        <thead>
	                            <tr>
	                                <th>ID</th>
	                                <th>Name</th>
	                                <th>Role</th>
	                                <th>Username</th>
	                                <th>Joined</th>
	                                <th></th>
	                            </tr>
	                        </thead>		
							<tbody>				
								<?php
	                                $users = \App\GroupMembers::where('group','=', $group->id)->where('status','>', 0)->orderBy('id', 'ASC')->get(); 
	                                foreach ($users as $user) {
	                                $profile = \App\Profiles::where('user','=',$user->user)->first(); 
	                                $account = \App\User::where('id','=',$user->user)->first();
	                            ?>
	                                <tr id="u-{{ $account->id}}">
	                                    <td>{{ $account->id }}</td>
	                                    <td>{{ $account->name }}</td>
	                                    <td>
		                                    <select @if($account->id == $group->admin) disabled="" @endif user-id="{{ $account->id }}" data-group-user="{{ $account->id }}" data-group="{{ $group->id }}" class="form-control roleChange" >
		                                        <option value="1" @if($user->role==1) selected @endif>Member</option>
		                                        <option value="2" @if($user->role==2) selected @endif>Moderator</option>
		                                        <option value="3" @if($user->role==3 || $account->id == $group->admin) selected @endif>Admin</option>
		                                    </select>
	                                    </td>
	                                    <td>@if(isset($profile->username)) {{ "@" . $profile->username }} @endif</td>
	                                    <td>{{ convertHTMLTime($user->date) }}</td>
	                                    <td>@if($account->id != $group->admin)
	                                    <button class="btn btn-danger decide-member" data-group="{{ $group->id }}" data-group-user="{{ $account->id }}" data-decision="deny" title="Delete User"><i class="fa fa-times"></i></button></td>
	                                    @else
	                                    	ADMIN
	                                    @endif
	                                </tr>
	                            <?php } ?>
	                            </tbody>
                            </table>
						</div><!-- /Socialite-form -->
					</div>
				</div>
				<!-- End of second panel -->

			</div>
		</div>
@endsection

@section('js')
    @parent
    	<script type="text/javascript">
    	$(function() {
    		$(".pic").change(function(){
    			$("#pic").submit();
    		});
    		$(".roleChange").change(function(){
    			var group = $(this).attr("data-group");
				var user = $(this).attr("data-group-user");
				var status = $(this).val();
				$.ajax({
			        url: "/group/role/" + status + "/" + user + "/" + group,
			        type: "POST"
			    }).done(function(msg){

				});
			});
    	});
    	var myusername = "{{ App\Profiles::where("user","=",Auth::user()->id)->value("username") }}";
    	</script>
@endsection