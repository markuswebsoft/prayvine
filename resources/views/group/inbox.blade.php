@extends('layouts.social')

@section('css')
    @parent
    <style>
    	.list-group-item.active, .list-group-item:hover{
    		background:#E6EAEE !important;

    	}
    	.list-group-item{
    		border:none !important;
    	}
    </style>
@endsection

@section('content')
    @parent
    	<div class="row" style="margin-top:10px;">
			<div class="col-md-4">
				<div class="post-filters">
					<div class="panel panel-default">
	<div class="panel-body nopadding">
		<div class="mini-profile">
			<div class="background">
				<div class="avatar-img">
					<img src='/uploads/pics/{{ App\Groups::where("id","=", $group->id)->value("icon") }}' alt="Admin" title="Admin">
				</div>
			</div>
		    <div class="avatar-profile">
		        <div class="avatar-details">
		            <h2 class="avatar-name"><a href="#">{{ App\Groups::where("id","=",$group->id)->value("name") }}</a></h2>
		            <h4 class="avatar-mail">
		            <br><br>
		            </h4>
		        </div>      
		    </div><!-- /avatar-profile -->
		    <!-- menu -->

		    @include('group.nav')

		</div>
	</div><!-- /panel-body -->
</div><!-- /panel -->
	</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							Messages From Moderators
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">
						<table class="table socialite">
	                        <thead>
	                            <tr>
	                                <th width="25%" class="text-center">From</th>
	                                <th>Message</th>
	                                <th width="15%" class="text-center">Date</th>
	                            </tr>
	                        </thead>		
							<tbody>				
								<?php
	                                $messages = \App\Inbox::where('group','=', $group->id)->where('type', 2)->orderBy('date', 'DESC')->get(); 
	                                foreach ($messages as $message) {
	                                $userProfile = \App\Profiles::where('user','=',$message->author)->first(); 
	                                $user = \App\User::where('id',$userProfile->user)->first();
	                            ?>
	                                <tr>
	                                  	<td>{{ $user->name}}</td>
	                                  	<td><b>{{ $message->subject }}</b> <br>{{ $message->content }}</td> 
	                                  	<td>{{ convertHTMLTime($message->date) }}</td>  
	                                </tr>
	                            <?php } ?>
	                            </tbody>
                            </table>
						</div><!-- /Socialite-form -->
					</div>
				</div>
				<!-- End of first panel -->
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							Messages From Members
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">
						<table class="table socialite">
	                        <thead>
	                            <tr>
	                                <th width="25%" class="text-center">From</th>
	                                <th>Message</th>
	                                <th width="15%" class="text-center">Date</th>
	                            </tr>
	                        </thead>		
							<tbody>				
								<?php
	                                $messages = \App\Inbox::where('group','=', $group->id)->where('type', 1)->orderBy('date', 'DESC')->get(); 
	                                foreach ($messages as $message) {
	                                $userProfile = \App\Profiles::where('user','=',$message->author)->first(); 
	                                $user = \App\User::where('id',$userProfile->user)->first();
	                            ?>
	                                <tr>
	                                  	<td>{{ $user->name}}</td>
	                                  	<td><b>{{ $message->subject }}</b> <br>{{ $message->content }}</td> 
	                                  	<td>{{ convertHTMLTime($message->date) }}</td>  
	                                </tr>
	                            <?php } ?>
	                            </tbody>
                            </table>
						</div><!-- /Socialite-form -->
					</div>
				</div>
				<!-- End of second panel -->

			</div>
		</div>
@endsection

@section('js')
    @parent
    	<script type="text/javascript">
    	$(function() {
    		$(".pic").change(function(){
    			$("#pic").submit();
    		});
    		$(".roleChange").change(function(){
    			var group = $(this).attr("data-group");
				var user = $(this).attr("data-group-user");
				var status = $(this).val();
				$.ajax({
			        url: "/group/role/" + status + "/" + user + "/" + group,
			        type: "POST"
			    }).done(function(msg){

				});
			});
    	});
    	var myusername = "{{ App\Profiles::where("user","=",Auth::user()->id)->value("username") }}";
    	</script>
@endsection