@extends('layouts.social')

@section('css')
    @parent
    <style>
    	.list-group-item.active, .list-group-item:hover{
    		background:#E6EAEE !important;

    	}
    	.list-group-item{
    		border:none !important;
    	}
    </style>
@endsection

@section('content')
    @parent
    	<div class="row" style="margin-top:10px;">
			<div class="col-md-4">
				<div class="post-filters">
					<div class="panel panel-default">
	<div class="panel-body nopadding">
		<div class="mini-profile">
			<div class="background">
				<div class="avatar-img">
					<img src='/uploads/pics/{{ $group->icon }}' alt="Admin" title="Admin">
				</div>
			</div>
		    <div class="avatar-profile">
		        <div class="avatar-details">
		            <h2 class="avatar-name"><a href="#">{{ $group->name }}</a></h2>
		            <h4 class="avatar-mail">
		            <br><br>
		            </h4>
		        </div>      
		    </div><!-- /avatar-profile -->
		    <!-- menu -->

		    @include('group.nav')

		</div>
	</div><!-- /panel-body -->
</div><!-- /panel -->
<div class="list-group list-group-navigation socialite-group">
	<a href="#" class="list-group-item">
		<div class="list-icon socialite-icon active">
			<i class="fa fa-photo"></i>
		</div>
		<div class="list-text">
			Update Group Icon
			<div class="text-muted">
				<form id="pic" method="POST" action="{{ URL('/group/changeIcon') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
              		<input type="hidden" name="_method" value="POST">
              		<input type="hidden" name="group" value="{{ $group->id }}">
					<input type="file" name="pic" class="pic">
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</a>
</div>
	</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
				
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							General Settings
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">							
							<form method="POST" action="{{ URL('/group/changeProfile') }}">
									{{ csrf_field() }}
              						<input type="hidden" name="_method" value="POST">
              						<input type="hidden" name="group" value="{{ $group->id }}">
              						@if(session('error') !== null)
              							<div class="alert alert-danger">
					                        {{ session('error') }}
					                    </div>
					                @endif
					                @if(session('success') !== null)
              							<div class="alert alert-success">
					                        {{ session('success') }}
					                    </div>
					                @endif
								<div class="row">
									<div class="col-md-12">
										<fieldset class="form-group required usercheck">
											<label for="username">Name of Prayer Circle</label>
											<input class="form-control" placeholder="Name of Prayer Circle" minlength="6" maxlength="50" name="groupname" type="text" required="" value='@if(old("groupname") !== null){{ old("groupname") }}@else{{ $group->name }}@endif'>
										</fieldset>
									</div>
								</div>
								<fieldset class="form-group">
									<label for="about">Description of Prayer Circle</label>
									<textarea class="form-control" placeholder="Enter description about your prayer circle" name="description" cols="50" rows="10" id="about">@if(old('bio') !== null){{ old('description') }}@else{{ $group->description }} @endif</textarea>
								</fieldset>

								<div class="row">
									<div class="col-md-6">
									<label>Group Status</label>
									<select class="form-control" name="active">
										<option value="1" name="active" @if($group->active==1) selected @endif>Active</option>
										<option value="0" name="active" @if($group->active==0) selected @endif>Closed</option>
									</select>
									</div>
								</div>

									<div class="pull-right">
										<input class="btn btn-success" type="submit" value="Save changes">
									</div>
									<div class="clearfix"></div>
								</form>
							</div><!-- /Socialite-form -->
						</div>
					</div>
					<!-- End of first panel -->
					<!-- End of second panel -->

				</div>
			</div>
@endsection

@section('js')
    @parent
    	<script type="text/javascript">
    	$(function() {
    		$(".pic").change(function(){
    			$("#pic").submit();
    		});
    	});
    	var myusername = "{{ App\Profiles::where("user","=",Auth::user()->id)->value("username") }}";
    	</script>
@endsection