@extends('layouts.social')

@section('css')
    @parent
      <style>
      .like-post, .like-post:hover, .like-post:visited, .like-post:link, .like-post:active{
        text-decoration: none;
      }
      .modal-backdrop.in{
        opacity: 0.4 !important;
      }
      .rmb{
        border:none !important;
        border-radius: 0;
      }
            .truncate {
  width: 100px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
img.post-pic{
        width:100%;
      }
            img.avatar-img{
        width:75px;
        border-radius:50px;
      }
      .user-bio-block{
        margin-top:0;
      }
    </style>
@endsection

@section('content')
    @parent
    	<div class="col-md-10">
								<div class="timeline-cover-section">
	<div class="timeline-cover" style="margin-top:100px">
          
        <div class="user-cover-progress hidden">

    </div>
      <!-- <div class="cover-bottom">
    </div> -->
    <div class="user-timeline-name">
      <a href="#">{{ $group->name }}</a>
              <span class="verified-badge bg-success">
          <i class="fa fa-check"></i>
        </span>
          </div>
    </div>
	<div class="timeline-list">
		<ul class="list-inline pagelike-links" style="">
      <li class=""><p style="padding:0 20px;"><span class="top-list">{{ $group->name }}</span></p></li>						
			<li class=""><a href="#"><span class="top-list">{{ App\GroupMembers::where('group','=', $group->id)->where('status','=', 1)->count() }} Members</span></a></li>
			</ul>
			<div class="status-button">
					<a href="#" class="btn btn-status">Status</a>
			</div>
      <div class="timeline-user-avtar" style="margin-top:25px;">

        <img src='/uploads/pics/{{ $group->icon }}' alt="Admin" title="Admin">
              
        <div class="user-avatar-progress hidden">
        </div>
      </div><!-- /timeline-user-avatar -->
		</div><!-- /timeline-list -->
	</div><!-- timeline-cover-section -->
				<div class="row" style="margin-top:25px;">
					<div class="timeline">
						<div class="col-md-4">
							<div class="user-profile-buttons">
							<div class="row follow-links pagelike-links">
								<!-- This [if-1] is for checking current user timeline or diff user timeline -->
                <div class="col-md-12">	
                <div class="user-bio-block text-center">
                @if($group->admin == Auth::user()->id)
										<a href="{{ URL('/edit/group/' . $group->id) }}" class="btn btn-profile"><i class="fa fa-pencil-square-o"></i>Admin Panel</a>
								 <!-- End of [if-1]-->
                <br>
                @else
                  @if(App\GroupMembers::where('group','=', $group->id)->where('user','=', Auth::user()->id)->count() == 0 )
                    <form action="{{ URL('/group/join/' . $group->id) }}" method="post">
                      {{ csrf_field() }}
                      <input type="hidden" name="_method" value="POST">
                      <input type="hidden" name="group" value="{{ $group->id }}">
                      <button type="submit" class="btn btn-success">Request to Join Prayer Circle</button>
                    </form>
                  @endif
                  @if(App\GroupMembers::where('group','=', $group->id)->where('user','=', Auth::user()->id)->where('status','=',0)->count() == 1)
                    Pending Admin Approval
                  @endif
                  @if(App\GroupMembers::where('group','=', $group->id)->where('user','=', Auth::user()->id)->where('status','=',1)->count() == 1)
                  <form action="{{ URL('/group/leave/' . $group->id) }}" method="post">
                      {{ csrf_field() }}
                      <input type="hidden" name="_method" value="POST">
                      <input type="hidden" name="group" value="{{ $group->id }}">
                    <button class="btn btn-danger">Leave Prayer Circle</button>
                  </form>
                  @endif
                @endif
                </div>
							</div>
              </div>
						</div>

						<div class="user-bio-block">
							<div class="bio-header">Bio</div>
							<div class="bio-description">
								{{ $group->name }}
							</div>
						</div>
	<!-- /my pages -->

	<!-- my-groups -->

	<!-- /my pages -->
													</div>

						<!-- Post box on timeline,page,group -->
						<div class="col-md-8">
@if(App\GroupMembers::where('group','=', $group->id)->where('user','=', Auth::user()->id)->where('status','=',1)->count() == 1 || App\Groups::where('id',$group->id)->value('admin') == Auth::user()->id)
							<form action="{{ URL('/post') }}" method="POST" class="create-post-form" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="POST">
              <input type="hidden" name="r" value="groups/{{ $group->id }}">
              <input type="hidden" name="group" value="{{ $group->id }}">
                <div class="panel panel-default panel-create"> <!-- panel-create -->
                    <div class="panel-heading">
                        <div class="heading-text">
                            What's going on?
                        </div>
                    </div>
                    <div class="panel-body"> 
                        <input type="text" name="title" class="form-control rmb" style="border-bottom:1px solid #DFE3E9 !important;" placeholder="Title" minlength="6" maxlength="50" required="">
                        <textarea name="content" class="form-control createpost-form comment" cols="30" rows="3" id="createPost" placeholder="Enter text here..." required=""></textarea>
                        <input type="file" name="pic" class="form-control rmb"> <i class="fa fa-photo pull-right" style="position:abolute; margin-right: 20px;  margin-top:-25px; font-size:16px; color:silver;"></i>
                        <input type="text" name="link" class="form-control rmb" placeholder="Insert link (optional)"> <i class="fa fa-paperclip pull-right" style="position:abolute; margin-right: 20px;  margin-top:-25px; font-size:16px; color:silver;"></i>
                    </div><!-- panel-body -->
                    <div class="panel-footer">
                        <ul class="list-inline right-list">
                            <li><button type="submit" class="btn btn-submit btn-success">post</button></li>
                        </ul>
                        <ul class="list-inline right-list">
                            <li>
                                <select class="form-control" name="cat" style="min-width:100px;">
                                  <?php
                                    $cats = \App\Categories::orderBy('id', 'ASC')->get();  
                                    foreach ($cats as $cat) {
                                  ?>
                                    <option name="cat" value="{{ $cat->id }}">{{ $cat->name }}</option>
                                  <?php } ?>
                                </select>
                            </li>
                        </ul>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
            
		<div class="timeline-posts">
			<div class="jscroll-inner">
				<?php 
            $posts = \App\Posts::where('group','=', $group->id)->orderBy('date','DESC')->get();                
            foreach ($posts as $post) {
              $name = \App\User::where("id","=",$post->author)->value("name");
                    $username = \App\Profiles::where("user","=",$post->author)->value("username");
        ?>
            <div class="panel panel-default panel-post animated" id="c-{{ $post->id }}">
                      <div class="panel-heading no-bg">
                        <div class="post-author">
                        @if($post->author == Auth::user()->id || Auth::user()->admin == 1)
                          <div class="post-options">
                            <ul class="list-inline no-margin">
                              <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">                                                
                                  <li class="main-link">
                                    <a href="#" data-post-id="{{ $post->id }}" class="edit-post" data-toggle="modal" data-target="#editModal">
                                      <i class="fa fa-edit" aria-hidden="true"></i>Edit
                                      <span class="small-text">You can edit your post</span>
                                    </a>
                                  </li>
                                  <li class="main-link">
                                      <a href="javascript:void(0)" class="delete-post" data-post-id="{{ $post->id }}">
                                        <i class="fa fa-trash" aria-hidden="true"></i>delete
                                        <span class="small-text">This post will be deleted</span>
                                      </a>
                                  </li>            
                                </ul>
                              </li>
                            </ul>
                          </div>
                        @else

                        <a href="javascript:void(0)" class="pull-right reporting" data-post-id="{{ $post->id }}" data-post-cat="1"><i class="fa fa-flag report"></i></a>

                        @endif

                          
                          <div class="user-post-details">
                            <ul class="list-unstyled no-margin">
                            <li><br></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="col-sm-12 col-md-3 text-center">
                          <div class="user-avatar">
                              <a href="{{ URL('/profile/' . $username) }}"><img src='/uploads/pics/{{ App\Profiles::where("user","=",$post->author)->value("pic") }}' alt="Admin" title="Admin" class="avatar-img"></a>
                              <br>
                                <a href="{{ URL('/profile/' . $username) }}" class="user-name user">{{ $name }}</a>
                                <br>
                                {{ convertHTMLTime($post->date) }}
                            </div>
                          </div>
                          <div class="col-sm-12 col-md-9">
                            <div class="text-wrapper">
                              <h3 class="post-title">{{ $post->title }}</h3>
                              @if(!empty($post->pic))
                              <img src="/uploads/posts/{{ $post->pic }}" class="post-pic">
                              @endif
                              <p class="post-content">{{ substr($post->content, 0, 200) }}... <br> @if(!empty($post->link)) <a href="{{ $post->link }}" class="post-link" target="_blank">Read More</a>@endif</p>
                              <div class="post-image-holder  ">
                              </div>
                            </div>
                        </div>

                          <ul class="actions-count list-inline">
                          </ul>
                        </div>
                        <div class="panel-footer socialite">
                          <ul class="list-inline footer-list">    

                            <li><a href="#" data-post-id="{{ $post->id }}" data-toggle="modal" data-target="#prayModal" class="pray-post">
                             <img src="{{ URL::asset('/images/pray.png') }}" width="16px"> <span class="count-{{ $post->id }}">{{ App\Likes::where('post', '=', $post->id)->count() }} </span> Praying</a></li>
                            <li><a href="{{ URL('/draft/to/' . $username . '/' . $post->title) }}" class="show-comments"><i class="fa fa-comment-o"></i>Send Message</a></li>
                          </ul>
                        </div>
                        <div class="comments-section all_comments">
                          <div class="comments-wrapper">         
                            <div class="to-comment">  <!-- to-comment -->
                              <div class="commenter-avatar">
                      <a href="javascript:void(0)"><img src='/uploads/pics/{{ App\Profiles::where("user","=",Auth::user()->id)->value("pic") }}' alt="Admin" title="Admin"></a>
                              </div>
                              <div class="comment-textfield">
                                <form method="POST" action="{{ URL('/post/comment') }}" class="comment-form">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="POST">
                                  <input type="hidden" name="r" value="groups/{{$post->group}}#c-{{ $post->id }}">
                                  <input type="hidden" name="post" value="{{ $post->id }}">
                                  <input class="form-control post-comment" autocomplete="off" data-post-id="1499" name="comment" placeholder="Write a comment...Press Enter to Post">
                                </form>
                              </div>
                              <div class="clearfix"></div>
                                
                            </div><!-- to-comment -->

                            <div class="comments post-comments-list">
                                 
                              <?php
                              $comments = App\Comments::where('post','=', $post->id)->orderBy('date','ASC')->get();
                              foreach ($comments as $comment) {

                              $commenterName = \App\User::where("id","=",$comment->author)->value("name");
                              $commenterPic = \App\Profiles::where("user","=",$comment->author)->value("pic");
                              $commenterUser = \App\Profiles::where("user","=",$comment->author)->value("username");
                              ?>
                              <ul class="list-unstyled main-comment comment632 " id="" style="margin-bottom:7px;"> 
                                <li> 
                                  <div class="comments delete_comment_list"> <!-- main-comment -->
                                    <div class="commenter-avatar">
                                      <a href="{{ URL('/profile/' . $commenterUser) }}"><img src="/uploads/pics/{{ $commenterPic }}" title="Admin" alt="Admin"></a>
                                    </div>
                                    <div class="comments-list">
                                      <div class="commenter">
                                          @if($comment->author == Auth::user()->id || $post->author == Auth::user()->id)
                                            <a href="#" class="delete-comment delete_comment" data-commentdelete-id="{{ $comment->id }}"><i class="fa fa-times"></i></a>
                                          @else 
                                            <a href="javascript:void(0)" class="pull-right reporting" data-post-id="{{ $comment->id }}" data-post-cat="3"><i class="fa fa-flag report"></i></a>
                                          @endif
                                          <div class="commenter-name">
                                            <a href="{{ URL('/profile/' . $commenterUser) }}">{{ $commenterName }}</a><span class="comment-description">{{ $comment->comment }}</span>
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                </ul>
                                <?php } ?>
                            </div><!-- comments/main-comment  -->            
                          </div>        
                        </div>
                      </div>
                
        <?php 
            } 
        ?>
			</div>
		</div>
    @endif
	</div><!-- /row -->
</div>
@include('modals')
@endsection

@section('js')
    @parent
@endsection