@extends('layouts.social')

@section('css')
    @parent
    <style>
      .like-post, .like-post:hover, .like-post:visited, .like-post:link, .like-post:active{
        text-decoration: none;
      }
    </style>
@endsection

@section('content')
    @parent
        <div class="col-md-12 col-lg-12">      
            <div class="">
              <div class="">
              <div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Prayer Circles I Started</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Prayer Circles I've Joined</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">All of My Prayer Circles</a></li>
    <li role="presentation"><a href="#create" aria-controls="create" role="tab" data-toggle="tab">Create a New Prayer Circle</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <ul class="list-group page-likes">
        <?php
        $groups = App\Groups::where('admin','=',Auth::user()->id)->get();
        foreach ($groups as $group) {
        ?>
          <li class="list-group-item">
            <div class="connect-list">
              <div class="connect-link pull-left">
                
                <a href="{{ URL('/groups/' . $group->id) }}">
                  <img src='/uploads/pics/{{ App\Groups::where("id","=", $group->id)->value("icon") }}' alt="My news" title="My news">{{ $group->name }}
                </a>
                <p><label>About:</label><br> {{ $group->description }}</p>
              </div>
              <div class="clearfix"></div>
            </div><!-- /connect-list -->
          </li>
          <?php } ?>
        </ul>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
      <ul class="list-group page-likes">
        <?php
        $groups = App\GroupMembers::where('user','=', Auth::user()->id)->get();      
        foreach ($groups as $groupe) {
          $group2 = App\Groups::where('id','=', $groupe->group)->first();
        ?>
          <li class="list-group-item">
            <div class="connect-list">
              <div class="connect-link pull-left">
                <a href="{{ URL('/groups/' . $group2->id) }}">
                  <img src='/uploads/pics/{{ $group2->icon }}' alt="My news" title="My news">{{ $group2->name }}
                </a>
                <p><label>About:</label><br> {{ $group2->description }}</p>
              </div>
              <div class="clearfix"></div>
            </div><!-- /connect-list -->
          </li>
          <?php } ?>
        </ul>
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">
        <ul class="list-group page-likes">
          <?php
          $groups = App\Groups::get();
          foreach ($groups as $group) {
          ?>
            <li class="list-group-item">
              <div class="connect-list">
                <div class="connect-link pull-left">
                  <a href="{{ URL('/groups/' . $group->id) }}">
                    <img src='/uploads/pics/{{ App\Groups::where("id","=", $group->id)->value("icon") }}' alt="My news" title="My news"> {{ $group->name }}
                  </a>
                  <p><label>About:</label><br> {{ $group->description }}</p>
                </div>
                <div class="clearfix"></div>
              </div><!-- /connect-list -->
            </li>
            <?php } ?>
          </ul>
    </div>
    <div role="tabpanel" class="tab-pane" id="create">
        <div class="">
          <div class="panel-heading panel-settings">
            <h3 class="panel-title">Create group</h3>
          </div>
          <div class="panel-body nopadding">
            <div class="socialite-form">
              <form class="form margin-right" method="POST" action="{{ URL('/group/create') }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="POST">
                <fieldset class="form-group required ">
                  <label for="name" class="control-label">name</label>
                  <input class="form-control" placeholder="Name of your group" name="name" maxlength="50" type="text" id="name">
                </fieldset>
                <fieldset class="form-group">
                  <label for="about">About</label>
                  <textarea class="form-control" placeholder="Write about your group..." rows="4" cols="20" name="description" id="about" required=""></textarea>
                </fieldset>
                <fieldset class="form-group hide">
                  <label for="privacy" class="control-label">Privacy</label>
                  <div class="radio select-option">
                    <label class="radio-header">
                      <input type="radio" name="privacy" id="optionsRadios1" value="1" checked="">
                      <i class="fa fa-globe"></i> open group
                      <p class="radio-text">Anyone can see and join the group.</p>
                    </label>
                  </div>
                  <div class="radio select-option margin-left">
                    <label class="margin-left-113 radio-header">
                      <input type="radio" name="privacy" id="optionsRadios2" value="0">
                      <i class="fa fa-lock"></i> closed group
                      <p class="radio-text">Anyone can see and request to join the group. Requests can be accepted or declined by admins.</p>
                    </label>
                  </div>
                </fieldset>
                <div class="pull-right">
                  <input class="btn btn-success" type="submit" value="Create group">
                </div>
                <div class="clearfix"></div>
              </form>
            </div><!-- /socialite-form -->
          </div>
        </div><!-- /panel -->   
    </div>
  </div>
</div>
                
              </div>
            </div>
        </div>

@endsection

@section('js')
    @parent
      <script> var SessionToken = "{{ csrf_token() }}"; </script>
@endsection