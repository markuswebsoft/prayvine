@extends('layouts.social')

@section('css')
  <style></style>
    @parent
    <style>
      .like-post, .like-post:hover, .like-post:visited, .like-post:link, .like-post:active{
        text-decoration: none;
      }
      .modal-backdrop.in{
        opacity: 0.4 !important;
      }
      @media (min-width: 1010px) {
        body .main-content{
          
          height:100%;
        }
        .center-content{
          padding-left:5%;
        }
      }
      img.avatar-img{
        width:75px;
        border-radius:50px;
      }
      .rmb{
        border:none !important;
        border-radius: 0;
      }
      img.post-pic{
        width:100%;
      }
.mini-profile{
  padding: 10px 0;
}
.mini-profile.socialite ul{
  list-style: none;
  width:100%;
}
.mini-profile.socialite ul li {
width:100%;
height:45px;
margin-left:-32px;

display:block;
}
.mini-profile.socialite ul li a, .mini-profile.socialite ul li a:hover{
  display:block;
  font-size: 13px;
color:#354052;
text-decoration: none;
}
.mini-profile.socialite ul li a i{
  width:25px;
}
.mini-profile.socialite ul li a .badge{
  margin-top:-3px;
}
.image-hover:hover, .image-hover:link{
text-decoration: none;
}
    </style>
@endsection
@section('aside')
<aside class="left">
            <div class="panel aside-bg">
              <div class="panel-body nopadding">
                <div class="mini-profile socialite text-center">
                  <div class="">
                      <div class="avatar-img">
                          <img src='/uploads/pics/{{ App\Profiles::where("user","=",Auth::user()->id)->value("pic") }}' alt="Admin" title="Admin">
                          <br>

                          <a href="{{ URL('/') }}">{{ Auth::user()->name }}</a>
                          <br>
                          <i class="fa fa-globe"></i> location, country
                          
                      </div>
                  </div>
                </div><!-- /mini-profile -->    
                <div class="mini-profile socialite text-left">
                  <br>
                  <ul>
                    <li>
                      <a href="{{ URL('/inbox') }}"><i class="fa fa-envelope"></i><span class="pull-right badge badge-danger">{{ $unread }}</span>Inbox
                      </a>
                    </li>
                    <li>
                      <a href="{{ URL('/groups') }}">
                            <i class="fa fa-bullseye"></i>All Prayer Circles
                      </a>
                    </li>
                    <li>
                    <a href="#">
                      <i class="fa fa-first-order"></i>Prayer Requests
                    </a></li>
                  </ul>   
                </div>                        
              </div>
            </div><!-- /panel -->
</aside>
<aside class="right">
            <div class="panel aside-bg">
              <div class="panel-body nopadding">
                    <div class="timeline-posts">
                    <p>Answered Prayers</p>
                <div class="jscroll-inner" style="overflow-y:auto;">
                <?php 
                    foreach ($feed as $post) {
                      if($post->cat == 4){
                        if(\App\Comments::where('post',$post->id)->where('author','!=', Auth::user()->id)->count() > 0){
                    $name = \App\User::where("id","=",$post->author)->value("name");
                    $username = \App\Profiles::where("user","=",$post->author)->value("username");

                ?>
                    <div class="panel panel-default panel-post animated" id="c-{{ $post->id }}">
                      <div class="panel-heading no-bg">
                        <div class="post-author">
                        @if($post->author == Auth::user()->id || Auth::user()->admin == 1)
                          <div class="post-options">
                            <ul class="list-inline no-margin">
                              <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">                                                
                                  <li class="main-link">
                                    <a href="#" data-post-id="{{ $post->id }}" class="edit-post" data-toggle="modal" data-target="#editModal">
                                      <i class="fa fa-edit" aria-hidden="true"></i>Edit
                                      <span class="small-text">You can edit your post</span>
                                    </a>
                                  </li>
                                  <li class="main-link">
                                      <a href="javascript:void(0)" class="delete-post" data-post-id="{{ $post->id }}">
                                        <i class="fa fa-trash" aria-hidden="true"></i>delete
                                        <span class="small-text">This post will be deleted</span>
                                      </a>
                                  </li>            
                                </ul>
                              </li>
                            </ul>
                          </div>
                        @else

                        <a href="javascript:void(0)" class="pull-right reporting" data-post-id="{{ $post->id }}" data-post-cat="1"><i class="fa fa-flag report"></i></a>

                        @endif

                          <div class="user-avatar">
                            <a href="{{ URL('/profile/' . $username) }}"><img src='/uploads/pics/{{ App\Profiles::where("user","=",$post->author)->value("pic") }}' alt="Admin" title="Admin"></a>
                          </div>
                          <div class="user-post-details">
                            <ul class="list-unstyled no-margin">
                              <li>
                                <a href="{{ URL('/profile/' . $username) }}" class="user-name user">{{ $name }}</a>
                                <br>
                                {{ convertHTMLTime($post->date) }}
                              </li>
                              <li>
                                <time class="post-time timeago">@if(Auth::user()->type == 1) Missionary @else Supporter @endif</time>
                              </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="text-wrapper">
                            <h3>{{ $post->title }}</h3>
                            @if(!empty($post->pic))
                            <img src="/uploads/posts/{{ $post->pic }}" class="post-pic">
                            @endif
                            <p class="post-content">{{ substr($post->content, 0, 200) }}...</p>
                            <div class="post-image-holder  ">
                            </div>
                          </div>
                          <ul class="actions-count list-inline">
                          </ul>
                        </div>
                        <div class="panel-footer socialite">
                          <ul class="list-inline footer-list">    

                            <li><a href="javascript:void(0)" class="like-post" data-post-id="{{ $post->id }}">
                            <img src="{{ URL::asset('/images/cross.png') }}" width="16px"> <span class="count-{{ $post->id }}">{{ App\Likes::where('post', '=', $post->id)->count() }}</span> Amen</a></li>
                          </ul>
                        </div>
                      </div>
                
                <?php 
              } // comment checker
              } // end cat checker
              } // end foreach 
              ?>
                </div>
            </div>           
              </div>
            </div><!-- /panel -->
</aside>
@endsection
@section('content')
    @parent
    <div class="col-md-12 show-mobile" style="padding: 5px 0;">
      <ul class="list-inline" style="width:100%; margin-bottom:-10px;">
        <li style="width:49%; background:white; padding: 15px 0; border-radius:5px" class="text-center">
          <a href="{{ URL('/feed') }}" style="color:#6d8f70">All Prayer Requests</a>
        </li>
        <li style="width:49%; padding: 15px 0; border-radius:5px" class="text-center">
          <a href="{{ URL('/prayers') }}" style="color:#6d8f70">Answered Prayers</a>
        </li>
      </ul>
    </div>
        <div class="col-md-6 col-md-offset-2 center-content">
        
        <div class="row">
          <div class="panel panel-default panel-post animated" id="">
            <div class="panel-heading no-bg text-right">
              <div class="row" style="padding:0 15px">
              <p>My Prayer Circles</p>
                <?php
                    $groups = App\GroupMembers::where('user','=',Auth::user()->id)->get();
                    foreach ($groups as $groupe) {
                    $group = App\Groups::where('id','=',$groupe->group)->first();
                ?>
                  <div class="col-md-3 col-sm-3 col-xs-3 best-pics text-center">
                    <a href="{{ URL('/groups/' . $group->id) }}" class="image-hover" title="{{ $group->name }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ $group->name }}">
                      <img src="/uploads/pics/{{ $group->icon }}" alt="{{ $group->name }}" title="{{ $group->name }}" class="avatar-img">
                      <br>
                      {{ $group->name }}
                    </a>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
            <div class="timeline-posts row">
    

                <p>All Prayer Requests</p>
                <div class="jscroll-inner">
                <?php 

                    foreach ($feed as $post) {
                    $name = \App\User::where("id","=",$post->author)->value("name");
                    $username = \App\Profiles::where("user","=",$post->author)->value("username");
                ?>
                    <div class="panel panel-default panel-post animated" id="c-{{ $post->id }}">
                      <div class="panel-heading no-bg">
                        <div class="post-author">
                        @if($post->author == Auth::user()->id || Auth::user()->admin == 1)
                          <div class="post-options">
                            <ul class="list-inline no-margin">
                              <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">                                                
                                  <li class="main-link">
                                    <a href="#" data-post-id="{{ $post->id }}" class="edit-post" data-toggle="modal" data-target="#editModal">
                                      <i class="fa fa-edit" aria-hidden="true"></i>Edit
                                      <span class="small-text">You can edit your post</span>
                                    </a>
                                  </li>
                                  <li class="main-link">
                                      <a href="javascript:void(0)" class="delete-post" data-post-id="{{ $post->id }}">
                                        <i class="fa fa-trash" aria-hidden="true"></i>delete
                                        <span class="small-text">This post will be deleted</span>
                                      </a>
                                  </li>            
                                </ul>
                              </li>
                            </ul>
                          </div>
                        @else

                        <a href="javascript:void(0)" class="pull-right reporting" data-post-id="{{ $post->id }}" data-post-cat="1"><i class="fa fa-flag report"></i></a>

                        @endif
                          <div class="user-post-details">
                            <ul class="list-unstyled no-margin">
                            <li><br></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="col-sm-12 col-md-3 text-center">
                          <div class="user-avatar">
                              <a href="{{ URL('/profile/' . $username) }}"><img src='/uploads/pics/{{ App\Profiles::where("user","=",$post->author)->value("pic") }}' alt="Admin" title="Admin" class="avatar-img"></a>
                              <br>
                                <a href="{{ URL('/profile/' . $username) }}" class="user-name user">{{ $name }}</a>
                                <br>
                                {{ convertHTMLTime($post->date) }}
                            </div>
                          </div>
                          <div class="col-sm-12 col-md-9">
                            <div class="text-wrapper">
                              <h3 class="post-title">{{ $post->title }}</h3>
                              @if(!empty($post->pic))
                              <img src="/uploads/posts/{{ $post->pic }}" class="post-pic">
                              @endif
                              <p>{{ substr($post->content, 0, 200) }}</p>
                              <p class="post-content hide">{{ $post->content }}<br> @if(strlen($post->content) > 200)...<br><a href="#" class="post-link" data-post-id="{{ $post->id }}" data-toggle="modal" data-target="#prayModal" class="pray-post">Read More</a>@endif</p>
                              <div class="post-image-holder  ">
                              </div>
                            </div>
                        </div>

                          <ul class="actions-count list-inline">
                          </ul>
                        </div>
                        <div class="panel-footer socialite">
                          <ul class="list-inline footer-list">    

                            <li><a href="#" data-post-id="{{ $post->id }}" data-toggle="modal" data-target="#prayModal" class="pray-post">
                             <img src="{{ URL::asset('/images/pray.png') }}" width="16px"> <span class="count-{{ $post->id }}">{{ App\Likes::where('post', '=', $post->id)->count() }} </span> Praying</a></li>
                            <li><a href="{{ URL('/draft/to/' . $username . '/' . $post->title) }}" class="show-comments"><i class="fa fa-comment-o"></i>Send Message</a></li>
                          </ul>
                        </div>
                        <div class="comments-section all_comments">
                          <div class="comments-wrapper">         
                            <div class="to-comment">  <!-- to-comment -->
                              <div class="commenter-avatar">
                      <a href="javascript:void(0)"><img src='/uploads/pics/{{ App\Profiles::where("user","=",Auth::user()->id)->value("pic") }}' alt="Admin" title="Admin"></a>
                              </div>
                              <div class="comment-textfield">
                                <form method="POST" action="{{ URL('/post/comment') }}" class="comment-form">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="POST">
                                  <input type="hidden" name="r" value="feed#c-{{ $post->id }}">
                                  <input type="hidden" name="post" value="{{ $post->id }}">
                                  <input class="form-control post-comment" autocomplete="off" data-post-id="1499" name="comment" placeholder="Write response and press Enter to post">
                                </form>
                              </div>
                              <div class="clearfix"></div>
                                
                            </div><!-- to-comment -->

                            <div class="comments post-comments-list">
                                 
                              <?php
                              $comments = App\Comments::where('post','=', $post->id)->orderBy('date','ASC')->get();
                              foreach ($comments as $comment) {

                              $commenterName = \App\User::where("id","=",$comment->author)->value("name");
                              $commenterPic = \App\Profiles::where("user","=",$comment->author)->value("pic");
                              $commenterUser = \App\Profiles::where("user","=",$comment->author)->value("username");
                              ?>
                              <ul class="list-unstyled main-comment comment632 " id="" style="margin-bottom:7px;"> 
                                <li> 
                                  <div class="comments delete_comment_list"> <!-- main-comment -->
                                    <div class="commenter-avatar">
                                      <a href="{{ URL('/profile/' . $commenterUser) }}"><img src="/uploads/pics/{{ $commenterPic }}" title="Admin" alt="Admin"></a>
                                    </div>
                                    <div class="comments-list">
                                      <div class="commenter">
                                          @if($comment->author == Auth::user()->id || $post->author == Auth::user()->id)
                                            <a href="#" class="delete-comment delete_comment" data-commentdelete-id="{{ $comment->id }}"><i class="fa fa-times"></i></a>
                                          @else 
                                            <a href="javascript:void(0)" class="pull-right reporting" data-post-id="{{ $comment->id }}" data-post-cat="3"><i class="fa fa-flag report"></i></a>
                                          @endif
                                          <div class="commenter-name">
                                            <a href="{{ URL('/profile/' . $commenterUser) }}">{{ $commenterName }}</a><span class="comment-description">{{ $comment->comment }}</span>
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                </ul>
                                <?php } ?>
                            </div><!-- comments/main-comment  -->            
                          </div>        
                        </div>
                      </div>
                
                <?php } ?>
                </div>
            </div>
        </div>
        @include('modals')
@endsection

@section('js')
    @parent
@endsection