@extends('layouts.social')

@section('css')
    @parent
      <style>
      .like-post, .like-post:hover, .like-post:visited, .like-post:link, .like-post:active{
        text-decoration: none;
      }
      .modal-backdrop.in{
        opacity: 0.4 !important;
      }
    </style>
@endsection

@section('content')
    @parent
    	<div class="col-md-10">
								<div class="timeline-cover-section">
	<div class="timeline-cover">
		<img src='/uploads/backgrounds/{{ $profile->background }}' alt="Admin" title="Admin">
					
				<div class="user-cover-progress hidden">

		</div>
			<!-- <div class="cover-bottom">
		</div> -->
		<div class="user-timeline-name">
			<a href="#">{{ $user->name }}</a>
					</div>
		</div>
	<div class="timeline-list">
		<ul class="list-inline pagelike-links" style="">
      <li class=""><p style="padding:0 20px;"><span class="top-list">Missionary</span></p></li>						
			<li class=""><a href="#"><span class="top-list">{{ getTotalPosts($user->id) }} Posts</span></a></li>
			<li class=""><a href="#"><span class="top-list">{{ getTotalGroups($user->id) }} joined groups</span></a></li>
			</ul>
			<div class="timeline-user-avtar">

				<img src='/uploads/pics/{{ $profile->pic }}' alt="Admin" title="Admin">
							
				<div class="user-avatar-progress hidden">
				</div>
			</div><!-- /timeline-user-avatar -->

		</div><!-- /timeline-list -->
	</div><!-- timeline-cover-section -->



				
				<div class="row">
					<div class="timeline">
						<div class="col-md-4">
							<div class="user-profile-buttons">
							<div class="row follow-links pagelike-links">
								<!-- This [if-1] is for checking current user timeline or diff user timeline -->	
                @if($profile->user == Auth::user()->id)
										<div class="col-md-12"><a href="{{ URL('/edit/profile') }}" class="btn btn-profile"><i class="fa fa-pencil-square-o"></i>Edit profile</a></div>
								 <!-- End of [if-1]-->
                @endif

							</div>
						</div>

						<div class="user-bio-block">
							<div class="bio-header">Bio</div>
							<div class="bio-description">
								{{ $profile->bio }}
							</div>
						</div>
	<!-- /my pages -->

	<!-- my-groups -->
	<div class="widget-pictures widget-best-pictures">

		<div class="picture pull-left">
			Groups
		</div>
		<div class="clearfix"></div>
		<div class="best-pictures my-best-pictures">
			<div class="row">
      <?php
        $groups = App\Groups::where('admin','=',$profile->user)->get();
        foreach ($groups as $group) {
        ?>
       <div class="col-md-2 col-sm-2 col-xs-2 best-pics">
          <a href="{{ URL('/groups/' . $group->id) }}" class="image-hover" title="{{ $group->name }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ $group->name }}">
            <img src="/uploads/pics/{{ $group->icon }}" alt="{{ $group->name }}" title="{{ $group->name }}">
          </a>
        </div>

     <?php } ?>
     <?php
        $groups = App\GroupMembers::where('user','=',$profile->user)->where('status',1)->get();
        foreach ($groups as $groupe) {
        $group = App\Groups::where('id','=',$groupe->group)->first();
        ?>
       <div class="col-md-2 col-sm-2 col-xs-2 best-pics">
          <a href="{{ URL('/groups/' . $group->id) }}" class="image-hover" title="{{ $group->name }}" data-toggle="tooltip" data-placement="top" data-original-title="{{ $group->name }}">
            <img src="/uploads/pics/{{ $group->icon }}" alt="{{ $group->name }}" title="{{ $group->name }}">
          </a>
        </div>

     <?php } ?>
								
		</div><!-- /row -->

		</div>
	</div>
	<!-- /my pages -->
													</div>

						<!-- Post box on timeline,page,group -->
						<div class="col-md-8">
@if($profile->user == Auth::user()->id)

          @endif

		<div class="timeline-posts">
			<div class="jscroll-inner">
				<?php 
                    $posts = \App\Posts::where('author','=', $profile->user)->orderBy('id', 'DESC')->get();                
                    foreach ($posts as $post) {
                ?>
                    <div class="panel panel-default panel-post animated" id="c-{{ $post->id }}">
                      <div class="panel-heading no-bg">
                        <div class="post-author">
                        @if($profile->user == Auth::user()->id || Auth::user()->admin == 1)
                          <div class="post-options">
                            <ul class="list-inline no-margin">
                              <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">                                                
                                  <li class="main-link">
                                    <a href="#" data-post-id="{{ $post->id }}" class="edit-post" data-toggle="modal" data-target="#editModal">
                                      <i class="fa fa-edit" aria-hidden="true"></i>Edit
                                      <span class="small-text">You can edit your post</span>
                                    </a>
                                  </li>
                                  <li class="main-link">
                                      <a href="javascript:void(0)" class="delete-post" data-post-id="{{ $post->id }}">
                                        <i class="fa fa-trash" aria-hidden="true"></i>delete
                                        <span class="small-text">This post will be deleted</span>
                                      </a>
                                  </li>            
                                </ul>
                              </li>
                            </ul>
                          </div>
                          @else 
            <a href="javascript:void(0)" class="pull-right reporting" data-post-id="{{ $post->id }}" data-post-cat="2"><i class="fa fa-flag report"></i></a>
            
                        @endif
                          <div class="user-avatar">
                            <a href="#"><img src='/uploads/pics/{{ App\Profiles::where("user","=",$post->author)->value("pic") }}' alt="Admin" title="Admin"></a>
                          </div>
                          <div class="user-post-details">
                            <ul class="list-unstyled no-margin">
                              <li>
                                <a href="{{ URL('/profile/' . $profile->username) }}" class="user-name user">{{ $user->name }}</a>
                                          </li>
                              <li>
                                <time class="post-time timeago">@if(Auth::user()->type == 1) Missionary @else Supporter @endif</time>
                                          </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="text-wrapper">
                            <p class="post-content">{{ $post->content }}</p>
                            <div class="post-image-holder  ">
                            </div>
                          </div>
                          <ul class="actions-count list-inline">
                          </ul>
                        </div>
                        <div class="panel-footer socialite">
                          <ul class="list-inline footer-list">    

                            <li>
                            <a href="#" data-post-id="{{ $post->id }}" data-toggle="modal" data-target="#prayModal" class="pray-post">

                             <img src="{{ URL::asset('/images/pray.png') }}" width="16px"> <span class="count-{{ $post->id }}">{{ App\Likes::where('post', '=', $post->id)->count() }}</span> Praying</a></li>
                            <li><a href="javascript:void(0)" class="show-comments"><i class="fa fa-comment-o"></i>comment</a></li>
                          </ul>
                        </div>
                        <div class="comments-section all_comments">
                          <div class="comments-wrapper">         
                            <div class="to-comment">  <!-- to-comment -->
                              <div class="commenter-avatar">
                                <a href="#"><img src='/uploads/pics/{{ App\Profiles::where("user","=", Auth::user()->id)->value("pic") }}' alt="Admin" title="Admin"></a>
                              </div>
                              <div class="comment-textfield">
                                  <form method="POST" action="{{ URL('/post/comment') }}" class="comment-form">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="POST">
                                  <input type="hidden" name="r" value="profile/{{ $profile->username }}/#c-{{ $post->id }}">
                                  <input type="hidden" name="post" value="{{ $post->id }}">
                                  <input class="form-control post-comment" autocomplete="off" data-post-id="1499" name="comment" placeholder="Write a comment...Press Enter to Post">
                                </form>
                              </div>
                              <div class="clearfix"></div>
                                
                            </div><!-- to-comment -->

                            <div class="comments post-comments-list">
                                 
                              	<?php
                              $comments = App\Comments::where('post','=', $post->id)->orderBy('date','ASC')->get();
                              foreach ($comments as $comment) {

                              $commenterName = \App\User::where("id","=",$comment->author)->value("name");
                              $commenterPic = \App\Profiles::where("user","=",$comment->author)->value("pic");
                              $commenterUser = \App\Profiles::where("user","=",$comment->author)->value("username");
                              ?>
                              <ul class="list-unstyled main-comment comment632 " id="comment632"> 
                                <li> 
                                  <div class="comments delete_comment_list"> <!-- main-comment -->
                                    <div class="commenter-avatar">
                                      <a href="{{ URL('/profile/' . $commenterUser) }}"><img src="/uploads/pics/{{ $commenterPic }}" title="Admin" alt="Admin"></a>
                                    </div>
                                    <div class="comments-list">
                                      <div class="commenter">
                                          @if($comment->author == Auth::user()->id || $post->author == Auth::user()->id)
                                            <a href="#" class="delete-comment delete_comment" data-commentdelete-id="{{ $comment->id }}"><i class="fa fa-times"></i></a>
                                          @else 
                                            <a href="javascript:void(0)" class="pull-right reporting" data-post-id="{{ $comment->id }}" data-post-cat="3"><i class="fa fa-flag report"></i></a>
                                          @endif
                                          <div class="commenter-name">
                                            <a href="{{ URL('/profile/' . $commenterUser) }}">{{ $commenterName }}</a><span class="comment-description">{{ $comment->comment }}</span>
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                </ul>
                                <?php } ?>
                              
                            </div><!-- comments/main-comment  -->            
                          </div>        
                        </div>
                      </div>
                <?php } ?>
			</div>
		</div>
	</div><!-- /row -->
</div>
<!-- Modals -->
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
                <div class="modal-dialog modal-likes" role="document">
                      <div class="modal-content">
                        <form action="{{ URL('/post/edit') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="postID" value="" id="postID">
                        <input type="hidden" name="r" value="feed" id="r">
                        <div class="panel panel-default panel-post animated" id="post1642">
                            <div class="panel-heading no-bg">
                              <div class="post-author">
                                <div class="user-avatar">
                                    <a href='/profile/{{ $profile->username }}'><img src='/uploads/pics/{{ $profile->pic }}' alt="Admin" title="Admin"></a>
                                </div>
                                <div class="user-post-details">
                                    <ul class="list-unstyled no-margin">
                                      <li>
                                        <a href='/profile/{{ $username = \App\Profiles::where("user","=",Auth::user()->id)->value("username") }}' class="user-name user">{{ Auth::user()->name }}</a>
                                      </li>
                                      <li>
                                        <time class="post-time timeago" datetime="2016-09-06 17:31:21" title="2016-09-06 17:31:21">
                                          Edit your post
                                        </time>
                                      </li>
                                    </ul>
                                </div>
                              </div>
                            </div>
                          <div class="panel-body">
                              <div class="text-wrapper">
                                    <textarea name="content" id="previousPost" rows="10" class="form-control" required="">#test</textarea>
                              </div>
                          </div>
                          <div class="panel-footer">
                                    <ul class="list-inline pull-right">
                                        <li><button type="submit" class="btn btn-submit btn-success">save changes</button></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </form>
                  </div>
                </div>
            </div>
                    @include('modals')
@endsection

@section('js')
    @parent
      <script type="text/javascript">
        $("body").on("click", ".edit-post", function(){
            var previousPost = $(this).closest(".panel").find(".post-content").text();
            $("#editModal").find("#postID").val($(this).attr("data-post-id"));
            $("#editModal").find("#r").val("profile/{{ $username }}/#c-" + $(this).attr("data-post-id"));
            $("#editModal").find("#previousPost").text(previousPost);
        });
      </script>
@endsection