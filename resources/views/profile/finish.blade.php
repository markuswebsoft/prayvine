@extends('layouts.social')

@section('css')
    @parent
        <style>
            .missionary, .supporter{
                display: none;
            }
            .er{
                color:red;
            }
        </style>
@endsection

@section('content')
    @parent
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading no-bg panel-settings">
                        <h3 class="panel-title">
                        Finish Your Profile
                    </h3>
                </div>
                <div class="panel-body">
                <form method="POST" action="{{ URL('/profile') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="POST">
                    @if(session('error') !== null)
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    <div class="form-group">
                        <select name="type" required="" class="form-control type" id="type">
                            <option name="type" value="">Select Account Type</option>
                            <option name="type" value="1" @if(old('type') == 1) selected @endif>Missionary</option>
                            <option name="type" value="2" @if(old('type') == 2) selected @endif>Supporter (required admin approval)</option>
                        </select>
                    </div>
                    <div class="missionary">
                        <div class="form-group">
                            <label>Missionary Name</label>
                            <input type="text" class="form-control" name="missionary" value="{{ old('missionary') }}">
                        </div>
                        <div class="form-group">
                            <label>Mission Location</label>
                            <select name="mission" class="form-control">
                                <option name="mission" value="">Select Mission Location</option>
                                <option name="mission" value="Africa" @if(old('mission') == "Africa") selected @endif>Africa</option>
                                <option name="mission" value="Asia" @if(old('mission') == "Asia") selected @endif>Asia</option>
                                <option name="mission" value="Australia South Pacific" @if(old('mission') == "Australia South Pacific") selected @endif>Australia South Pacific</option>
                                <option name="mission" value="Europe Europe East" @if(old('mission') == "Europe Europe East") selected @endif>Europe Europe East</option>
                                <option name="mission" value="North America Central America Caribbean" @if(old('mission') == "North America Central America Caribbean") selected @endif>North America Central America Caribbean</option>
                                <option name="mission" value="South America" @if(old('mission') == "South America") selected @endif>South America</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>How do you currently communicate mission news and prayer needs? (Select all that apply)</label>
                            <br>
                            <input type="checkbox" name="communicate[]" value="Email newsletter"> Email newsletter
                            <br>
                            <input type="checkbox" name="communicate[]" value="Print newsletter"> Print newsletter
                            <br>
                            <input type="checkbox" name="communicate[]" value="Facebook"> Facebook
                            <br>
                            <input type="checkbox" name="communicate[]" value="Text messaging"> Text messaging
                            <br>
                            <input type="checkbox" name="communicate[]" value="Phone tree"> Phone tree
                            <br>
                            <input type="checkbox" name="communicate[]" value="None"> None of the above
                            <br><br>
                            <label>Other:</label>
                            <input type="text" name="communicate[]" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>How frequently do you communicate with your mission supporters?</label>
                            <select name="frequency" required="" class="form-control">
                                <option name="frequency" value="Once or twice per year" @if(old('frequency') == "Once or twice per year") selected @endif>Once or twice per year</option>
                                <option name="frequency" value="Four to six times a year" @if(old('frequency') == "Four to six times a year") selected @endif>Four to six times a year</option>
                                <option name="frequency" value="Once a month" @if(old('frequency') == "Once a month") selected @endif>Once a month</option>
                            </select>
                        </div>
                    </div>
                    <div class="supporter">
                        
                    </div>
                    <div class="form-group">
                        <label>Skype Username (If outside of the U.S)</label>
                        <input type="text" class="form-control" name="skype" maxlength="50" required="" value="{{ old('skype') }}">
                    </div>
                    <div class="form-group usercheck required">
                        <label>Username</label>
                        <input type="text" class="form-control checkUsername" name="username" minlength="6" maxlength="15" required="" value="{{ old('username') }}">
                    </div>
                    <div class="form-group required">
                        <label>Describe yourself</label>
                        <textarea class="form-control" name="bio" required="">{{ old('bio') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Date of Birth</label>
                        <input type="date" class="form-control" name="dob" value="{{ old('dob') }}">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Finish Profile">
                    </div>
                </form>
                </div>
            </div>
        </div>
@endsection

@section('js')
    @parent
        <script>
        $(".type").change(function(){
            if($(this).val() == 1){
                $(".supporter").hide();
                $(".missionary").show();
            }
            if($(this).val() == 2){
                $(".missionary").hide();
                $(".supporter").show();
            }
        });
        var myusername = "";
        </script>
@endsection