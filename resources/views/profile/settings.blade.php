
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf_token" content="G7uoecvaZfMJbqosWslbOsVv6P3mW8hDkCGlR7xR"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" />
        <meta name="keywords" content="facebook clone, laravel, live chat, message, news feed, php social network, php social platform, php socialite, post, social, social network, social networking, social platform, social script, socialite">
        <meta name="description" content="Socialite is the FIRST Social networking script developed on Laravel with all enhanced features, Pixel perfect design and extremely user friendly. User interface and user experience are extra added features to Socialite. Months of research, passion and hard work had made the Socialite more flexible, feature-available and very user friendly!">
        <link rel="icon" type="image/x-icon" href="https://socialite.laravelguru.com/setting/favicon.jpg">


        <title>Socialite</title>

        <link media="all" type="text/css" rel="stylesheet" href="https://socialite.laravelguru.com/themes/default/assets/css/style-df9536bfeb.css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
        function SP_source() {
          return "https://socialite.laravelguru.com/";
        }
        var base_url = "https://socialite.laravelguru.com/";
        var theme_url = "https://socialite.laravelguru.com/themes/default/assets/";
        var current_username = "bootstrapguru";
        </script>
        <script src="https://socialite.laravelguru.com/themes/default/assets/js/main-adb4cfc428.js"></script>

            </head>
    <body>
        <nav class="navbar socialite navbar-default no-bg">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-4" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-4">
			<form class="navbar-form navbar-left form-left" role="search">
				<div class="input-group no-margin">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
					</span>
					<input type="text" id="navbar-search" data-url="https://socialite.laravelguru.com/api/v1/timelines" class="form-control" placeholder="Search for people, trends, pages and groups">
				</div><!-- /input-group -->
			</form>
			<!-- Collect the nav links, forms, and other content for toggling -->
			
						<ul class="nav navbar-nav navbar-right" id="navbar-right" v-cloak>
				<li>
					<ul class="list-inline notification-list">
						<li class="dropdown message notification">
							<a href="#" data-toggle="dropdown" @click.prevent="showNotifications" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell" aria-hidden="true">
																	</i>
								<span class="small-screen">Notifications</span>
							</a>
							<div class="dropdown-menu">
								<div class="dropdown-menu-header">
									<span class="pull-left">Notifications</span>
									<a v-if="unreadNotifications > 0" class="pull-right" href="#" @click.prevent="markNotificationsRead" >mark all read</a>
									<div class="clearfix"></div>
								</div>
																<ul class="list-unstyled dropdown-messages-list scrollable" data-type="notifications">
									<li class="inbox-message"  v-bind:class="[ !notification.seen ? 'active' : '' ]" v-for="notification in notifications.data">
										<a href="#/notification/{{ notification.id }}">
											<div class="media">
												<div class="media-left">
													<img class="media-object img-icon" v-bind:src="notification.notified_from.avatar" alt="images">
												</div>
												<div class="media-body">
													<h4 class="media-heading">
														<span class="notification-text"> {{ notification.description }} </span>
														<span class="message-time">
															<span class="notification-type"><i class="fa fa-user" aria-hidden="true"></i></span>
															<time class="timeago" datetime="{{ notification.created_at }}" title="{{ notification.created_at }}">
																{{ notification.created_at }}
															</time>
														</span>
													</h4>
												</div>
											</div>
										</a>
									</li>
									<li v-if="notificationsLoading" class="dropdown-loading">
										<i class="fa fa-spin fa-spinner"></i>
									</li>
								</ul>
																<div class="dropdown-menu-footer"><br>
									
								</div>
							</div>
						</li>
						<li class="dropdown message largescreen-message">
							<a href="#" data-toggle="dropdown" @click="showConversations" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-comments" aria-hidden="true">
									<span class="count" v-if="unreadConversations" >{{ unreadConversations }}</span>
								</i>
								<span class="small-screen">messages</span>
							</a>
							<div class="dropdown-menu">
								<div class="dropdown-menu-header">
									<span class="pull-left">messages</span>
									<div class="clearfix"></div>
								</div>
								<div class="no-messages hidden">
									<i class="fa fa-commenting-o" aria-hidden="true"></i>
									<p>You don&#039;t have any messages</p>
								</div>
								<ul class="list-unstyled dropdown-messages-list scrollable" data-type="messages">
									<li class="inbox-message" v-for="conversation in conversations.data">
										<a href="#" onclick="chatBoxes.sendMessage({{ conversation.user.id }})">
											<div class="media">
												<div class="media-left">
													<img class="media-object img-icon" v-bind:src="conversation.user.avatar" alt="images">
												</div>
												<div class="media-body">
													<h4 class="media-heading">
														<span class="message-heading">{{ conversation.user.name }}</span> 
														<span class="online-status hidden"></span>
														<time class="timeago message-time" datetime="{{ conversation.lastMessage.created_at }}" title="{{ conversation.lastMessage.created_at }}">
															{{ conversation.lastMessage.created_at }}
														</h4>
														<p class="message-text">
															{{ conversation.lastMessage.body }}
														</p>
													</div>
												</div>
											</a>
										</li>
										<li v-if="conversationsLoading" class="dropdown-loading">
											<i class="fa fa-spin fa-spinner"></i>
										</li>
									</ul>
									<div class="dropdown-menu-footer">
										<a href="https://socialite.laravelguru.com/messages">See all</a>
									</div>
								</div>
							</li>
							<li class="smallscreen-message">
								<a href="https://socialite.laravelguru.com/messages">
									<i class="fa fa-comments" aria-hidden="true">
										<span class="count" v-if="unreadConversations" >{{ unreadConversations }}</span>
									</i>
									<span class="small-screen">messages</span>
								</a>
							</li>
							<li class="chat-list-toggle">
								<a href="#"><i class="fa fa-users" aria-hidden="true"></i><span class="small-screen">chat-list</span></a>
							</li>
						</ul>
					</li>
					<li class="dropdown user-image socialite">
						<a href="#" class="dropdown-toggle no-padding" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" class="img-radius img-30" title="Admin">

							<span class="user-name">Admin</span><i class="fa fa-angle-down" aria-hidden="true"></i></a>
							<ul class="dropdown-menu">
																<li class=""><a href="https://socialite.laravelguru.com/admin"><i class="fa fa-user-secret" aria-hidden="true"></i>Admin</a></li>
																<li class=""><a href="#"><i class="fa fa-user" aria-hidden="true"></i>my profile</a></li>

								<li class=""><a href="#/pages-groups"><i class="fa fa-bars" aria-hidden="true"></i>My Pages &amp; Groups</a></li>

								<li class="active"><a href="#/settings/general"><i class="fa fa-cog" aria-hidden="true"></i>settings</a></li>

								<li><a href="https://socialite.laravelguru.com/logout"><i class="fa fa-unlock" aria-hidden="true"></i>Logout</a></li>
							</ul>
						</li>
	               <!--  <li class="logout">
	                    <a href="https://socialite.laravelguru.com/logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
	                </li> -->
	            </ul>
	            	        </div><!-- /.navbar-collapse -->
	    </div><!-- /.container-fluid -->
	</nav>	


	



        <div class="main-content">
            <!-- main-section -->
<!-- <div class="main-content"> -->
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="post-filters">
					<div class="panel panel-default">
	<div class="panel-body nopadding">
		<div class="mini-profile">
			<div class="background">
		        <div class="widget-bg">
		            <img src="  https://socialite.laravelguru.com/user/cover/2016-08-26-14-42-30despicable-me-2-evil-minion-wallpaper.jpg " alt="Admin" title="Admin">
		        </div>
				<div class="avatar-img">
					<img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin">
				</div>
			</div>
		    <div class="avatar-profile">
		        <div class="avatar-details">
		            <h2 class="avatar-name"><a href="#">Admin</h2></a>
		            <h4 class="avatar-mail">
		            	<a href="#" >
		            		@bootstrapguru
		            	</a>
		            </h4>
		        </div>      
		    </div><!-- /avatar-profile -->
		</div>
	</div><!-- /panel-body -->
</div><!-- /panel -->
<div class="list-group list-group-navigation socialite-group">
	<a href="#/settings/general" class="list-group-item">
		<div class="list-icon socialite-icon active">
			<i class="fa fa-user"></i>
		</div>
		<div class="list-text">
			general settings
			<div class="text-muted">
				You can change general settings
			</div>
		</div>
		<div class="clearfix"></div>
	</a>
	<a href="#/settings/privacy" class="list-group-item">
		<div class="list-icon socialite-icon ">
			<i class="fa fa-user-secret"></i>
		</div>
		<div class="list-text">
			privacy settings
			<div class="text-muted">
				Change privacy settings here
			</div>
		</div>
		<div class="clearfix"></div>
	</a>
	<a href="#/settings/notifications" class="list-group-item">
		<div class="list-icon socialite-icon ">
			<i class="fa fa-comments"></i>
		</div>
		<div class="list-text">
			email notifications
			<div class="text-muted">
				Manage your email notifications
			</div>
		</div>
		<div class="clearfix"></div>
	</a>
	<a href="#/settings/affliates" class="list-group-item">
		<div class="list-icon socialite-icon ">
			<i class="fa fa-user-plus"></i>
		</div>
		<div class="list-text">
			my affiliates
			<div class="text-muted">
				List of your affiliates
			</div>
		</div>
		<div class="clearfix"></div>
	</a>
	<a href="#/settings/deactivate" class="list-group-item">
		<div class="list-icon socialite-icon ">
			<i class="fa fa-trash"></i>
		</div>
		<div class="list-text">
			deactivate account
			<div class="text-muted">
				You can deactivate your account
			</div>
		</div>
		<div class="clearfix"></div>
	</a>
</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
				
					<div class="panel-heading no-bg panel-settings">
											<h3 class="panel-title">
							general settings
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">							
							<form method="POST" action="#/settings/general">
								<input type="hidden" name="_token" value="G7uoecvaZfMJbqosWslbOsVv6P3mW8hDkCGlR7xR">
								<div class="row">
									<div class="col-md-6">

										<fieldset class="form-group required ">
											<label for="username">Username</label>
											<input class="form-control" placeholder="Username" name="new_username" type="text" value="bootstrapguru">
																					</fieldset>
										
									</div>
									<div class="col-md-6">
										<fieldset class="form-group required ">
											<label for="name">Full name</label>
											<input class="form-control" placeholder="Full name" name="name" type="text" value="Admin" id="name">
																					</fieldset>
									</div>
								</div>
								<fieldset class="form-group">
									<label for="about">About</label>
									<textarea class="form-control" placeholder="Enter description about you" name="about" cols="50" rows="10" id="about">Some text about me</textarea>
								</fieldset>

								<div class="row">
									<div class="col-md-6">
										<fieldset class="form-group required ">
											<label for="email">E-mail address</label>
											<input class="form-control" placeholder="E-mail address" name="email" type="email" value="admin@socialite.com" id="email">
																					</fieldset>
									</div>
									<div class="col-md-6">
										<fieldset class="form-group">
											<label for="birthday">Birthday</label>
											

											<div class="input-group date datepicker">

												<span class="input-group-addon addon-left calendar-addon">
													<span class="fa fa-calendar"></span>
												</span>
												<input class="form-control" id="datepicker1" name="birthday" type="text" value="1994-01-03">
												<span class="input-group-addon addon-right angle-addon">
													<span class="fa fa-angle-down"></span>
												</span>
											</div>
										</fieldset>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-6">
										<fieldset class="form-group required">
											<label for="gender">Gender</label>
											<select class="form-control" id="gender" name="gender"><option value="male" selected="selected">Male</option><option value="female">Female</option><option value="other">None</option></select>										
										</fieldset>
									</div>
									<div class="col-md-6">
										<fieldset class="form-group">
											<label for="city">Current city</label>
											<input class="form-control" placeholder="Current city" name="city" type="text" value="Hyderabad" id="city">
										</fieldset>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<fieldset class="form-group">
											<label for="country">Country</label>
											<input class="form-control" placeholder="Country" name="country" type="text" value="India" id="country">
										</fieldset>
									</div>
									<div class="col-md-6">
										<fieldset class="form-group">
											<label for="timezone">Timezone</label>
											<select class="form-control" id="timezone" name="timezone"><option selected="selected" value="">Timezone</option><option value="Pacific/Midway">(GMT-11:00) Midway Island, Samoa</option></select>
											</fieldset>
										</div>
									<div class="col-md-6">
										<fieldset class="form-group">
											<label for="language">Language</label>
											<select class="form-control" id="language" name="language"><option value="en" selected="selected">English</option><option value="fr">French</option><option value="es">Spanish</option><option value="it">Italian</option><option value="pt">Portuguese</option><option value="ru">Russia</option><option value="ja">Japanese</option><option value="nl">Dutch</option><option value="zh">Chines</option><option value="hi">Hindi</option></select>
										</fieldset>
									</div>
									</div>

									<div class="pull-right">
										<input class="btn btn-success" type="submit" value="Save changes">
									</div>
									<div class="clearfix"></div>
								</form>
							</div><!-- /Socialite-form -->
						</div>
					</div>
					<!-- End of first panel -->

					<div class="panel panel-default">
						<div class="panel-heading no-bg panel-settings">
							<h3 class="panel-title">
								update password
							</h3>
						</div>
						<div class="panel-body nopadding">
							<div class="socialite-form">								
								<form method="POST" action="#/settings/password">
									<input type="hidden" name="_token" value="G7uoecvaZfMJbqosWslbOsVv6P3mW8hDkCGlR7xR">

									<div class="row">
										<div class="col-md-6">
											<fieldset class="form-group ">
												<label for="current_password">current password</label>
												<input type="password" class="form-control" id="current_password" name="current_password" value="" placeholder= "Enter old password">

																							</fieldset>
										</div>
										<div class="col-md-6">
											<fieldset class="form-group ">
												<label for="new_password">New password</label>
												<input type="password" class="form-control" id="new_password" name="new_password" value="" placeholder="Enter new password">

																							</fieldset>
										</div>
									</div>

									<div class="pull-right">
										<input class="btn btn-success" type="submit" value="save password">
									</div>
									<div class="clearfix"></div>
								</form>
							</div><!-- /Socialite-form -->
						</div>
					</div>
					<!-- End of second panel -->

				</div>
			</div><!-- /row -->
		</div>
	<!-- </div> --><!-- /main-content -->

        </div>

        <!-- right-sidebar -->
<div id="chatBoxes" v-cloak>
	<div class="chat-list">
		<div class="left-sidebar socialite">
			<ul class="list-group following-group scrollable smooth-scroll">
				<li class="list-group-item group-heading">following
					<div class="dropdown btn-setting">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i>
							<ul class="dropdown-menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li><a href="#">Separated link</a></li>
							</ul>
						</div>
					</li>
					<li class="list-group-item" v-for="conversation in conversations.data">
						<a href="#" @click.prevent="showChatBox(conversation)">
							<div class="media">
								<div class="media-left">
									<img v-bind:src="conversation.user.avatar" alt="images">
								</div>
								<div class="media-body">
									<h4 class="media-heading">{{ conversation.user.name }}</h4>
									<span class="pull-right active-ago" v-if="message">
										<time class="microtime" datetime="{{ message.created_at }}" title="{{ message.created_at }}">
                                            {{ message.created_at }}
                                        </time>
									</span>
								</div>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<!--/right-sidebar-->
		<div class="chatters" id="chatters">
						<div class="chat-box" v-bind:class="[chatBox.minimised ? 'chat-box-small' : '',  ]" v-for="chatBox in chatBoxes">
				<div class="chat-box-header">
					<span class="pull-left">
						<a href="#">{{ chatBox.user.name }}</a>
					</span>
					<ul class="list-inline pull-right">
						<li class="minimize-chatbox"><a href="#"><i class="fa fa-minus" @click.prevent="chatBox.minimised ? chatBox.minimised=false : chatBox.minimised=true" aria-hidden="true"></i></a></li>
						<li class="close-chatbox"><a href="#" @click.prevent="chatBoxes.$remove(chatBox)" ><i class="fa fa-times" aria-hidden="true"></i></a></li>
					</ul>
				</div>
				<div class="chat-conversation scrollable smooth-scroll">
					<ul class="list-unstyled chat-conversation-list">
						<li class="message-conversation" v-bind:class="[(1==message.user.id) ? 'current-user' : '',  ]" v-for="message in chatBox.conversationMessages.data">
							<div class="media">
								<div class="media-left">
									<a href="#">
										<img v-bind:src="message.user.avatar" alt="images">
									</a>
								</div>
								<div class="media-body ">
									<p class="post-text">
										{{ message.body }}
									</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="message-input">
					<fieldset class="form-group">
						<input class="form-control" v-model="chatBox.newMessage" v-on:keyup.enter="postMessage(chatBox)" id="exampleTextarea" >
					</fieldset>
					<!-- <ul class="list-inline">this fields are hidden because in dev 1.0 we dont use this fuctionality ,if we enable this the height of chat list to be increased
						<li><a href="#"><i class="fa fa-camera-retro" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
					</ul> -->
				</div>
			</div>
					</div>
	</div>

	
        
        <!-- Modal starts here-->
<div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
    <div class="modal-dialog modal-likes" role="document">
        <div class="modal-content">
        	<i class="fa fa-spinner fa-spin"></i>
        </div>
    </div>
</div>
<div class="col-md-12">
	<div class="footer-description">
		<div class="socialite-terms text-center">
							<a href="https://socialite.laravelguru.com/contact">Contact</a> - 
				<a href="#/create-page">Create page</a> - 
				<a href="#/create-group">Create group</a>
										- <a href="https://socialite.laravelguru.com/page/about">about</a>		        
		    				- <a href="https://socialite.laravelguru.com/page/privacy">privacy</a>		        
		    				- <a href="https://socialite.laravelguru.com/page/disclaimer">disclaimer</a>		        
		    				- <a href="https://socialite.laravelguru.com/page/terms">terms</a>		        
		    	
		    <a href="https://socialite.laravelguru.com/contact"> - Contact</a>
		</div>
		<div class="socialite-terms text-center">
			Available languages <span>:</span>
							
				English - 
				
				French - 
				
				Spanish - 
				
				Italian - 
				
				Portuguese - 
				
				Russia - 
				
				Japanese - 
				
				Dutch - 
				
				Chines - 
				
				Hindi - 
						
		</div>
		<div class="socialite-terms text-center">
			Copyright &copy; 2016 Socialite. All Rights Reserved
		</div>
	</div>
</div>



        <script>
                      var pusherConfig = {
                token: "G7uoecvaZfMJbqosWslbOsVv6P3mW8hDkCGlR7xR",
                PUSHER_KEY: "356db902ba009eec38a7"
            };
       </script>

        <script src="https://socialite.laravelguru.com/themes/default/assets/js/notifications.js"></script>
<script src="https://socialite.laravelguru.com/themes/default/assets/js/chatboxes.js"></script>
<script src="https://socialite.laravelguru.com/themes/default/assets/js/app.js"></script>

        
    </body>
</html>
