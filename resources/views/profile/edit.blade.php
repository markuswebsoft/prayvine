@extends('layouts.social')

@section('css')
    @parent
@endsection

@section('content')
    @parent
    	<div class="row">
			<div class="col-md-4">
				<div class="post-filters">
					<div class="panel panel-default">
	<div class="panel-body nopadding">
		<div class="mini-profile">
			<div class="background">
		        <div class="widget-bg">
		            <img src='/uploads/backgrounds/{{ App\Profiles::where("user","=",Auth::user()->id)->value("background") }}' alt="Admin" title="Admin">
		        </div>
				<div class="avatar-img">
					<img src='/uploads/pics/{{ App\Profiles::where("user","=",Auth::user()->id)->value("pic") }}' alt="Admin" title="Admin">
				</div>
			</div>
		    <div class="avatar-profile">
		        <div class="avatar-details">
		            <h2 class="avatar-name"><a href="#">{{ App\User::where("id","=",Auth::user()->id)->value("name") }}</a></h2>
		            <h4 class="avatar-mail">
		            	<a href="#">
		            		{{ "@" . App\Profiles::where("user","=",Auth::user()->id)->value("username") }}
		            	</a>
		            </h4>
		        </div>      
		    </div><!-- /avatar-profile -->
		</div>
	</div><!-- /panel-body -->
</div><!-- /panel -->
<div class="list-group list-group-navigation socialite-group">
	<a href="#" class="list-group-item">
		<div class="list-icon socialite-icon active">
			<i class="fa fa-photo"></i>
		</div>
		<div class="list-text">
			Update Profile Photo
			<div class="text-muted">
				<form id="pic" method="POST" action="{{ URL('/edit/profile/pic') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
              		<input type="hidden" name="_method" value="POST">
					<input type="file" name="pic" class="pic">
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</a>
</div>
<div class="list-group list-group-navigation socialite-group">
	<a href="#" class="list-group-item">
		<div class="list-icon socialite-icon active">
			<i class="fa fa-object-group"></i>
		</div>
		<div class="list-text">
			Update Background Photo
			<div class="text-muted">
				<form id="background" method="POST" action="{{ URL('/edit/profile/background') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
              		<input type="hidden" name="_method" value="POST">
					<input type="file" name="background" class="background">
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</a>
</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
				
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							General Settings
						</h3>
					</div>
					<div class="panel-body nopadding">
						<div class="socialite-form">							
							<form method="POST" action="{{ URL('/edit/profile/general') }}">
									{{ csrf_field() }}
              						<input type="hidden" name="_method" value="POST">
              						@if(session('error') !== null)
              							<div class="alert alert-danger">
					                        {{ session('error') }}
					                    </div>
					                @endif
					                @if(session('success') !== null)
              							<div class="alert alert-success">
					                        {{ session('success') }}
					                    </div>
					                @endif
								<div class="row">
									<div class="col-md-6">
										<fieldset class="form-group required usercheck">
											<label for="username">Username</label>
											<input class="form-control checkUsername" placeholder="Username" minlength="6" maxlength="15" name="username" type="text" value='@if(old("username") !== null){{ old("username") }}@else{{ App\Profiles::where("user","=",Auth::user()->id)->value("username") }}@endif'>
										</fieldset>
									</div>
									<div class="col-md-6">
										<fieldset class="form-group required ">
											<label for="name">Full name</label>
											<input class="form-control" placeholder="Full name" name="name" type="text" value='{{ App\User::where("id","=",Auth::user()->id)->value("name") }}' id="name">
										</fieldset>
									</div>
								</div>
								<fieldset class="form-group">
									<label for="about">About</label>
									<textarea class="form-control" placeholder="Enter description about you" name="bio" cols="50" rows="10" id="about">@if(old('bio') !== null){{ old('bio') }}@else{{App\Profiles::where("user","=",Auth::user()->id)->value("bio") }} @endif</textarea>
								</fieldset>

								<div class="row">
									<div class="col-md-6">
										<fieldset class="form-group required ">
											<label for="email">E-mail address</label>
											<input class="form-control" placeholder="E-mail address" name="email" type="email" value="@if(old('email') !== null){{ old('email') }}@else{{ Auth::user()->email }}@endif" id="email">
																					</fieldset>
									</div>
									<div class="col-md-6">
										<fieldset class="form-group">
											<label for="birthday">Birthday</label>
											<input class="form-control" id="datepicker1" name="dob" type="date" value={{ App\Profiles::where("user","=",Auth::user()->id)->value("dob") }}>
										</fieldset>
									</div>
								</div>

									<div class="pull-right">
										<input class="btn btn-success" type="submit" value="Save changes">
									</div>
									<div class="clearfix"></div>
								</form>
							</div><!-- /Socialite-form -->
						</div>
					</div>
					<!-- End of first panel -->

					<div class="panel panel-default">
						<div class="panel-heading no-bg panel-settings">
							<h3 class="panel-title">
								update password
							</h3>
						</div>
						<div class="panel-body nopadding">
							<div class="socialite-form">								
								<form method="POST" action="{{ URL('/edit/profile/password') }}">
									{{ csrf_field() }}
              						<input type="hidden" name="_method" value="POST">
									<div class="row">
										<div class="col-md-6">
											<fieldset class="form-group ">
												<label for="current_password">current password</label>
												<input type="password" class="form-control" id="current_password" name="current_password" value="" placeholder="Enter old password">

																							</fieldset>
										</div>
										<div class="col-md-6">
											<fieldset class="form-group ">
												<label for="new_password">New password</label>
												<input type="password" class="form-control" id="new_password" name="new_password" value="" placeholder="Enter new password">

											</fieldset>
										</div>
									</div>

									<div class="pull-right">
										<input class="btn btn-success" type="submit" value="save password">
									</div>
									<div class="clearfix"></div>
								</form>
							</div><!-- /Socialite-form -->
						</div>
					</div>
					<!-- End of second panel -->

				</div>
			</div>
@endsection

@section('js')
    @parent
    	<script type="text/javascript">
    	$(function() {
    		$(".pic").change(function(){
    			$("#pic").submit();
    		});
    		$(".background").change(function(){
    			$("#background").submit();
    		});
    	});
    	var myusername = "{{ App\Profiles::where("user","=",Auth::user()->id)->value("username") }}";
    	</script>
@endsection