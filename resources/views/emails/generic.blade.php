<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <title>Prayvine Email</title>
   <style type="text/css">
   	a {color: #4A72AF;}
	body, #header h1, #header h2, p {margin: 0; padding: 0;}
	#main {border: 1px solid #cfcece;}
	img {display: block;}
	#top-message p, #bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
	#header h1 {color: #ffffff !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; font-size: 24px; margin-bottom: 0!important; padding-bottom: 0; }
	#header h2 {color: #ffffff !important; font-family: Arial, Helvetica, sans-serif; font-size: 24px; margin-bottom: 0 !important; padding-bottom: 0; }
	#header p {color: #ffffff !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; font-size: 12px;  }
	h1, h2, h3, h4, h5, h6 {margin: 0 0 0.8em 0;}
	h3 {font-size: 28px; color: #444444 !important; font-family: Arial, Helvetica, sans-serif; }
	h4 {font-size: 22px; color: #4A72AF !important; font-family: Arial, Helvetica, sans-serif; }
	h5 {font-size: 18px; color: #444444 !important; font-family: Arial, Helvetica, sans-serif; }
	p {font-size: 12px; color: #444444 !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; line-height: 1.5;}
   </style>
</head>
<body>
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="e4e4e4"><tr><td>
<table id="main" width="600" align="center" cellpadding="0" cellspacing="15" bgcolor="ffffff">
		<tr>
			<td>
				<table id="header" cellpadding="10" cellspacing="0" align="center" bgcolor="8fb3e9" style="text-align:center;">
					<tr>
						<td width="570" bgcolor="395245"><h1><img src="http://54.152.77.34/frontend/images/branding.png"></h1></td>
					</tr>
					<tr>
						<td width="570" bgcolor="4a6154" style="background: url(http://tessat.s3.amazonaws.com/email-bg.jpg);"><h2 style="color:#ffffff!important">{{ $title }}</h2></td>
					</tr>
				</table><!-- header -->
			</td>
		</tr><!-- header -->

		<tr>
			<td></td>
		</tr>
		<tr>
			<td>
				<table id="content-2" cellpadding="0" cellspacing="0" align="center">
					<tr>
						<td width="570"><p>{{ $p1 }}</p></td>
					</tr>
				</table><!-- content-2 -->
			</td>
		</tr><!-- content-2 -->
		<tr>
			<td height="30"><img src="http://dummyimage.com/570x30/fff/fff" /></td>
		</tr>
		<tr>
			<td height="30"><img src="http://dummyimage.com/570x30/fff/fff" /></td>
		</tr>
		<tr>
			<td height="30"><img src="http://dummyimage.com/570x30/fff/fff" /></td>
		</tr>
		<tr>
			<td>
				<table id="content-6" cellpadding="0" cellspacing="0" align="center">
					<p align="center">Prayvine is a new digital tool for advancing ministry relationships, Prayvine connects missionaries and their supporters through prayer and online community. </p>
					<p align="center"><a href="#">CALL TO ACTION</a></p>
				</table>
			</td>
		</tr>

	</table><!-- main -->
</td></tr></table><!-- wrapper -->



</body>
</html>