
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf_token" content="G7uoecvaZfMJbqosWslbOsVv6P3mW8hDkCGlR7xR"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
        <meta name="keywords" content="facebook clone, laravel, live chat, message, news feed, php social network, php social platform, php socialite, post, social, social network, social networking, social platform, social script, socialite">
        <meta name="description" content="Socialite is the FIRST Social networking script developed on Laravel with all enhanced features, Pixel perfect design and extremely user friendly. User interface and user experience are extra added features to Socialite. Months of research, passion and hard work had made the Socialite more flexible, feature-available and very user friendly!">
        <link rel="icon" type="image/x-icon" href="https://socialite.laravelguru.com/setting/favicon.jpg">


        <title>Socialite</title>

        <link media="all" type="text/css" rel="stylesheet" href="https://socialite.laravelguru.com/themes/default/assets/css/style-df9536bfeb.css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
        function SP_source() {
          return "https://socialite.laravelguru.com/";
        }
        var base_url = "https://socialite.laravelguru.com/";
        var theme_url = "https://socialite.laravelguru.com/themes/default/assets/";
        </script>
        <script src="https://socialite.laravelguru.com/themes/default/assets/js/main-adb4cfc428.js"></script>

            </head>
    <body>
        <div class="">
            <nav class="navbar socialite navbar-default no-bg">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-4" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand socialite" href="https://socialite.laravelguru.com">
				<img class="socialite-logo" src="https://socialite.laravelguru.com/setting/logo.jpg" alt="Socialite" title="Socialite">
			</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-4">
			
							
						<ul class="nav navbar-nav navbar-right">
				<li class="logout">
					<a href="https://socialite.laravelguru.com/register"><i class="fa fa-sign-in" aria-hidden="true"></i> Join</a>
				</li>
			</ul>
				        </div><!-- /.navbar-collapse -->
	    </div><!-- /.container-fluid -->
	</nav>	
	    
        </div>
        
        <div class="main-content">	
	<div class="container">		
		<div class="panel panel-default contact-panel">
		
			<div class="panel-heading no-bg panel-settings">
				<h3 class="panel-title">Contact</h3>

			</div>
			<div class="panel-body static-body">
				<p class="static-para">
					Contact page description can be here
				</p>
								<div class="login-block static-pages">


					<!-- /contact form goes here --> 

					<div class="contact-form">
						<!-- <h3 class="av-special-heading-tag" itemprop="headline">Send us mail</h3> -->
						<div class="special-heading-border">
							<div class="special-heading-inner-border">

							</div>
						</div>
					</div>

					<form method="POST" action="https://socialite.laravelguru.com/contact" class="socialite-form">
					<input type="hidden" name="_token" value="G7uoecvaZfMJbqosWslbOsVv6P3mW8hDkCGlR7xR">
						<fieldset>
							<div class="row">
								<div class="col-md-6 col-sm-12 col-xs-12 left-form">
									<div class="form-group required ">
										<label for="name" class="control-label">name</label>
										<input type="text" class="form-control" name="name" placeholder="Enter your name">
																			</div>
								</div>
								<div class="col-md-6 col-sm-12 col-xs-12 right-form">
									<div class="form-group required ">
										<label for="email" class="control-label">E-mail address</label>
										<input type="email" class="form-control" name="email" placeholder="Enter genuine E-mail">
																			</div>
								</div>

								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group required ">
										<label for="subject" class="control-label">Subject</label>
										<input type="text" class="form-control" placeholder="Main subject line" name="subject">
																			</div>
								</div>

								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group required ">
										<label for="message" class="control-label">Message</label>
										<textarea class="form-control" rows="3" placeholder="Enter your message" name="message"></textarea>
																			</div>
								</div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success btn-submit">Submit</button>
							</div>

						</fieldset>
					</form>

				</div>
				<!-- /login-block -->
			</div>
		</div>
	</div>
</div>
        
        <!-- Modal starts here-->
<div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
    <div class="modal-dialog modal-likes" role="document">
        <div class="modal-content">
        	<i class="fa fa-spinner fa-spin"></i>
        </div>
    </div>
</div>
<div class="col-md-12">
	<div class="footer-description">
		<div class="socialite-terms text-center">
							<a href="https://socialite.laravelguru.com/login">login</a> - 
				<a href="https://socialite.laravelguru.com/register">register</a>
										- <a href="https://socialite.laravelguru.com/page/about">about</a>		        
		    				- <a href="https://socialite.laravelguru.com/page/privacy">privacy</a>		        
		    				- <a href="https://socialite.laravelguru.com/page/disclaimer">disclaimer</a>		        
		    				- <a href="https://socialite.laravelguru.com/page/terms">terms</a>		        
		    	
		    <a href="https://socialite.laravelguru.com/contact"> - Contact</a>
		</div>
		<div class="socialite-terms text-center">
			Available languages <span>:</span>
							
				English - 
				
				French - 
				
				Spanish - 
				
				Italian - 
				
				Portuguese - 
				
				Russia - 
				
				Japanese - 
				
				Dutch - 
				
				Chines - 
				
				Hindi - 
						
		</div>
		<div class="socialite-terms text-center">
			Copyright &copy; 2016 Socialite. All Rights Reserved
		</div>
	</div>
</div>



        <script src="https://socialite.laravelguru.com/themes/default/assets/js/app.js"></script>

    </body>
</html>
