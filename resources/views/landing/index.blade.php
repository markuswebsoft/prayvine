
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Benaissa Ghrib" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title>LIGHTLANDIN : Landing Page Template</title>

<!--Stylesheet-->

<!--[if IE 7]>
<link rel="stylesheet" href="{{ URL::asset('/frontend/css/fontello-ie7.css') }}"><![endif]-->

<link href="{{ URL::asset('/frontend/css/font.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('/frontend/css/fontello.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('/frontend/css/main.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ URL::asset('/frontend/css/ui.totop.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('/frontend/css/jquery-ui-1.8.23.custom.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('/frontend/css/flexslider.css') }}" />



<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<!--[if lt IE 8]>
<style>
/* For IE < 8 (trigger hasLayout) */
.clearfix {
    zoom:1;
}
</style>
<![endif]-->
<style type="text/css">
    .holder960 .teaserTitle{
        margin-top:100px !important ;
    }
    #header{
        background: transparent !important;
        border-bottom: 0;
    }
    .fs14{
        font-size: 14px;
        color:whitesmoke;
    }
    #footer{
        background-color:rgba(0,0,0,0.5);
        color:white;
        margin:0;
        padding:30px 15px;
        margin-top:-80px;
    }
    #footer ul{
        float:left;
    }
    #footer p{
        float:right;
        color:silver;
    }
    #footer ul li a{
        color:white !important;
    }
    .btn.btn-primary{
        background-color: rgba(217,217,35, 0.7);
        font-size: 16px;
    }
    .helperlink{
        width:150px;
        margin: 0 auto;
        color:white;
        font-size: 12px;
    }
</style>

</head>

<body>

    <!-- Preloader -->
    <div id="loader">
      <div id="loaderInner"></div>
    </div>


    
<!--Wrapper-->
    <div id="wrapper">
        <header id="header" class=" default clearfix">
            
            <!--Holder 960-->
            <div class=" clearfix">

            <!--Logo-->
            <div class="logo" style="width:100%;">
            <div href="#wrapper" style="width:100%;">
                <h1 style="color:white; width:89%;">
                    <table width="100%">
                        <tr>
                            <td style="width:30px;">
                                <img src="{{ URL::asset('/frontend/images/exclamation.png') }}" width="25px">
                            </td>
                            <td style="vertical-align:top; text-align:left">  &nbsp; Learn More</td>
                            <td style="vertical-align:top; text-align:right;"><a href="https://prayvine.zendesk.com/hc/en-us" target="_blank">  &nbsp; Help</a></td>
                            <td style="width:30px; text-align:right;"><img src="{{ URL::asset('/frontend/images/learnmore.png') }}" width="25px"></td>
                        </tr>
                    </table>
                </h1>
            </div>
            </div>
            <!--End logo-->


            <!--Navigation-->
            <!--End navigation-->
                </div>
                <!--End Holder 960-->
            </header>

            <!--Header-->
            
            
            <!--Teaser-->
            <div id="teaser" class="clearfix">
                
                    <!--Overlay-->
                    <div class="overlay">
                        
                        
                    <!--Holder 960-->
                    <div class="holder960 clearfix">
                        
                        <!--Teaser title-->
                        <div class="teaserTitle" style="text-align:center; margin:130px 0 0 0!important;">
                            <img src="{{ URL::asset('/frontend/images/branding.png') }}" width="250px">
                            <br><br>
                            <p class="fs14">Online Prayer Circles for Christian Ministry</p>
<br>
                            <p class="fs14">
A new digital tool for advancing ministry relationships, <br>Prayvine connects missionaries and their supporters through <br>prayer and online community. 
<br><br>
Sign in or sign up below:</p>
<br><br>
                        </div>
                        <!--End teaser title-->
                        
                        <!--Down-->
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" style="width:330px; margin: 0 auto;">
                        {{ csrf_field() }}
                        @if(session('error') !== null)
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label"></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label"></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    LOGIN
                                </button>
                                <br><br>
                                <a class="helperlink" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                                <br><br>
                                <a class="helperlink" href="{{ url('/register') }}" style="color:white">
                                    Need an account?
                                </a>
                            </div>
                        </div>
                    </form>
                        <!--End down-->
                    
                    </div>
                    <!--End Holder 960-->
                    
                    </div>
                    <!--End overlay-->
            </div>
            <!--End teaser-->
            <footer id="footer" class="clearfix"> 
                <ul>
                    <li><a href="{{ URL('/about') }}">About Prayvine</a></li>
                    <li><a href="https://payments.paysimple.com/Buyer/CheckOutFormPay/W0-l9FxrpeWVa6gK1VLDqCY5ja0-" target="_blank">Donate</a></li>
                    <li><a href="{{ URL('/privacy') }}">Privacy</a></li>
                    <li><a href="{{ URL('/terms') }}">Terms of Use</a></li>
                </ul>
                <p>© 2016 Prayvine | Prayvine is a 501(c)(3) not-for-profit organization; all donations are tax-deductible to the full extent of the law.</p>
            </footer>
    </div>
<!--ENd wrapper-->  
    
<!--Javascript-->
<script src="{{ URL::asset('/frontend/js/jquery-1.9.1.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('/frontend/js/jquery-migrate-1.2.1.js') }}"></script>
<script src="{{ URL::asset('/frontend/js/jquery.flexslider-min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('/frontend/js/jquery.easing.1.3.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('/frontend/js/jquery.scrollTo-min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('/frontend/js/jquery-ui-1.8.23.custom.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('/frontend/js/waypoints.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('/frontend/js/jquery.parallax-1.1.3.js') }}"></script>
<script src="{{ URL::asset('/frontend/js/Placeholders.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('/frontend/js/jquery.ui.totop.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('/frontend/js/jquery.validate.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('/frontend/js/script.js') }}" type="text/javascript"></script>


<!-- Google analytics -->


<!-- End google analytics -->


</body>
</html>
