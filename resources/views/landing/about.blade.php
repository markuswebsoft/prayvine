@extends('layouts.frontend')

@section('content')
<h2>Prayvine is a new way to organize, write, and share prayers with your trusted friends.</h2>
<h3>Here are some ways people are using Prayvine...</h3>
<ul><li>Major illness or crisis: Create a prayer community on Prayvine where you pray together about a major health problem or difficult life circumstance. Post updates to keep everyone informed and receive encouragement through intercessory prayer.</li>
<li>Small groups and Bible-study groups: Continue to share and pray for each other during the daily grind between group meetings.</li>
<li>Missionaries and ministry workers: Provide regular updates to your supporters and receive encouragement as you read their prayers and pray for them.</li>
<li>One-on-one discipleship and mentoring: Create a Prayvine prayer community for your discipleship relationship, where you can pray for each other one-on-one.</li>
<li>Mothers: Create a prayer community for a circle of moms to regularly pray for one anothers' children.</li>
<li>Men: Receive accountability (e.g. work, marriage, etc.) and encouragement through prayer communities on Prayvine.</li>
<li>Individuals: Spend time with God, without inviting anyone. Possibilities include prayer journals for thanksgiving, worship, and praying through the Bible.</li>
</ul><h3>How is Prayvine Different?</h3>
<p>Unlike email or social networks (e.g. Facebook), Prayvine was created for prayer:</p>
<ul><li>Easy-to-use, focused on prayer: Clean, clutter-free design helps you focus on the Lord.</li>
<li>Private: All Prayvine prayer communities are invitation-only and private by default. We will never sell your personal information to advertisers or third parties. Prayer communities cannot be shared with Facebook, Twitter, or other social networks.</li>
<li>Secure: We encrypt all communications as it travels between your browser and our web servers. We use https by default.</li>
<li>Ad-free: Advertising is about getting people to feel like they are missing something; prayer is about the riches we already have in God. That's why we don't show ads on Prayvine.</li>
<li>Not-for-profit: Prayvine is incorporated as a 501(c)(3) not-for-profit public charity. We are are motivated by faith and strive to live by the values of God's kingdom. </li>
<li>Free gift of grace: Prayvine will always be available for free to all, regardless of their religious beliefs or backgrounds. <a href="https://payments.paysimple.com/Buyer/CheckOutFormPay/W0-l9FxrpeWVa6gK1VLDqCY5ja0-">You can partner with us through your tax-deductible donations by clicking here.</a></li>
</ul><h3>Mission and Team</h3>
<h3><em>"I am the vine, you are the branches. He who abides in Me, and I in him, bears much fruit; for without Me you can do nothing." -John 15:5 (NKJV)</em><br><br>Prayvine's mission is to follow Jesus by building and giving away technology products that can help facilitate God's loving, redemptive, and transforming work in the world. </h3>
<p>Prayvine was co-founded in 2013 by Ian and Kim Hsu as a 501(c)(3) non-profit. In response to God's calling in his life, Ian left his job as Director of Internet Media Outreach at Stanford University to combine his deep understanding of social applications with his desire to engage with and pray more consistently for missionaries. Ian and Kim attend Peninsula Bible Church (Palo Alto, CA), where they have enjoyed serving with children's, middle school, college, and women's ministries over the past 15+ years.</p>
<p>Prayvine is governed by a board of directors. The Prayvine board of directors consists of Paul Taylor (pastor, Peninsula Bible Church, Palo Alto, California), Judy Herminghaus (retired pastor of women's ministry and missions, Truckee, California), Oliver Miao (CEO, Pixelberry Studios, Mountain View, California), and Ian Hsu (Co-founder, Prayvine, Orlando, Florida). </p>
@endsection