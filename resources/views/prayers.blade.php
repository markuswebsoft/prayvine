@extends('layouts.social')

@section('css')
  <style></style>
    @parent
    <style>
      .like-post, .like-post:hover, .like-post:visited, .like-post:link, .like-post:active{
        text-decoration: none;
      }
      .modal-backdrop.in{
        opacity: 0.4 !important;
      }
      @media (min-width: 1010px) {
        body .main-content{
          
          height:100%;
        }
        .center-content{
          padding-left:5%;
        }
      }
      img.avatar-img{
        width:75px;
        border-radius:50px;
      }
      .rmb{
        border:none !important;
        border-radius: 0;
      }
      img.post-pic{
        width:100%;
      }
.mini-profile{
  padding: 10px 0;
}
.mini-profile.socialite ul{
  list-style: none;
  width:100%;
}
.mini-profile.socialite ul li {
width:100%;
height:45px;
margin-left:-32px;

display:block;
}
.mini-profile.socialite ul li a, .mini-profile.socialite ul li a:hover{
  display:block;
  font-size: 13px;
color:#354052;
text-decoration: none;
}
.mini-profile.socialite ul li a i{
  width:25px;
}
.mini-profile.socialite ul li a .badge{
  margin-top:-3px;
}
.image-hover:hover, .image-hover:link{
text-decoration: none;
}
    </style>
@endsection
@section('aside')
@endsection
@section('content')
    @parent
        <div class="col-md-12 show-mobile" style="padding: 5px 0;">
      <ul class="list-inline" style="width:100%; margin-bottom:-10px;">
        <li style="width:49%; padding: 15px 0; border-radius:5px" class="text-center">
          <a href="{{ URL('/feed') }}" style="color:#6d8f70">All Prayer Requests</a>
        </li>
        <li style="width:49%; background:white; padding: 15px 0; border-radius:5px" class="text-center">
          <a href="{{ URL('/prayers') }}" style="color:#6d8f70">Answered Prayers</a>
        </li>
      </ul>
    </div>
        <div class="col-md-6 col-md-offset-2 center-content">
            <div class="timeline-posts row">
                <div>
            <div class="panel aside-bg">
              <div class="panel-body nopadding">
                    <div class="timeline-posts">
                    <p>Answered Prayers</p>
                <div class="jscroll-inner" style="overflow-y:auto;">
                <?php 
                    foreach ($feed as $post) {
                      if($post->cat == 4){
                        if(\App\Comments::where('post',$post->id)->where('author','!=', Auth::user()->id)->count() > 0){
                    $name = \App\User::where("id","=",$post->author)->value("name");
                    $username = \App\Profiles::where("user","=",$post->author)->value("username");

                ?>
                    <div class="panel panel-default panel-post animated" id="c-{{ $post->id }}">
                      <div class="panel-heading no-bg">
                        <div class="post-author">
                        @if($post->author == Auth::user()->id || Auth::user()->admin == 1)
                          <div class="post-options">
                            <ul class="list-inline no-margin">
                              <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">                                                
                                  <li class="main-link">
                                    <a href="#" data-post-id="{{ $post->id }}" class="edit-post" data-toggle="modal" data-target="#editModal">
                                      <i class="fa fa-edit" aria-hidden="true"></i>Edit
                                      <span class="small-text">You can edit your post</span>
                                    </a>
                                  </li>
                                  <li class="main-link">
                                      <a href="javascript:void(0)" class="delete-post" data-post-id="{{ $post->id }}">
                                        <i class="fa fa-trash" aria-hidden="true"></i>delete
                                        <span class="small-text">This post will be deleted</span>
                                      </a>
                                  </li>            
                                </ul>
                              </li>
                            </ul>
                          </div>
                        @else

                        <a href="javascript:void(0)" class="pull-right reporting" data-post-id="{{ $post->id }}" data-post-cat="1"><i class="fa fa-flag report"></i></a>

                        @endif

                          <div class="user-avatar">
                            <a href="{{ URL('/profile/' . $username) }}"><img src='/uploads/pics/{{ App\Profiles::where("user","=",$post->author)->value("pic") }}' alt="Admin" title="Admin"></a>
                          </div>
                          <div class="user-post-details">
                            <ul class="list-unstyled no-margin">
                              <li>
                                <a href="{{ URL('/profile/' . $username) }}" class="user-name user">{{ $name }}</a>
                                <br>
                                {{ convertHTMLTime($post->date) }}
                              </li>
                              <li>
                                <time class="post-time timeago">@if(Auth::user()->type == 1) Missionary @else Supporter @endif</time>
                              </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="panel-body">
                          <div class="text-wrapper">
                            <h3>{{ $post->title }}</h3>
                            @if(!empty($post->pic))
                            <img src="/uploads/posts/{{ $post->pic }}" class="post-pic">
                            @endif
                            <p class="post-content">{{ substr($post->content, 0, 200) }}...</p>
                            <div class="post-image-holder  ">
                            </div>
                          </div>
                          <ul class="actions-count list-inline">
                          </ul>
                        </div>
                        <div class="panel-footer socialite">
                          <ul class="list-inline footer-list">    

                            <li><a href="javascript:void(0)" class="like-post" data-post-id="{{ $post->id }}">
                             <img src="{{ URL::asset('/images/cross.png') }}" width="16px">  <span class="count-{{ $post->id }}">{{ App\Likes::where('post', '=', $post->id)->count() }}</span> Amen</a></li>
                          </ul>
                        </div>
                      </div>
                
                <?php 
              } // comment checker
              } // end cat checker
              } // end foreach 
              ?>
                </div>
            </div>           
              </div>
            </div><!-- /panel -->
                </div>
            </div>
        </div>
        @include('modals')
@endsection

@section('js')
    @parent
@endsection