    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body admin-nav">
                    <a href="{{ URL('/admin') }}" class="col-md-3 col-xs-12 @if($page =='users')active @endif"><i class="fa fa-user"></i> Users</a>
                    <a href="{{ URL('/admin/groups') }}" class="col-md-3 col-xs-12 @if($page =='groups')active @endif"><i class="fa fa-users"></i> Groups</a>
                    <a href="{{ URL('/admin/flags') }}" class="col-md-3 col-xs-12 @if($page =='flags')active @endif"><i class="fa fa-flag"></i> Flags</a>
                    <a href="{{ URL('/admin/stats') }}" class="col-md-3 col-xs-12 @if($page =='stats')active @endif"><i class="fa fa-bar-chart"></i> Statistics</a>
                </div>
            </div>
        </div>
    </div>