@extends('layouts.social')

@section('css')
    @parent
        <style>
            .admin-nav > a{
                padding:20px 0;
                background:white;
                text-align: center;
                font-size: 20px;
                text-decoration: none !important;
            }
            .admin-nav > a.active, .admin-nav > a:hover{
                background: #F5F8FA;
                cursor: pointer;
            }
            th{
                text-align:center;
            }
        </style>
@endsection

@section('content')
    @parent
    @include('admin.nav')
    <div class="row" style="padding:15px;">
        <div class="col-md-12">
            <div class="post-filters">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Dashboard</h3>
                    </div>
                    <div class="panel-body table-responsive">
                        <table class="annual-statistics table text-center">
                            <thead>
                                <tr>
                                    <th width="5%">ID</th>
                                    <th>Category</th>
                                    <th>Flagged Content</th>
                                    <th>Reported By</th>
                                    <th width="10%">Reported On</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $flags = App\Flags::get();
                                foreach ($flags as $flag) {
                                ?>
                                <tr>
                                    <td>{{ $flag->id }}</td>
                                    <td>{{ getCat($flag->cat) }}</td>
                                    <td>{{ getPost($flag->cat, $flag->pid) }}</td>
                                    <td>{{ App\User::where('id', $flag->author)->value("name") }}</td>
                                    <td>{{ convertHTMLTime($flag->date) }}</td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
        <script> var SessionToken = "{{ csrf_token() }}"; </script>
        <script src="{{ URL::asset('js/admin.js') }}"></script>
@endsection