@extends('layouts.social')

@section('css')
    @parent
        <style>
            .admin-nav > a{
                padding:20px 0;
                background:white;
                text-align: center;
                font-size: 20px;
                text-decoration: none !important;
            }
            .admin-nav > a.active, .admin-nav > a:hover{
                background: #F5F8FA;
                cursor: pointer;
            }
            .row{

            }
        </style>
@endsection

@section('content')
    @parent
    @include('admin.nav')
    <div class="row" style="padding:15px;">
        <div class="col-md-12">
                        <div class="post-filters">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Dashboard</h3>
        </div>
        <div class="panel-body table-responsive">
                <table class="annual-statistics table text-center">
                    <tbody><tr>
                        <td class="registered">Users Registered</td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\User::where("status",1)->count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">Active</a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\User::where("status",2)->count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">Suspended</a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\User::where("status",3)->count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">Blocked</a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\User::count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-success">Total</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="registered">Groups</td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\Groups::where("active",1)->count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">Active</a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\Groups::where("active",0)->count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">Closed</a>
                                </div>
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\Groups::count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-success">Total</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="registered">Posts Posted</td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\Posts::count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">Posts</a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\GroupPosts::count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">In Groups</a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\Comments::count() + App\GroupPostComments::count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">Comments</a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\Posts::count() + App\GroupPosts::count() + App\Comments::count() + App\GroupPostComments::count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-success">Total</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="registered">Extra</td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\User::where('type',1)->count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">Missionaries</a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\User::where('type',2)->count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">Supporters</a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\User::where('admin',1)->count()}}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">Admins</a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="widget-sales">
                                <div class="no-of-sales">{{ App\Likes::count() }}</div>
                                <div class="sales">
                                    <a href="#" class="text-primary">Likes</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody></table>
                <!--end-table-->
                
        </div>
    </div>
</div>
                    </div>
    </div>
@endsection

@section('js')
    @parent
        <script> var SessionToken = "{{ csrf_token() }}"; </script>
        <script src="{{ URL::asset('js/admin.js') }}"></script>
@endsection