@extends('layouts.social')

@section('css')
    @parent
        <style>
            .admin-nav > a{
                padding:20px 0;
                background:white;
                text-align: center;
                font-size: 20px;
                text-decoration: none !important;
            }
            .admin-nav > a.active, .admin-nav > a:hover{
                background: #F5F8FA;
                cursor: pointer;
            }
            select.btn{
                background-image:none !important;
            }
        </style>
@endsection

@section('content')
    @parent
    @include('admin.nav')
    <div class="row" style="padding:15px;">
        <div class="col-md-12">
            <div class="post-filters">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Dashboard</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table socialite">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                    <th>Status</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Joined</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $users = \App\User::where('type','>', 0)->where('status','=', 1)->orwhere('status','>', 2)->orderBy('id', 'ASC')->get(); 
                                    foreach ($users as $user) {
                                    $profile = \App\Profiles::where('user','=',$user->id)->first(); 
                                ?>
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>@if(isset($profile->username)) {{ "@" . $profile->username }} @endif</td>
                                        <td>
                                        <select user-id="{{ $user->id }}" class="status form-control
                                        @if($user->status==1) btn btn-success @endif
                                        @if($user->status==3) btn btn-warning @endif
                                        @if($user->status==4) btn btn-danger @endif
                                        ">
                                            <option value="1" @if($user->status==1) selected @endif>Active</option>
                                            <option value="3" @if($user->status==3) selected @endif>Suspended</option>
                                            <option value="4" @if($user->status==4) selected @endif>Banned</option>
                                        </select>
                                        </td>
                                        <td class="text-center">{{ getAccountType($user->type) }}</td>
                                        <td class="text-center">{{ convertHTMLTime($user->created_at) }}</td>
                                        <td><button class="btn btn-danger delete" user-id="{{ $user->id }}" title="Delete User"><i class="fa fa-times"></i></button></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <div class="row" style="padding:15px;">
        <div class="col-md-12">
            <div class="post-filters">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Supporters Pending Approval</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table socialite">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                    <th>Joined</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $users = \App\User::where('type','=', 2)->where('status','=', 2)->orderBy('id', 'ASC')->get(); 
                                    foreach ($users as $user) {
                                    $profile = \App\Profiles::where('user','=',$user->id)->first(); 
                                ?>
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>@if(isset($profile->username)) {{ "@" . $profile->username }} @endif</td>
                                        <td>{{ convertHTMLTime($user->created_at) }}</td>
                                        <td>
                                        <select user-id="{{ $user->id }}" class="status form-control">
                                            <option value="2" @if($user->status==2) selected @endif>Pending</option>
                                            <option value="1" @if($user->status==1) selected @endif>Activate</option>
                                        </select>
                                        </td>
                                        <td><button class="btn btn-danger delete" user-id="{{ $user->id }}" title="Delete User"><i class="fa fa-times"></i></button></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding:15px;">
        <div class="col-md-12">
            <div class="post-filters">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Incomplete Users</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table socialite">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                    <th>Joined</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $users = \App\User::where('type','=', 0)->orderBy('id', 'ASC')->get(); 
                                    foreach ($users as $user) {
                                    $profile = \App\Profiles::where('user','=',$user->id)->first(); 
                                ?>
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>@if(isset($profile->username)) {{ "@" . $profile->username }} @endif</td>
                                        <td>{{ convertHTMLTime($user->created_at) }}</td>
                                        
                                        <td><button class="btn btn-danger delete" user-id="{{ $user->id }}" title="Delete User"><i class="fa fa-times"></i></button></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
        <script> var SessionToken = "{{ csrf_token() }}"; </script>
        <script src="{{ URL::asset('js/admin.js') }}"></script>
@endsection