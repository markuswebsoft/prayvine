
            <!-- Edit Post Modal -->
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
                <div class="modal-dialog modal-likes" role="document">
                      <div class="modal-content">
                        <form action="{{ URL('/post/edit') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="postID" value="" id="postID">
                        <input type="hidden" name="r" value="feed" id="r">
                        <div class="panel panel-default panel-post animated" id="post1642">
                            <div class="panel-heading no-bg">
                              <div class="post-author">
                                <div class="user-avatar">
                                    <a href='/profile/{{ $username = \App\Profiles::where("user","=",Auth::user()->id)->value("username") }}'><img src='/uploads/pics/{{ \App\Profiles::where("user","=",Auth::user()->id)->value("pic") }}' alt="Admin" title="Admin"></a>
                                </div>
                                <div class="user-post-details">
                                    <ul class="list-unstyled no-margin">
                                      <li>
                                        <a href='/profile/{{ $username = \App\Profiles::where("user","=",Auth::user()->id)->value("username") }}' class="user-name user">{{ Auth::user()->name }}</a>
                                      </li>
                                      <li>
                                        <time class="post-time timeago" datetime="2016-09-06 17:31:21" title="2016-09-06 17:31:21">
                                          Edit your post
                                        </time>
                                      </li>
                                    </ul>
                                </div>
                              </div>
                            </div>
                          <div class="panel-body">
                              <div class="text-wrapper">
                              <label>Prayer Request Title</label>
                              <input type="text" class="form-control" name="title" id="previousTitle" required="">
                              <br>
                              <label>Post Link (optional)</label>
                              <input type="text" class="form-control" name="link" id="previousLink">
                              <br>
                              <label>Post Message</label>
                                    <textarea name="content" id="previousPost" rows="10" class="form-control" required="">#test</textarea>
                              </div>
                          </div>
                          <div class="panel-footer">
                              <ul class="list-inline pull-right">
                                  <li><button type="submit" class="btn btn-submit btn-success">save changes</button></li>
                              </ul>
                              <div class="clearfix"></div>
                          </div>
                        </div>
                    </form>
                  </div>
                </div>
            </div>
            <!-- Edit Post Modal -->
            <div class="modal fade" id="prayModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
                <div class="modal-dialog modal-likes" role="document">
                      <div class="modal-content">
                        <div class="panel panel-default panel-post animated" id="post1642">
                            <div class="panel-heading no-bg">
                              <div class="post-author">
                                <div class="user-avatar">
                                    <a href='/profile/{{ $username = \App\Profiles::where("user","=",Auth::user()->id)->value("username") }}'><img src='/uploads/pics/{{ \App\Profiles::where("user","=",Auth::user()->id)->value("pic") }}' alt="Admin" title="Admin"></a>
                                </div>
                                <div class="user-post-details">
                                    <ul class="list-unstyled no-margin">
                                      <li>
                                        <a href='/profile/{{ $username = \App\Profiles::where("user","=",Auth::user()->id)->value("username") }}' class="user-name user">{{ Auth::user()->name }}</a>
                                      </li>
                                      <li>
                                        <time class="post-time timeago" datetime="2016-09-06 17:31:21" title="2016-09-06 17:31:21">
                                          Pray for this post
                                        </time>
                                      </li>
                                    </ul>
                                </div>
                              </div>
                            </div>
                          <div class="panel-body">
                              <div class="text-wrapper">
                              <h2 id="prayTitle"></h2>
                              <img id="prayPic" src="" class="" width="100%">
                                    <p id="prayPost" required=""></p>
                              </div>
                          </div>
                          <div class="panel-footer">
                              <ul class="list-inline pull-right">
                                  <li><button type="submit" class="like-post btn btn-success" class="btn btn-submit btn-success like-post" data-post-id="" data-dismiss="modal">I Prayed for This</button></li>
                              </ul>
                              <div class="clearfix"></div>
                          </div>
                        </div>
                  </div>
                </div>
            </div>