
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf_token" content="G7uoecvaZfMJbqosWslbOsVv6P3mW8hDkCGlR7xR"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" />
        <meta name="keywords" content="facebook clone, laravel, live chat, message, news feed, php social network, php social platform, php socialite, post, social, social network, social networking, social platform, social script, socialite">
        <meta name="description" content="Socialite is the FIRST Social networking script developed on Laravel with all enhanced features, Pixel perfect design and extremely user friendly. User interface and user experience are extra added features to Socialite. Months of research, passion and hard work had made the Socialite more flexible, feature-available and very user friendly!">
        <link rel="icon" type="image/x-icon" href="https://socialite.laravelguru.com/setting/favicon.jpg">


        <title>Socialite</title>

        <link media="all" type="text/css" rel="stylesheet" href="https://socialite.laravelguru.com/themes/default/assets/css/style-df9536bfeb.css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
        function SP_source() {
          return "https://socialite.laravelguru.com/";
        }
        var base_url = "https://socialite.laravelguru.com/";
        var theme_url = "https://socialite.laravelguru.com/themes/default/assets/";
        var current_username = "bootstrapguru";
        </script>
        <script src="https://socialite.laravelguru.com/themes/default/assets/js/main-adb4cfc428.js"></script>

            </head>
    <body>
        <nav class="navbar socialite navbar-default no-bg">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-4" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand socialite" href="https://socialite.laravelguru.com">
				<img class="" src="https://socialite.laravelguru.com/setting/logo.jpg" alt="Socialite" title="PrayVine">
			</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-4">
			<form class="navbar-form navbar-left form-left" role="search">
				<div class="input-group no-margin">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
					</span>
					<input type="text" id="navbar-search" data-url="https://socialite.laravelguru.com/api/v1/timelines" class="form-control" placeholder="Search for people, trends, pages and groups">
				</div><!-- /input-group -->
			</form>
			<!-- Collect the nav links, forms, and other content for toggling -->
			
						<ul class="nav navbar-nav navbar-right" id="navbar-right" v-cloak>
				<li>
					<ul class="list-inline notification-list">
						<li class="dropdown message notification">
							<a href="#" data-toggle="dropdown" @click.prevent="showNotifications" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell" aria-hidden="true">
																	</i>
								<span class="small-screen">Notifications</span>
							</a>
							<div class="dropdown-menu">
								<div class="dropdown-menu-header">
									<span class="pull-left">Notifications</span>
									<a v-if="unreadNotifications > 0" class="pull-right" href="#" @click.prevent="markNotificationsRead" >mark all read</a>
									<div class="clearfix"></div>
								</div>
																<ul class="list-unstyled dropdown-messages-list scrollable" data-type="notifications">
									<li class="inbox-message"  v-bind:class="[ !notification.seen ? 'active' : '' ]" v-for="notification in notifications.data">
										<a href="#/notification/{{ notification.id }}">
											<div class="media">
												<div class="media-left">
													<img class="media-object img-icon" v-bind:src="notification.notified_from.avatar" alt="images">
												</div>
												<div class="media-body">
													<h4 class="media-heading">
														<span class="notification-text"> {{ notification.description }} </span>
														<span class="message-time">
															<span class="notification-type"><i class="fa fa-user" aria-hidden="true"></i></span>
															<time class="timeago" datetime="{{ notification.created_at }}" title="{{ notification.created_at }}">
																{{ notification.created_at }}
															</time>
														</span>
													</h4>
												</div>
											</div>
										</a>
									</li>
									<li v-if="notificationsLoading" class="dropdown-loading">
										<i class="fa fa-spin fa-spinner"></i>
									</li>
								</ul>
																<div class="dropdown-menu-footer"><br>
									
								</div>
							</div>
						</li>
						<li class="dropdown message largescreen-message">
							<a href="#" data-toggle="dropdown" @click="showConversations" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-comments" aria-hidden="true">
									<span class="count" v-if="unreadConversations" >{{ unreadConversations }}</span>
								</i>
								<span class="small-screen">messages</span>
							</a>
							<div class="dropdown-menu">
								<div class="dropdown-menu-header">
									<span class="pull-left">messages</span>
									<div class="clearfix"></div>
								</div>
								<div class="no-messages hidden">
									<i class="fa fa-commenting-o" aria-hidden="true"></i>
									<p>You don&#039;t have any messages</p>
								</div>
								<ul class="list-unstyled dropdown-messages-list scrollable" data-type="messages">
									<li class="inbox-message" v-for="conversation in conversations.data">
										<a href="#" onclick="chatBoxes.sendMessage({{ conversation.user.id }})">
											<div class="media">
												<div class="media-left">
													<img class="media-object img-icon" v-bind:src="conversation.user.avatar" alt="images">
												</div>
												<div class="media-body">
													<h4 class="media-heading">
														<span class="message-heading">{{ conversation.user.name }}</span> 
														<span class="online-status hidden"></span>
														<time class="timeago message-time" datetime="{{ conversation.lastMessage.created_at }}" title="{{ conversation.lastMessage.created_at }}">
															{{ conversation.lastMessage.created_at }}
														</h4>
														<p class="message-text">
															{{ conversation.lastMessage.body }}
														</p>
													</div>
												</div>
											</a>
										</li>
										<li v-if="conversationsLoading" class="dropdown-loading">
											<i class="fa fa-spin fa-spinner"></i>
										</li>
									</ul>
									<div class="dropdown-menu-footer">
										<a href="https://socialite.laravelguru.com/messages">See all</a>
									</div>
								</div>
							</li>
							<li class="smallscreen-message">
								<a href="https://socialite.laravelguru.com/messages">
									<i class="fa fa-comments" aria-hidden="true">
										<span class="count" v-if="unreadConversations" >{{ unreadConversations }}</span>
									</i>
									<span class="small-screen">messages</span>
								</a>
							</li>
							<li class="chat-list-toggle">
								<a href="#"><i class="fa fa-users" aria-hidden="true"></i><span class="small-screen">chat-list</span></a>
							</li>
						</ul>
					</li>
					<li class="dropdown user-image socialite">
						<a href="#" class="dropdown-toggle no-padding" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" class="img-radius img-30" title="Admin">

							<span class="user-name">Admin</span><i class="fa fa-angle-down" aria-hidden="true"></i></a>
							<ul class="dropdown-menu">
																<li class=""><a href="https://socialite.laravelguru.com/admin"><i class="fa fa-user-secret" aria-hidden="true"></i>Admin</a></li>
																<li class="active"><a href="#"><i class="fa fa-user" aria-hidden="true"></i>my profile</a></li>

								<li class=""><a href="#/pages-groups"><i class="fa fa-bars" aria-hidden="true"></i>My Pages &amp; Groups</a></li>

								<li class=""><a href="#/settings/general"><i class="fa fa-cog" aria-hidden="true"></i>settings</a></li>

								<li><a href="https://socialite.laravelguru.com/logout"><i class="fa fa-unlock" aria-hidden="true"></i>Logout</a></li>
							</ul>
						</li>
	               <!--  <li class="logout">
	                    <a href="https://socialite.laravelguru.com/logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
	                </li> -->
	            </ul>
	            	        </div><!-- /.navbar-collapse -->
	    </div><!-- /.container-fluid -->
	</nav>	


	



        <div class="main-content">
            <!-- main-section -->

	<div class="container">
		<div class="row">				 
			<div class="col-md-10">
								<div class="timeline-cover-section">
	<div class="timeline-cover">
		<img src="  https://socialite.laravelguru.com/user/cover/2016-08-26-14-42-30despicable-me-2-evil-minion-wallpaper.jpg " alt="Admin" title="Admin">
					<a href="#" class="btn btn-camera-cover change-cover"><i class="fa fa-camera" aria-hidden="true"></i><span class="change-cover-text">Change cover</span></a>
				<div class="user-cover-progress hidden">

		</div>
			<!-- <div class="cover-bottom">
		</div> -->
		<div class="user-timeline-name">
			<a href="#">Admin</a>
							<span class="verified-badge bg-success">
					<i class="fa fa-check"></i>
				</span>
					</div>
		</div>
	<div class="timeline-list">
		<ul class="list-inline pagelike-links">							
							<li class=""><a href="#/posts" ><span class="top-list">630 Posts</span></a></li>
			
			<li class=""><a href="#/following" ><span class="top-list">47 following</span></a></li>
			<li class=""><a href="#/followers" ><span class="top-list">16  followers</span></a></li>
			<li class=""><a href="#/liked-pages" ><span class="top-list">9 liked pages</span></a></li>
			<li class=""><a href="#/joined-groups" ><span class="top-list">13  joined groups</span></a></li>

							<li class=""><a href="#/follow-requests" ><span class="top-list">0 Follow requests</span></a></li>
			
						

			</ul>
			<div class="status-button">
					<a href="#" class="btn btn-status">Status</a>
			</div>
			<div class="timeline-user-avtar">

				<img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin">
									<div class="chang-user-avatar">
						<a href="#" class="btn btn-camera change-avatar"><i class="fa fa-camera" aria-hidden="true"></i><span class="avatar-text">update profile<span>picture</span></span></a>
					</div>
							
				<div class="user-avatar-progress hidden">
				</div>
			</div><!-- /timeline-user-avatar -->

		</div><!-- /timeline-list -->
	</div><!-- timeline-cover-section -->



				
				<div class="row">
					<div class="timeline">
						<div class="col-md-4">
														<div class="user-profile-buttons">
	<div class="row follow-links pagelike-links">
		<!-- This [if-1] is for checking current user timeline or diff user timeline -->	
				<div class="col-md-12"><a href="#/settings/general" class="btn btn-profile"><i class="fa fa-pencil-square-o"></i>Edit profile</a></div>
		 <!-- End of [if-1]-->

	</div>
</div>

<div class="user-bio-block">
	<div class="bio-header">Bio</div>
	<div class="bio-description">
		Some text about me
	</div>
	<ul class="bio-list list-unstyled">
				<li>
			<i class="fa fa-map-marker" aria-hidden="true"></i><span>Lives in India</span>
		</li>
		
				<li><i class="fa fa-building-o"></i><span>From Hyderabad</span></li>
		
				<li><i class="fa fa-calendar"></i><span>

			Born on January 03

		</span></li>
			</ul>
</div>

<!-- Change avatar form -->
<form class="change-avatar-form hidden" action="https://socialite.laravelguru.com/ajax/change-avatar" method="post" enctype="multipart/form-data">
	<input name="timeline_id" value="1" type="hidden">
	<input name="timeline_type" value="user" type="hidden">
	<input class="change-avatar-input hidden" accept="image/jpeg,image/png" type="file" name="change_avatar" >
</form>

<!-- Change cover form -->
<form class="change-cover-form hidden" action="https://socialite.laravelguru.com/ajax/change-cover" method="post" enctype="multipart/form-data">
	<input name="timeline_id" value="1" type="hidden">
	<input name="timeline_type" value="user" type="hidden">
	<input class="change-cover-input hidden" accept="image/jpeg,image/png" type="file" name="change_cover" >
</form>

	<!-- my-pages -->
	<div class="widget-pictures widget-best-pictures">
		<div class="picture pull-left">
			Pages
		</div>
		<div class="clearfix"></div>
		<div class="best-pictures my-best-pictures">
			<div class="row">
								<div class="alert alert-warning">No pages added yet!</div>
							</div><!-- /row -->
		</div>
	</div>
	<!-- /my pages -->

	<!-- my-groups -->
	<div class="widget-pictures widget-best-pictures">

		<div class="picture pull-left">
			Groups
		</div>
		<div class="clearfix"></div>
		<div class="best-pictures my-best-pictures">
			<div class="row">
				
								<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
					<a href="https://socialite.laravelguru.com/mygroup" class="image-hover" title="My news" data-toggle="tooltip" data-placement="top">
						<img src="  https://socialite.laravelguru.com/group/avatar/2016-08-19-17-18-13announcement.png  " alt="My news" title="My news" >
					</a>
				</div>
								<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
					<a href="https://socialite.laravelguru.com/myfamily" class="image-hover" title="My family" data-toggle="tooltip" data-placement="top">
						<img src="  https://socialite.laravelguru.com/group/avatar/2016-08-19-17-24-25viral-marketing.png  " alt="My family" title="My family" >
					</a>
				</div>
								<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
					<a href="https://socialite.laravelguru.com/ThinkDifferent" class="image-hover" title="Think Different" data-toggle="tooltip" data-placement="top">
						<img src="  https://socialite.laravelguru.com/group/avatar/2016-08-19-17-47-27think.jpg  " alt="Think Different" title="Think Different" >
					</a>
				</div>
								<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
					<a href="https://socialite.laravelguru.com/fish" class="image-hover" title="fish" data-toggle="tooltip" data-placement="top">
						<img src="  https://socialite.laravelguru.com/group/avatar/default-group-avatar.png  " alt="fish" title="fish" >
					</a>
				</div>
								<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
					<a href="https://socialite.laravelguru.com/gustavo" class="image-hover" title="portal wedding" data-toggle="tooltip" data-placement="top">
						<img src="  https://socialite.laravelguru.com/group/avatar/default-group-avatar.png  " alt="portal wedding" title="portal wedding" >
					</a>
				</div>
								<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
					<a href="https://socialite.laravelguru.com/test123" class="image-hover" title="TestTest" data-toggle="tooltip" data-placement="top">
						<img src="  https://socialite.laravelguru.com/group/avatar/default-group-avatar.png  " alt="TestTest" title="TestTest" >
					</a>
				</div>
								<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
					<a href="https://socialite.laravelguru.com/asdfff" class="image-hover" title="asd" data-toggle="tooltip" data-placement="top">
						<img src="  https://socialite.laravelguru.com/group/avatar/default-group-avatar.png  " alt="asd" title="asd" >
					</a>
				</div>
								<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
					<a href="https://socialite.laravelguru.com/k0d0k" class="image-hover" title="pwned hckd by r00t@Kiddizer" data-toggle="tooltip" data-placement="top">
						<img src="  https://socialite.laravelguru.com/group/avatar/default-group-avatar.png  " alt="pwned hckd by r00t@Kiddizer" title="pwned hckd by r00t@Kiddizer" >
					</a>
				</div>
								<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
					<a href="https://socialite.laravelguru.com/myGraduatedPro" class="image-hover" title="Graduated Project" data-toggle="tooltip" data-placement="top">
						<img src="  https://socialite.laravelguru.com/group/avatar/default-group-avatar.png  " alt="Graduated Project" title="Graduated Project" >
					</a>
				</div>
								<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
					<a href="https://socialite.laravelguru.com/wedwed" class="image-hover" title="wedwed" data-toggle="tooltip" data-placement="top">
						<img src="  https://socialite.laravelguru.com/group/avatar/default-group-avatar.png  " alt="wedwed" title="wedwed" >
					</a>
				</div>
								<div class="col-md-2 col-sm-2 col-xs-2 best-pics">
					<a href="https://socialite.laravelguru.com/porno" class="image-hover" title="Porn" data-toggle="tooltip" data-placement="top">
						<img src="  https://socialite.laravelguru.com/group/avatar/default-group-avatar.png  " alt="Porn" title="Porn" >
					</a>
				</div>
								
		</div><!-- /row -->

		</div>
	</div>
	<!-- /my pages -->
	
















													</div>

						<!-- Post box on timeline,page,group -->
						<div class="col-md-8">

															<form action="https://socialite.laravelguru.com" method="post" class="create-post-form">
  <input type="hidden" name="_token" value="G7uoecvaZfMJbqosWslbOsVv6P3mW8hDkCGlR7xR">

    <div class="panel panel-default panel-create"> <!-- panel-create -->
        <div class="panel-heading">
            <div class="heading-text">
                What&#039;s going on?
            </div>
        </div>
        <div class="panel-body">        
            <textarea name="description" class="form-control createpost-form comment" cols="30" rows="3" id="createPost" cols="30" rows="2" placeholder="Write something.....#hashtags @mentions"></textarea>
               

                <div class="user-tags-added" style="display:none">
                    &nbsp; -- with
                    <div class="user-tag-names">
                        
                    </div>
                </div>
                <div class="user-tags-addon post-addon" style="display: none">
                    <span class="post-addon-icon"><i class="fa fa-user-plus"></i></span>
                    <div class="form-group">
                        <input type="text" id="userTags" class="form-control user-tags youtube-text" placeholder="Who are you with?" autocomplete="off" value="" >
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="users-results-wrapper"></div>
                <div class="youtube-iframe"></div>
        
                <div class="video-addon post-addon" style="display: none">
                    <span class="post-addon-icon"><i class="fa fa-film"></i></span>
                    <div class="form-group">
                        <input type="text" name="youtubeText" id="youtubeText" class="form-control youtube-text" placeholder="What are you watching?"  value="" >
                        <div class="clearfix"></div>
                    </div>
                </div>
              <div class="music-addon post-addon" style="display: none">
                  <span class="post-addon-icon"><i class="fa fa-music" aria-hidden="true"></i></span>
                 <div class="form-group">
                    <input type="text" name="soundCloudText" autocomplete="off" id ="soundCloudText" class="form-control youtube-text" placeholder="What are you listening to?"  value="" >
                    <div class="clearfix"></div>
                 </div>
              </div>
              <div class="soundcloud-results-wrapper">

              </div>
            <div class="location-addon post-addon" style="display: none">
                  <span class="post-addon-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                 <div class="form-group">
                    <input type="text" name="location" id="pac-input" class="form-control" placeholder="Where are you?"  autocomplete="off" value="" onKeyPress="return initMap(event)"><div class="clearfix"></div>
                 </div>                 
            </div>
              <div class="emoticons-wrapper  post-addon" style="display:none">
                  
              </div>
              <div class="images-selected post-images-selected" style="display:none">
                  <span>3</span> photo(s) selected
              </div>
              <!-- Hidden elements  -->
              <input type="hidden" name="timeline_id" value="1">
              <input type="hidden" name="youtube_title" value="">
              <input type="hidden" name="youtube_video_id" value="">
              <input type="hidden" name="locatio" value="">
              <input type="hidden" name="soundcloud_id" value="">
              <input type="hidden" name="user_tags" value="">
              <input type="hidden" name="soundcloud_title" value="">
              <input type="file"   class="post-images-upload hidden" multiple="multiple"  accept="image/jpeg,image/png,image/gif" name="post_images_upload[]" >
              <div id="post-image-holder"></div>
        </div><!-- panel-body -->

        <div class="panel-footer">
            <ul class="list-inline left-list">
                <li><a href="#" id="addUserTags"><i class="fa fa-user-plus"></i></a></li>
                <li><a href="#" id="imageUpload"><i class="fa fa-camera-retro"></i></a></li>
                <li><a href="#" id="musicUpload"><i class="fa fa-music"></i></a></li>
                <li><a href="#" id="videoUpload"><i class="fa fa-film"></i></a></li>
                <li><a href="#" id="locationUpload"><i class="fa fa-map-marker"></i></a></li>
                <li><a href="#" id="emoticons"><i class="fa fa-smile-o"></i></a></li>
            </ul>
            <ul class="list-inline right-list">
                <li><button type="submit" class="btn btn-submit btn-success">post</button></li>
            </ul>

            <div class="clearfix"></div>
        </div>
    </div>
</form>




<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_vuWi_hzMDDeenNYwaNAj0PHzzS2GAx8&libraries=places&callback=initMap"
        async defer></script>

<script>
function initMap(event) 
{    
    var key;  
    var map = new google.maps.Map(document.getElementById('pac-input'), {
    });

    var input = /** @type  {!HTMLInputElement} */(
        document.getElementById('pac-input'));        

    if(window.event)
    {
        key = window.event.keyCode; 

    }
    else 
    {
        if(event)
            key = event.which;      
    }       

    if(key == 13){       
    //do nothing 
    return false;       
    //otherwise 
    } else { 
        var autocomplete = new google.maps.places.Autocomplete(input);  
        autocomplete.bindTo('bounds', map);

    //continue as normal (allow the key press for keys other than "enter") 
    return true; 
    } 
}
</script>






												

							<div class="timeline-posts">
																		 																		
	 										<div class="panel panel-default panel-post animated" id="post1209">
  <div class="panel-heading no-bg">
    <div class="post-author">
      <div class="post-options">
        <ul class="list-inline no-margin">
          <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
                            <li class="main-link">
                <a href="#" data-post-id="1209" class="notify-user unnotify">
                  <i class="fa  fa-bell-slash" aria-hidden="true"></i>stop notifications
                  <span class="small-text">You will not be notified</span>
                </a>
              </li>
              <li class="main-link hidden">
                <a href="#" data-post-id="1209" class="notify-user notify">
                  <i class="fa fa-bell" aria-hidden="true"></i>get notifications
                  <span class="small-text">You will be notified for likes,comments and shares</span>
                </a>
              </li>
                            
                            <li class="main-link">
                <a href="#" data-post-id="1209" class="edit-post">
                  <i class="fa fa-edit" aria-hidden="true"></i>Edit
                  <span class="small-text">You can edit your post</span>
                </a>
              </li>
              
                            <li class="main-link">
                <a href="#" class="delete-post" data-post-id="1209">
                  <i class="fa fa-trash" aria-hidden="true"></i>delete
                  <span class="small-text">This post will be deleted</span>
                </a>
              </li>
              
                            
            </ul>
          </li>
        </ul>
      </div>
      <div class="user-avatar">
        <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
      </div>
      <div class="user-post-details">
        <ul class="list-unstyled no-margin">
          <li>
            <a href="#" class="user-name user">Admin</a>
                      </li>
          <li>
            <time class="post-time timeago" datetime="2016-09-02 07:28:23" title="2016-09-02 07:28:23">
              2016-09-02 07:28:23
            </time>
                      </ul>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="text-wrapper">
        <p>http://www.theroot.com/articles/politics/2016/09/donald-trump-meets-with-mexican-president-enrique-pea-nieto-and-the-joke-is-on-all-of-us/</p>
        <div class="post-image-holder  ">
                  </div>
      </div>
                  <ul class="actions-count list-inline">
        
                
                
                

      </ul>
    </div>

    
    <div class="panel-footer socialite">
      <ul class="list-inline footer-list">
                
          <li><a href="#" class="like-post like-1209" data-post-id="1209"><i class="fa fa-thumbs-o-up"></i>like</a></li>

          <li class="hidden"><a href="#" class="like-post unlike-1209" data-post-id="1209"><i class="fa fa-thumbs-o-down"></i></i>unlike</a></li>
                <li><a href="#" class="show-comments"><i class="fa fa-comment-o"></i>comment</a></li>

                
      </ul>
    </div>

          <div class="comments-section all_comments" style="display:none">
        <div class="comments-wrapper">         
          <div class="to-comment">  <!-- to-comment -->
                        <div class="commenter-avatar">
              <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
            </div>
            <div class="comment-textfield">
              <form action="#" class="comment-form">
                <input class="form-control post-comment"  autocomplete="off" data-post-id="1209" name="post_comment" placeholder="Write a comment...Press Enter to Post" >
                <ul class="list-inline meme-reply hidden">
                  <li><a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
                </ul>
              </form>
            </div>
            <div class="clearfix"></div>
              
          </div><!-- to-comment -->

          <div class="comments post-comments-list"> <!-- comments/main-comment  -->
                      </div><!-- comments/main-comment  -->            
        </div>        
      </div><!-- /comments-section -->
      </div>


  
  <!-- Modal Ends here -->
    <a class="jscroll-next hidden" href="https://socialite.laravelguru.com/ajax/get-more-posts?page=2&amp;username=bootstrapguru">get more posts</a>
  
  
					
	 																		
	 										<div class="panel panel-default panel-post animated" id="post1208">
  <div class="panel-heading no-bg">
    <div class="post-author">
      <div class="post-options">
        <ul class="list-inline no-margin">
          <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
                            <li class="main-link">
                <a href="#" data-post-id="1208" class="notify-user unnotify">
                  <i class="fa  fa-bell-slash" aria-hidden="true"></i>stop notifications
                  <span class="small-text">You will not be notified</span>
                </a>
              </li>
              <li class="main-link hidden">
                <a href="#" data-post-id="1208" class="notify-user notify">
                  <i class="fa fa-bell" aria-hidden="true"></i>get notifications
                  <span class="small-text">You will be notified for likes,comments and shares</span>
                </a>
              </li>
                            
                            <li class="main-link">
                <a href="#" data-post-id="1208" class="edit-post">
                  <i class="fa fa-edit" aria-hidden="true"></i>Edit
                  <span class="small-text">You can edit your post</span>
                </a>
              </li>
              
                            <li class="main-link">
                <a href="#" class="delete-post" data-post-id="1208">
                  <i class="fa fa-trash" aria-hidden="true"></i>delete
                  <span class="small-text">This post will be deleted</span>
                </a>
              </li>
              
                            
            </ul>
          </li>
        </ul>
      </div>
      <div class="user-avatar">
        <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
      </div>
      <div class="user-post-details">
        <ul class="list-unstyled no-margin">
          <li>
            <a href="#" class="user-name user">Admin</a>
                      </li>
          <li>
            <time class="post-time timeago" datetime="2016-09-02 06:23:28" title="2016-09-02 06:23:28">
              2016-09-02 06:23:28
            </time>
                      </ul>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="text-wrapper">
        <p></p>
        <div class="post-image-holder   single-image ">
                    <a href="https://socialite.laravelguru.com/user/gallery/2016-09-02-06-23-2814045956_126755167771458_7569461110514096473_n.jpg" data-lightbox="imageGallery.1208" ><img src="https://socialite.laravelguru.com/user/gallery/2016-09-02-06-23-2814045956_126755167771458_7569461110514096473_n.jpg"  title="Admin" alt="Admin"></a>
                  </div>
      </div>
                  <ul class="actions-count list-inline">
        
                
                
                

      </ul>
    </div>

    
    <div class="panel-footer socialite">
      <ul class="list-inline footer-list">
                
          <li><a href="#" class="like-post like-1208" data-post-id="1208"><i class="fa fa-thumbs-o-up"></i>like</a></li>

          <li class="hidden"><a href="#" class="like-post unlike-1208" data-post-id="1208"><i class="fa fa-thumbs-o-down"></i></i>unlike</a></li>
                <li><a href="#" class="show-comments"><i class="fa fa-comment-o"></i>comment</a></li>

                
      </ul>
    </div>

          <div class="comments-section all_comments" style="display:none">
        <div class="comments-wrapper">         
          <div class="to-comment">  <!-- to-comment -->
                        <div class="commenter-avatar">
              <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
            </div>
            <div class="comment-textfield">
              <form action="#" class="comment-form">
                <input class="form-control post-comment"  autocomplete="off" data-post-id="1208" name="post_comment" placeholder="Write a comment...Press Enter to Post" >
                <ul class="list-inline meme-reply hidden">
                  <li><a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
                </ul>
              </form>
            </div>
            <div class="clearfix"></div>
              
          </div><!-- to-comment -->

          <div class="comments post-comments-list"> <!-- comments/main-comment  -->
                      </div><!-- comments/main-comment  -->            
        </div>        
      </div><!-- /comments-section -->
      </div>


  
  <!-- Modal Ends here -->
    <a class="jscroll-next hidden" href="https://socialite.laravelguru.com/ajax/get-more-posts?page=2&amp;username=bootstrapguru">get more posts</a>
  
  
					
	 																		
	 										<div class="panel panel-default panel-post animated" id="post1207">
  <div class="panel-heading no-bg">
    <div class="post-author">
      <div class="post-options">
        <ul class="list-inline no-margin">
          <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
                            <li class="main-link hidden">
                <a href="#" data-post-id="1207" class="notify-user unnotify">
                  <i class="fa  fa-bell-slash" aria-hidden="true"></i>stop notifications
                  <span class="small-text">You will not be notified</span>
                </a>
              </li>
              <li class="main-link">
                <a href="#" data-post-id="1207" class="notify-user notify">
                  <i class="fa fa-bell" aria-hidden="true"></i>get notifications
                  <span class="small-text">You will be notified for likes,comments and shares</span>
                </a>
              </li>
                            
                            <li class="main-link">
                <a href="#" data-post-id="1207" class="edit-post">
                  <i class="fa fa-edit" aria-hidden="true"></i>Edit
                  <span class="small-text">You can edit your post</span>
                </a>
              </li>
              
                            <li class="main-link">
                <a href="#" class="delete-post" data-post-id="1207">
                  <i class="fa fa-trash" aria-hidden="true"></i>delete
                  <span class="small-text">This post will be deleted</span>
                </a>
              </li>
              
                            
            </ul>
          </li>
        </ul>
      </div>
      <div class="user-avatar">
        <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
      </div>
      <div class="user-post-details">
        <ul class="list-unstyled no-margin">
          <li>
            <a href="#" class="user-name user">Admin</a>
                      </li>
          <li>
            <time class="post-time timeago" datetime="2016-09-02 05:35:04" title="2016-09-02 05:35:04">
              2016-09-02 05:35:04
            </time>
                      </ul>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="text-wrapper">
        <p>Hello!!! :)</p>
        <div class="post-image-holder  ">
                  </div>
      </div>
                  <ul class="actions-count list-inline">
        
                
                
                

      </ul>
    </div>

    
    <div class="panel-footer socialite">
      <ul class="list-inline footer-list">
                
          <li><a href="#" class="like-post like-1207" data-post-id="1207"><i class="fa fa-thumbs-o-up"></i>like</a></li>

          <li class="hidden"><a href="#" class="like-post unlike-1207" data-post-id="1207"><i class="fa fa-thumbs-o-down"></i></i>unlike</a></li>
                <li><a href="#" class="show-comments"><i class="fa fa-comment-o"></i>comment</a></li>

                
      </ul>
    </div>

          <div class="comments-section all_comments" style="display:none">
        <div class="comments-wrapper">         
          <div class="to-comment">  <!-- to-comment -->
                        <div class="commenter-avatar">
              <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
            </div>
            <div class="comment-textfield">
              <form action="#" class="comment-form">
                <input class="form-control post-comment"  autocomplete="off" data-post-id="1207" name="post_comment" placeholder="Write a comment...Press Enter to Post" >
                <ul class="list-inline meme-reply hidden">
                  <li><a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
                </ul>
              </form>
            </div>
            <div class="clearfix"></div>
              
          </div><!-- to-comment -->

          <div class="comments post-comments-list"> <!-- comments/main-comment  -->
                      </div><!-- comments/main-comment  -->            
        </div>        
      </div><!-- /comments-section -->
      </div>


  
  <!-- Modal Ends here -->
    <a class="jscroll-next hidden" href="https://socialite.laravelguru.com/ajax/get-more-posts?page=2&amp;username=bootstrapguru">get more posts</a>
  
  
					
	 																		
	 										<div class="panel panel-default panel-post animated" id="post1206">
  <div class="panel-heading no-bg">
    <div class="post-author">
      <div class="post-options">
        <ul class="list-inline no-margin">
          <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
                            <li class="main-link">
                <a href="#" data-post-id="1206" class="notify-user unnotify">
                  <i class="fa  fa-bell-slash" aria-hidden="true"></i>stop notifications
                  <span class="small-text">You will not be notified</span>
                </a>
              </li>
              <li class="main-link hidden">
                <a href="#" data-post-id="1206" class="notify-user notify">
                  <i class="fa fa-bell" aria-hidden="true"></i>get notifications
                  <span class="small-text">You will be notified for likes,comments and shares</span>
                </a>
              </li>
                            
                            <li class="main-link">
                <a href="#" data-post-id="1206" class="edit-post">
                  <i class="fa fa-edit" aria-hidden="true"></i>Edit
                  <span class="small-text">You can edit your post</span>
                </a>
              </li>
              
                            <li class="main-link">
                <a href="#" class="delete-post" data-post-id="1206">
                  <i class="fa fa-trash" aria-hidden="true"></i>delete
                  <span class="small-text">This post will be deleted</span>
                </a>
              </li>
              
                            
            </ul>
          </li>
        </ul>
      </div>
      <div class="user-avatar">
        <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
      </div>
      <div class="user-post-details">
        <ul class="list-unstyled no-margin">
          <li>
            <a href="#" class="user-name user">Admin</a>
                      </li>
          <li>
            <time class="post-time timeago" datetime="2016-09-02 05:33:51" title="2016-09-02 05:33:51">
              2016-09-02 05:33:51
            </time>
                      </ul>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="text-wrapper">
        <p>hi</p>
        <div class="post-image-holder  ">
                  </div>
      </div>
                  <ul class="actions-count list-inline">
        
                
                
                

      </ul>
    </div>

    
    <div class="panel-footer socialite">
      <ul class="list-inline footer-list">
                
          <li><a href="#" class="like-post like-1206" data-post-id="1206"><i class="fa fa-thumbs-o-up"></i>like</a></li>

          <li class="hidden"><a href="#" class="like-post unlike-1206" data-post-id="1206"><i class="fa fa-thumbs-o-down"></i></i>unlike</a></li>
                <li><a href="#" class="show-comments"><i class="fa fa-comment-o"></i>comment</a></li>

                
      </ul>
    </div>

          <div class="comments-section all_comments" style="display:none">
        <div class="comments-wrapper">         
          <div class="to-comment">  <!-- to-comment -->
                        <div class="commenter-avatar">
              <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
            </div>
            <div class="comment-textfield">
              <form action="#" class="comment-form">
                <input class="form-control post-comment"  autocomplete="off" data-post-id="1206" name="post_comment" placeholder="Write a comment...Press Enter to Post" >
                <ul class="list-inline meme-reply hidden">
                  <li><a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
                </ul>
              </form>
            </div>
            <div class="clearfix"></div>
              
          </div><!-- to-comment -->

          <div class="comments post-comments-list"> <!-- comments/main-comment  -->
                      </div><!-- comments/main-comment  -->            
        </div>        
      </div><!-- /comments-section -->
      </div>


  
  <!-- Modal Ends here -->
    <a class="jscroll-next hidden" href="https://socialite.laravelguru.com/ajax/get-more-posts?page=2&amp;username=bootstrapguru">get more posts</a>
  
  
					
	 																		
	 										<div class="panel panel-default panel-post animated" id="post1203">
  <div class="panel-heading no-bg">
    <div class="post-author">
      <div class="post-options">
        <ul class="list-inline no-margin">
          <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
                            <li class="main-link">
                <a href="#" data-post-id="1203" class="notify-user unnotify">
                  <i class="fa  fa-bell-slash" aria-hidden="true"></i>stop notifications
                  <span class="small-text">You will not be notified</span>
                </a>
              </li>
              <li class="main-link hidden">
                <a href="#" data-post-id="1203" class="notify-user notify">
                  <i class="fa fa-bell" aria-hidden="true"></i>get notifications
                  <span class="small-text">You will be notified for likes,comments and shares</span>
                </a>
              </li>
                            
                            <li class="main-link">
                <a href="#" data-post-id="1203" class="edit-post">
                  <i class="fa fa-edit" aria-hidden="true"></i>Edit
                  <span class="small-text">You can edit your post</span>
                </a>
              </li>
              
                            <li class="main-link">
                <a href="#" class="delete-post" data-post-id="1203">
                  <i class="fa fa-trash" aria-hidden="true"></i>delete
                  <span class="small-text">This post will be deleted</span>
                </a>
              </li>
              
                            
            </ul>
          </li>
        </ul>
      </div>
      <div class="user-avatar">
        <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
      </div>
      <div class="user-post-details">
        <ul class="list-unstyled no-margin">
          <li>
            <a href="#" class="user-name user">Admin</a>
                      </li>
          <li>
            <time class="post-time timeago" datetime="2016-09-02 04:47:06" title="2016-09-02 04:47:06">
              2016-09-02 04:47:06
            </time>
                      </ul>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="text-wrapper">
        <p>https://www.instagram.com/bilginninja/</p>
        <div class="post-image-holder  ">
                    <a href="https://socialite.laravelguru.com/user/gallery/2016-09-02-04-47-06enhanced-9503-1472245674-9.png" data-lightbox="imageGallery.1203" ><img src="https://socialite.laravelguru.com/user/gallery/2016-09-02-04-47-06enhanced-9503-1472245674-9.png"  title="Admin" alt="Admin"></a>
                    <a href="https://socialite.laravelguru.com/user/gallery/2016-09-02-04-47-07enhanced-14994-1472230145-20.jpg" data-lightbox="imageGallery.1203" ><img src="https://socialite.laravelguru.com/user/gallery/2016-09-02-04-47-07enhanced-14994-1472230145-20.jpg"  title="Admin" alt="Admin"></a>
                  </div>
      </div>
                  <ul class="actions-count list-inline">
        
                
                
                

      </ul>
    </div>

    
    <div class="panel-footer socialite">
      <ul class="list-inline footer-list">
                
          <li><a href="#" class="like-post like-1203" data-post-id="1203"><i class="fa fa-thumbs-o-up"></i>like</a></li>

          <li class="hidden"><a href="#" class="like-post unlike-1203" data-post-id="1203"><i class="fa fa-thumbs-o-down"></i></i>unlike</a></li>
                <li><a href="#" class="show-comments"><i class="fa fa-comment-o"></i>comment</a></li>

                
      </ul>
    </div>

          <div class="comments-section all_comments" style="display:none">
        <div class="comments-wrapper">         
          <div class="to-comment">  <!-- to-comment -->
                        <div class="commenter-avatar">
              <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
            </div>
            <div class="comment-textfield">
              <form action="#" class="comment-form">
                <input class="form-control post-comment"  autocomplete="off" data-post-id="1203" name="post_comment" placeholder="Write a comment...Press Enter to Post" >
                <ul class="list-inline meme-reply hidden">
                  <li><a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
                </ul>
              </form>
            </div>
            <div class="clearfix"></div>
              
          </div><!-- to-comment -->

          <div class="comments post-comments-list"> <!-- comments/main-comment  -->
                      </div><!-- comments/main-comment  -->            
        </div>        
      </div><!-- /comments-section -->
      </div>


  
  <!-- Modal Ends here -->
    <a class="jscroll-next hidden" href="https://socialite.laravelguru.com/ajax/get-more-posts?page=2&amp;username=bootstrapguru">get more posts</a>
  
  
					
	 																		
	 										<div class="panel panel-default panel-post animated" id="post1202">
  <div class="panel-heading no-bg">
    <div class="post-author">
      <div class="post-options">
        <ul class="list-inline no-margin">
          <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
                            <li class="main-link">
                <a href="#" data-post-id="1202" class="notify-user unnotify">
                  <i class="fa  fa-bell-slash" aria-hidden="true"></i>stop notifications
                  <span class="small-text">You will not be notified</span>
                </a>
              </li>
              <li class="main-link hidden">
                <a href="#" data-post-id="1202" class="notify-user notify">
                  <i class="fa fa-bell" aria-hidden="true"></i>get notifications
                  <span class="small-text">You will be notified for likes,comments and shares</span>
                </a>
              </li>
                            
                            <li class="main-link">
                <a href="#" data-post-id="1202" class="edit-post">
                  <i class="fa fa-edit" aria-hidden="true"></i>Edit
                  <span class="small-text">You can edit your post</span>
                </a>
              </li>
              
                            <li class="main-link">
                <a href="#" class="delete-post" data-post-id="1202">
                  <i class="fa fa-trash" aria-hidden="true"></i>delete
                  <span class="small-text">This post will be deleted</span>
                </a>
              </li>
              
                            
            </ul>
          </li>
        </ul>
      </div>
      <div class="user-avatar">
        <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
      </div>
      <div class="user-post-details">
        <ul class="list-unstyled no-margin">
          <li>
            <a href="#" class="user-name user">Admin</a>
                      </li>
          <li>
            <time class="post-time timeago" datetime="2016-09-02 04:45:41" title="2016-09-02 04:45:41">
              2016-09-02 04:45:41
            </time>
                      </ul>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="text-wrapper">
        <p></p>
        <div class="post-image-holder  ">
                  </div>
      </div>
                  <div class="soundcloud-wrapper">
        <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/217676148&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
      </div>
            <ul class="actions-count list-inline">
        
                
                
                

      </ul>
    </div>

    
    <div class="panel-footer socialite">
      <ul class="list-inline footer-list">
                
          <li><a href="#" class="like-post like-1202" data-post-id="1202"><i class="fa fa-thumbs-o-up"></i>like</a></li>

          <li class="hidden"><a href="#" class="like-post unlike-1202" data-post-id="1202"><i class="fa fa-thumbs-o-down"></i></i>unlike</a></li>
                <li><a href="#" class="show-comments"><i class="fa fa-comment-o"></i>comment</a></li>

                
      </ul>
    </div>

          <div class="comments-section all_comments" style="display:none">
        <div class="comments-wrapper">         
          <div class="to-comment">  <!-- to-comment -->
                        <div class="commenter-avatar">
              <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
            </div>
            <div class="comment-textfield">
              <form action="#" class="comment-form">
                <input class="form-control post-comment"  autocomplete="off" data-post-id="1202" name="post_comment" placeholder="Write a comment...Press Enter to Post" >
                <ul class="list-inline meme-reply hidden">
                  <li><a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
                </ul>
              </form>
            </div>
            <div class="clearfix"></div>
              
          </div><!-- to-comment -->

          <div class="comments post-comments-list"> <!-- comments/main-comment  -->
                      </div><!-- comments/main-comment  -->            
        </div>        
      </div><!-- /comments-section -->
      </div>


  
  <!-- Modal Ends here -->
    <a class="jscroll-next hidden" href="https://socialite.laravelguru.com/ajax/get-more-posts?page=2&amp;username=bootstrapguru">get more posts</a>
  
  
					
	 																		
	 										<div class="panel panel-default panel-post animated" id="post1201">
  <div class="panel-heading no-bg">
    <div class="post-author">
      <div class="post-options">
        <ul class="list-inline no-margin">
          <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
                            <li class="main-link">
                <a href="#" data-post-id="1201" class="notify-user unnotify">
                  <i class="fa  fa-bell-slash" aria-hidden="true"></i>stop notifications
                  <span class="small-text">You will not be notified</span>
                </a>
              </li>
              <li class="main-link hidden">
                <a href="#" data-post-id="1201" class="notify-user notify">
                  <i class="fa fa-bell" aria-hidden="true"></i>get notifications
                  <span class="small-text">You will be notified for likes,comments and shares</span>
                </a>
              </li>
                            
                            <li class="main-link">
                <a href="#" data-post-id="1201" class="edit-post">
                  <i class="fa fa-edit" aria-hidden="true"></i>Edit
                  <span class="small-text">You can edit your post</span>
                </a>
              </li>
              
                            <li class="main-link">
                <a href="#" class="delete-post" data-post-id="1201">
                  <i class="fa fa-trash" aria-hidden="true"></i>delete
                  <span class="small-text">This post will be deleted</span>
                </a>
              </li>
              
                            
            </ul>
          </li>
        </ul>
      </div>
      <div class="user-avatar">
        <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
      </div>
      <div class="user-post-details">
        <ul class="list-unstyled no-margin">
          <li>
            <a href="#" class="user-name user">Admin</a>
                      </li>
          <li>
            <time class="post-time timeago" datetime="2016-09-02 04:09:13" title="2016-09-02 04:09:13">
              2016-09-02 04:09:13
            </time>
                      </ul>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="text-wrapper">
        <p>Hello!</p>
        <div class="post-image-holder  ">
                  </div>
      </div>
                  <ul class="actions-count list-inline">
        
                
                
                

      </ul>
    </div>

    
    <div class="panel-footer socialite">
      <ul class="list-inline footer-list">
                
          <li><a href="#" class="like-post like-1201" data-post-id="1201"><i class="fa fa-thumbs-o-up"></i>like</a></li>

          <li class="hidden"><a href="#" class="like-post unlike-1201" data-post-id="1201"><i class="fa fa-thumbs-o-down"></i></i>unlike</a></li>
                <li><a href="#" class="show-comments"><i class="fa fa-comment-o"></i>comment</a></li>

                
      </ul>
    </div>

          <div class="comments-section all_comments" style="display:none">
        <div class="comments-wrapper">         
          <div class="to-comment">  <!-- to-comment -->
                        <div class="commenter-avatar">
              <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
            </div>
            <div class="comment-textfield">
              <form action="#" class="comment-form">
                <input class="form-control post-comment"  autocomplete="off" data-post-id="1201" name="post_comment" placeholder="Write a comment...Press Enter to Post" >
                <ul class="list-inline meme-reply hidden">
                  <li><a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
                </ul>
              </form>
            </div>
            <div class="clearfix"></div>
              
          </div><!-- to-comment -->

          <div class="comments post-comments-list"> <!-- comments/main-comment  -->
                      </div><!-- comments/main-comment  -->            
        </div>        
      </div><!-- /comments-section -->
      </div>


  
  <!-- Modal Ends here -->
    <a class="jscroll-next hidden" href="https://socialite.laravelguru.com/ajax/get-more-posts?page=2&amp;username=bootstrapguru">get more posts</a>
  
  
					
	 																		
	 										<div class="panel panel-default panel-post animated" id="post1200">
  <div class="panel-heading no-bg">
    <div class="post-author">
      <div class="post-options">
        <ul class="list-inline no-margin">
          <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
                            <li class="main-link">
                <a href="#" data-post-id="1200" class="notify-user unnotify">
                  <i class="fa  fa-bell-slash" aria-hidden="true"></i>stop notifications
                  <span class="small-text">You will not be notified</span>
                </a>
              </li>
              <li class="main-link hidden">
                <a href="#" data-post-id="1200" class="notify-user notify">
                  <i class="fa fa-bell" aria-hidden="true"></i>get notifications
                  <span class="small-text">You will be notified for likes,comments and shares</span>
                </a>
              </li>
                            
                            <li class="main-link">
                <a href="#" data-post-id="1200" class="edit-post">
                  <i class="fa fa-edit" aria-hidden="true"></i>Edit
                  <span class="small-text">You can edit your post</span>
                </a>
              </li>
              
                            <li class="main-link">
                <a href="#" class="delete-post" data-post-id="1200">
                  <i class="fa fa-trash" aria-hidden="true"></i>delete
                  <span class="small-text">This post will be deleted</span>
                </a>
              </li>
              
                            
            </ul>
          </li>
        </ul>
      </div>
      <div class="user-avatar">
        <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
      </div>
      <div class="user-post-details">
        <ul class="list-unstyled no-margin">
          <li>
            <a href="#" class="user-name user">Admin</a>
                      </li>
          <li>
            <time class="post-time timeago" datetime="2016-09-02 03:47:41" title="2016-09-02 03:47:41">
              2016-09-02 03:47:41
            </time>
                      </ul>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="text-wrapper">
        <p>zcxcxzczx</p>
        <div class="post-image-holder  ">
                  </div>
      </div>
                  <ul class="actions-count list-inline">
        
                
                
                

      </ul>
    </div>

    
    <div class="panel-footer socialite">
      <ul class="list-inline footer-list">
                
          <li><a href="#" class="like-post like-1200" data-post-id="1200"><i class="fa fa-thumbs-o-up"></i>like</a></li>

          <li class="hidden"><a href="#" class="like-post unlike-1200" data-post-id="1200"><i class="fa fa-thumbs-o-down"></i></i>unlike</a></li>
                <li><a href="#" class="show-comments"><i class="fa fa-comment-o"></i>comment</a></li>

                
      </ul>
    </div>

          <div class="comments-section all_comments" style="display:none">
        <div class="comments-wrapper">         
          <div class="to-comment">  <!-- to-comment -->
                        <div class="commenter-avatar">
              <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
            </div>
            <div class="comment-textfield">
              <form action="#" class="comment-form">
                <input class="form-control post-comment"  autocomplete="off" data-post-id="1200" name="post_comment" placeholder="Write a comment...Press Enter to Post" >
                <ul class="list-inline meme-reply hidden">
                  <li><a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
                </ul>
              </form>
            </div>
            <div class="clearfix"></div>
              
          </div><!-- to-comment -->

          <div class="comments post-comments-list"> <!-- comments/main-comment  -->
                      </div><!-- comments/main-comment  -->            
        </div>        
      </div><!-- /comments-section -->
      </div>


  
  <!-- Modal Ends here -->
    <a class="jscroll-next hidden" href="https://socialite.laravelguru.com/ajax/get-more-posts?page=2&amp;username=bootstrapguru">get more posts</a>
  
  
					
	 																		
	 										<div class="panel panel-default panel-post animated" id="post1199">
  <div class="panel-heading no-bg">
    <div class="post-author">
      <div class="post-options">
        <ul class="list-inline no-margin">
          <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
                            <li class="main-link">
                <a href="#" data-post-id="1199" class="notify-user unnotify">
                  <i class="fa  fa-bell-slash" aria-hidden="true"></i>stop notifications
                  <span class="small-text">You will not be notified</span>
                </a>
              </li>
              <li class="main-link hidden">
                <a href="#" data-post-id="1199" class="notify-user notify">
                  <i class="fa fa-bell" aria-hidden="true"></i>get notifications
                  <span class="small-text">You will be notified for likes,comments and shares</span>
                </a>
              </li>
                            
                            <li class="main-link">
                <a href="#" data-post-id="1199" class="edit-post">
                  <i class="fa fa-edit" aria-hidden="true"></i>Edit
                  <span class="small-text">You can edit your post</span>
                </a>
              </li>
              
                            <li class="main-link">
                <a href="#" class="delete-post" data-post-id="1199">
                  <i class="fa fa-trash" aria-hidden="true"></i>delete
                  <span class="small-text">This post will be deleted</span>
                </a>
              </li>
              
                            
            </ul>
          </li>
        </ul>
      </div>
      <div class="user-avatar">
        <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
      </div>
      <div class="user-post-details">
        <ul class="list-unstyled no-margin">
          <li>
            <a href="#" class="user-name user">Admin</a>
                      </li>
          <li>
            <time class="post-time timeago" datetime="2016-09-02 03:38:29" title="2016-09-02 03:38:29">
              2016-09-02 03:38:29
            </time>
                      </ul>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="text-wrapper">
        <p>test</p>
        <div class="post-image-holder   single-image ">
                    <a href="https://socialite.laravelguru.com/user/gallery/2016-09-02-03-38-29thumbnail-01.jpg" data-lightbox="imageGallery.1199" ><img src="https://socialite.laravelguru.com/user/gallery/2016-09-02-03-38-29thumbnail-01.jpg"  title="Admin" alt="Admin"></a>
                  </div>
      </div>
                  <ul class="actions-count list-inline">
        
                
                
                

      </ul>
    </div>

    
    <div class="panel-footer socialite">
      <ul class="list-inline footer-list">
                
          <li><a href="#" class="like-post like-1199" data-post-id="1199"><i class="fa fa-thumbs-o-up"></i>like</a></li>

          <li class="hidden"><a href="#" class="like-post unlike-1199" data-post-id="1199"><i class="fa fa-thumbs-o-down"></i></i>unlike</a></li>
                <li><a href="#" class="show-comments"><i class="fa fa-comment-o"></i>comment</a></li>

                
      </ul>
    </div>

          <div class="comments-section all_comments" style="display:none">
        <div class="comments-wrapper">         
          <div class="to-comment">  <!-- to-comment -->
                        <div class="commenter-avatar">
              <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
            </div>
            <div class="comment-textfield">
              <form action="#" class="comment-form">
                <input class="form-control post-comment"  autocomplete="off" data-post-id="1199" name="post_comment" placeholder="Write a comment...Press Enter to Post" >
                <ul class="list-inline meme-reply hidden">
                  <li><a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
                </ul>
              </form>
            </div>
            <div class="clearfix"></div>
              
          </div><!-- to-comment -->

          <div class="comments post-comments-list"> <!-- comments/main-comment  -->
                      </div><!-- comments/main-comment  -->            
        </div>        
      </div><!-- /comments-section -->
      </div>


  
  <!-- Modal Ends here -->
    <a class="jscroll-next hidden" href="https://socialite.laravelguru.com/ajax/get-more-posts?page=2&amp;username=bootstrapguru">get more posts</a>
  
  
					
	 																		
	 										<div class="panel panel-default panel-post animated" id="post1198">
  <div class="panel-heading no-bg">
    <div class="post-author">
      <div class="post-options">
        <ul class="list-inline no-margin">
          <li class="dropdown"><a href="#" class="dropdown-togle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
                            <li class="main-link">
                <a href="#" data-post-id="1198" class="notify-user unnotify">
                  <i class="fa  fa-bell-slash" aria-hidden="true"></i>stop notifications
                  <span class="small-text">You will not be notified</span>
                </a>
              </li>
              <li class="main-link hidden">
                <a href="#" data-post-id="1198" class="notify-user notify">
                  <i class="fa fa-bell" aria-hidden="true"></i>get notifications
                  <span class="small-text">You will be notified for likes,comments and shares</span>
                </a>
              </li>
                            
                            <li class="main-link">
                <a href="#" data-post-id="1198" class="edit-post">
                  <i class="fa fa-edit" aria-hidden="true"></i>Edit
                  <span class="small-text">You can edit your post</span>
                </a>
              </li>
              
                            <li class="main-link">
                <a href="#" class="delete-post" data-post-id="1198">
                  <i class="fa fa-trash" aria-hidden="true"></i>delete
                  <span class="small-text">This post will be deleted</span>
                </a>
              </li>
              
                            
            </ul>
          </li>
        </ul>
      </div>
      <div class="user-avatar">
        <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
      </div>
      <div class="user-post-details">
        <ul class="list-unstyled no-margin">
          <li>
            <a href="#" class="user-name user">Admin</a>
                      </li>
          <li>
            <time class="post-time timeago" datetime="2016-09-02 03:13:32" title="2016-09-02 03:13:32">
              2016-09-02 03:13:32
            </time>
                      </ul>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="text-wrapper">
        <p>;)
</p>
        <div class="post-image-holder  ">
                  </div>
      </div>
                  <ul class="actions-count list-inline">
        
                
                <li>
          <a href="#" class="show-all-comments"><span class="count-circle"><i class="fa fa-comment"></i></span>2 comments</a>
        </li>
                
                

      </ul>
    </div>

    
    <div class="panel-footer socialite">
      <ul class="list-inline footer-list">
                
          <li><a href="#" class="like-post like-1198" data-post-id="1198"><i class="fa fa-thumbs-o-up"></i>like</a></li>

          <li class="hidden"><a href="#" class="like-post unlike-1198" data-post-id="1198"><i class="fa fa-thumbs-o-down"></i></i>unlike</a></li>
                <li><a href="#" class="show-comments"><i class="fa fa-comment-o"></i>comment</a></li>

                
      </ul>
    </div>

          <div class="comments-section all_comments" style="display:none">
        <div class="comments-wrapper">         
          <div class="to-comment">  <!-- to-comment -->
                        <div class="commenter-avatar">
              <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin"></a>
            </div>
            <div class="comment-textfield">
              <form action="#" class="comment-form">
                <input class="form-control post-comment"  autocomplete="off" data-post-id="1198" name="post_comment" placeholder="Write a comment...Press Enter to Post" >
                <ul class="list-inline meme-reply hidden">
                  <li><a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
                </ul>
              </form>
            </div>
            <div class="clearfix"></div>
              
          </div><!-- to-comment -->

          <div class="comments post-comments-list"> <!-- comments/main-comment  -->
                                    <ul class="list-unstyled main-comment comment522 " id="comment522">    
    <li> 
      <div class="comments delete_comment_list"> <!-- main-comment -->
        <div class="commenter-avatar">
          <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" title="Admin" alt="Admin"></a>
        </div>
        <div class="comments-list">
          <div class="commenter">
                        <a href="#" class="delete-comment delete_comment" data-commentdelete-id="522"><i class="fa fa-times"></i></a>
                        <div class="commenter-name">
              <a href="#">Admin</a><span class="comment-description">khjh</span>
            </div>
            <ul class="list-inline comment-options">

                            <li><a href="#" class="text-capitalize like-comment like" data-comment-id="522">like</a></li>
              <li class="hidden"><a href="#" class="text-capitalize like-comment unlike" data-comment-id="522">unlike</a></li>
                            <li>.</li>
              <li><a href="#" class="show-comment-reply">reply</a></li>    
              <li>.</li>
                            <li><a href="#" class="show-likes like3-522"><i class="fa fa-thumbs-up"></i>0</a></li>
              <li class="show-likes like4-522 hidden"></li>
                            <li>.</li>
              <li>
                <time class="post-time timeago" datetime="2016-09-02 03:23:09" title="2016-09-02 03:23:09">2016-09-02 03:23:09</time>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <li>
              <div class="to-comment comment-reply" style="display:none" >  <!-- to-comment -->
        <div class="commenter-avatar">
          <img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin">
        </div>
        <div class="comment-textfield">
          <form action="#" class="comment-form">
            <input class="form-control post-comment" autocomplete="off" data-post-id="1198" data-comment-id="522" name="post_comment" placeholder="Write a comment...Press Enter to Post" rows="1">
          </form>
        </div>
        <div class="clearfix"></div>
      </div><!-- to-comment -->
       
    </li>
        
  </ul>
</li><!-- replys/sub-comment -->
                        <ul class="list-unstyled main-comment comment521 " id="comment521">    
    <li> 
      <div class="comments delete_comment_list"> <!-- main-comment -->
        <div class="commenter-avatar">
          <a href="#"><img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" title="Admin" alt="Admin"></a>
        </div>
        <div class="comments-list">
          <div class="commenter">
                        <a href="#" class="delete-comment delete_comment" data-commentdelete-id="521"><i class="fa fa-times"></i></a>
                        <div class="commenter-name">
              <a href="#">Admin</a><span class="comment-description">khjh</span>
            </div>
            <ul class="list-inline comment-options">

                            <li><a href="#" class="text-capitalize like-comment like" data-comment-id="521">like</a></li>
              <li class="hidden"><a href="#" class="text-capitalize like-comment unlike" data-comment-id="521">unlike</a></li>
                            <li>.</li>
              <li><a href="#" class="show-comment-reply">reply</a></li>    
              <li>.</li>
                            <li><a href="#" class="show-likes like3-521"><i class="fa fa-thumbs-up"></i>0</a></li>
              <li class="show-likes like4-521 hidden"></li>
                            <li>.</li>
              <li>
                <time class="post-time timeago" datetime="2016-09-02 03:23:08" title="2016-09-02 03:23:08">2016-09-02 03:23:08</time>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <li>
              <div class="to-comment comment-reply" style="display:none" >  <!-- to-comment -->
        <div class="commenter-avatar">
          <img src="https://socialite.laravelguru.com/user/avatar/2016-08-26-14-42-38despicable-me-2-evil-minion-wallpaper.jpg" alt="Admin" title="Admin">
        </div>
        <div class="comment-textfield">
          <form action="#" class="comment-form">
            <input class="form-control post-comment" autocomplete="off" data-post-id="1198" data-comment-id="521" name="post_comment" placeholder="Write a comment...Press Enter to Post" rows="1">
          </form>
        </div>
        <div class="clearfix"></div>
      </div><!-- to-comment -->
       
    </li>
        
  </ul>
</li><!-- replys/sub-comment -->
                                  </div><!-- comments/main-comment  -->            
        </div>        
      </div><!-- /comments-section -->
      </div>


  
  <!-- Modal Ends here -->
    <a class="jscroll-next hidden" href="https://socialite.laravelguru.com/ajax/get-more-posts?page=2&amp;username=bootstrapguru">get more posts</a>
  
  
					
	 										 																							</div>
						</div>
					</div>
				</div><!-- /row -->
			</div><!-- /col-md-10 -->

			<div class="col-md-2">
				<img src="http://placehold.it/190x200?text=Ad+Block"><br><br>
	<img src="http://placehold.it/190x200?text=Ad+Block">

			</div>

		</div><!-- /row -->
	</div>

        </div>

        <!-- right-sidebar -->
<div id="chatBoxes" v-cloak>
	<div class="chat-list">
		<div class="left-sidebar socialite">
			<ul class="list-group following-group scrollable smooth-scroll">
				<li class="list-group-item group-heading">following
					<div class="dropdown btn-setting">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i>
							<ul class="dropdown-menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li><a href="#">Separated link</a></li>
							</ul>
						</div>
					</li>
					<li class="list-group-item" v-for="conversation in conversations.data">
						<a href="#" @click.prevent="showChatBox(conversation)">
							<div class="media">
								<div class="media-left">
									<img v-bind:src="conversation.user.avatar" alt="images">
								</div>
								<div class="media-body">
									<h4 class="media-heading">{{ conversation.user.name }}</h4>
									<span class="pull-right active-ago" v-if="message">
										<time class="microtime" datetime="{{ message.created_at }}" title="{{ message.created_at }}">
                                            {{ message.created_at }}
                                        </time>
									</span>
								</div>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<!--/right-sidebar-->
		<div class="chatters" id="chatters">
						<div class="chat-box" v-bind:class="[chatBox.minimised ? 'chat-box-small' : '',  ]" v-for="chatBox in chatBoxes">
				<div class="chat-box-header">
					<span class="pull-left">
						<a href="#">{{ chatBox.user.name }}</a>
					</span>
					<ul class="list-inline pull-right">
						<li class="minimize-chatbox"><a href="#"><i class="fa fa-minus" @click.prevent="chatBox.minimised ? chatBox.minimised=false : chatBox.minimised=true" aria-hidden="true"></i></a></li>
						<li class="close-chatbox"><a href="#" @click.prevent="chatBoxes.$remove(chatBox)" ><i class="fa fa-times" aria-hidden="true"></i></a></li>
					</ul>
				</div>
				<div class="chat-conversation scrollable smooth-scroll">
					<ul class="list-unstyled chat-conversation-list">
						<li class="message-conversation" v-bind:class="[(1==message.user.id) ? 'current-user' : '',  ]" v-for="message in chatBox.conversationMessages.data">
							<div class="media">
								<div class="media-left">
									<a href="#">
										<img v-bind:src="message.user.avatar" alt="images">
									</a>
								</div>
								<div class="media-body ">
									<p class="post-text">
										{{ message.body }}
									</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="message-input">
					<fieldset class="form-group">
						<input class="form-control" v-model="chatBox.newMessage" v-on:keyup.enter="postMessage(chatBox)" id="exampleTextarea" >
					</fieldset>
					<!-- <ul class="list-inline">this fields are hidden because in dev 1.0 we dont use this fuctionality ,if we enable this the height of chat list to be increased
						<li><a href="#"><i class="fa fa-camera-retro" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
					</ul> -->
				</div>
			</div>
					</div>
	</div>

	
        
        <!-- Modal starts here-->
<div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
    <div class="modal-dialog modal-likes" role="document">
        <div class="modal-content">
        	<i class="fa fa-spinner fa-spin"></i>
        </div>
    </div>
</div>
<div class="col-md-12">
	<div class="footer-description">
		<div class="socialite-terms text-center">
							<a href="https://socialite.laravelguru.com/contact">Contact</a> - 
				<a href="#/create-page">Create page</a> - 
				<a href="#/create-group">Create group</a>
										- <a href="https://socialite.laravelguru.com/page/about">about</a>		        
		    				- <a href="https://socialite.laravelguru.com/page/privacy">privacy</a>		        
		    				- <a href="https://socialite.laravelguru.com/page/disclaimer">disclaimer</a>		        
		    				- <a href="https://socialite.laravelguru.com/page/terms">terms</a>		        
		    	
		    <a href="https://socialite.laravelguru.com/contact"> - Contact</a>
		</div>
		<div class="socialite-terms text-center">
			Available languages <span>:</span>
							
				English - 
				
				French - 
				
				Spanish - 
				
				Italian - 
				
				Portuguese - 
				
				Russia - 
				
				Japanese - 
				
				Dutch - 
				
				Chines - 
				
				Hindi - 
						
		</div>
		<div class="socialite-terms text-center">
			Copyright &copy; 2016 Socialite. All Rights Reserved
		</div>
	</div>
</div>



        <script>
                      var pusherConfig = {
                token: "G7uoecvaZfMJbqosWslbOsVv6P3mW8hDkCGlR7xR",
                PUSHER_KEY: "356db902ba009eec38a7"
            };
       </script>

        <script src="https://socialite.laravelguru.com/themes/default/assets/js/lightbox.min.js"></script>
<script src="https://socialite.laravelguru.com/themes/default/assets/js/notifications.js"></script>
<script src="https://socialite.laravelguru.com/themes/default/assets/js/chatboxes.js"></script>
<script src="https://socialite.laravelguru.com/themes/default/assets/js/app.js"></script>

        
    </body>
</html>
