<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf_token" content="G7uoecvaZfMJbqosWslbOsVv6P3mW8hDkCGlR7xR"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" />
        <meta name="keywords" content="">
        <meta name="description" content="{{ $description }}">
        <link rel="icon" type="image/x-icon" href="{{ URL::asset('/images/favicon.png') }}">
		<title>{{ $title }}</title>
        <link media="all" type="text/css" rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
        <link media="all" type="text/css" rel="stylesheet" href="{{ URL::asset('css/pray.css') }}">
        <script type="text/javascript">
        function SP_source() {
          return "htpp://54.152.77.34/";
        }
        var base_url = "http://54.152.77.34/";
        var theme_url = "http://54.152.77.34/";
        var current_username = "{{ Auth::user()->name }}";
        </script>
        <script src="{{ URL::asset('js/main.js') }}"></script>
        @yield('css')
        	<style>	
        		.mini-profile .background .avatar-img img{
        			border:none !important;
        		}
        		a.searchlink, a.searchlink:hover{
        			text-decoration: none !important;
        		}
        		.nav-link{
        			color:white;
        			font-size: 16px;
        			padding: 6px !important;
        		}
        	</style>
    </head>
    <body>
        <nav class="navbar socialite navbar-default no-bg">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-4" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand socialite" href="{{ URL('/feed') }}">
						<img class="socialite-logo" src="{{ URL::asset('/frontend/images/branding.png') }}" alt="PrayVine" title="PrayVine" style="padding:8px;">
					</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-4">
					<div>
				<form class="navbar-form navbar-left form-left" role="search" style="max-width: 500px;">
					<div class="input-group no-margin">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
					</span>
					<div class="selectize-control form-control multi">
						<div class="selectize-input items not-full has-options focus input-active dropdown-active">
							<input type="text" autocomplete="off" tabindex="" id="navbar-search" placeholder="Search for people, trends, pages and groups" style="width: 281px; opacity: 1; position: relative; left: 0px;">
						</div>
						<div class="selectize-dropdown multi form-control" id="searches" style="display: none; width: 360px; top: 30px; left: 0px; visibility: visible;">
							<div class="selectize-dropdown-content">

							</div>
						</div>
					</div><!-- /input-group -->
				</form>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<ul class="nav navbar-nav navbar-right" id="navbar-right">

						<li class="dropdown user-image socialite">
							<a href='/profile/{{ App\Profiles::where("user","=",Auth::user()->id)->value("username")  }}' class="dropdown-toggle no-padding" role="button" aria-haspopup="true" aria-expanded="false">
								<img src='@if(Auth::user()->type > 0) /uploads/pics/{{ App\Profiles::where("user","=",Auth::user()->id)->value("pic") }} @else /images/d.jpg @endif' alt="Admin" class="img-radius img-30" title="Admin">
								<span class="user-name">{{ Auth::user()->name }}</span></a>
								<ul class="dropdown-menu">
									
										<li class=""><a href="@if(Auth::user()->type > 0){{ URL('/profile/' . App\Profiles::where('user','=',Auth::user()->id)->value('username')) }} @else # @endif"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>

									<li class="">
										<a href="{{ URL('/groups') }}"><i class="fa fa-bullseye" aria-hidden="true"></i>Prayer Circles
										</a>
									</li>
								</ul>
							</li>
							<li>
					<ul class="list-inline notification-list">
						<li class="dropdown message notification">
							<a href="https://payments.paysimple.com/Buyer/CheckOutFormPay/W0-l9FxrpeWVa6gK1VLDqCY5ja0-" data-toggle="dropdown" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false" target="_blank">
								<i class="fa fa-dollar" aria-hidden="true">
																	</i>
								<span class="small-screen">Donate</span>
							</a>
							<div class="dropdown-menu">
								<div class="dropdown-menu-header">
									<span class="pull-left">Notifications</span>
									
									<div class="clearfix"></div>
								</div>
								<ul class="list-unstyled dropdown-messages-list scrollable" data-type="notifications">
								</ul>
								<div class="dropdown-menu-footer"><br>
									
								</div>
							</div>
						</li>
						<li class="dropdown message">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								<i class="fa fa-unlock" aria-hidden="true"></i>
								<span class="small-screen">Log Out</span>
							</a>
							<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
							<div class="dropdown-menu">
								<div class="dropdown-menu-header">
									<span class="pull-left">Messages</span>
									<div class="clearfix"></div>
								</div>
								<div class="no-messages hidden">
									<i class="fa fa-envelope-o" aria-hidden="true"></i>
									<p>You don't have any messages</p>
								</div>
								<ul class="list-unstyled dropdown-messages-list scrollable" data-type="messages">
									
										
									</ul>
									<div class="dropdown-menu-footer">
										<a href="https://socialite.laravelguru.com/messages">See all</a>
									</div>
								</div>
							</li>
							<li class="smallscreen-message">
								<a href="{{ URL('/inbox') }}">
									<i class="fa fa-envelope-o" aria-hidden="true">
										
									</i>
									<span class="small-screen">My Inbox</span>
								</a>
							</li>
							<li class="chat-list-toggle">
								<a href="{{ URL('/inbox') }}"><i class="fa fa-envelope-o" aria-hidden="true"></i><span class="small-screen">chat-list</span></a>
							</li>
						</ul>
					</li>
			            </ul>
			            
			        </div><!-- /.navbar-collapse -->
			    </div><!-- /.container-fluid -->
			</nav>
		@yield('aside')
        <div class="main-content">
			<div class="container">
				<div class="row">
					@yield('content')
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="footer-description">
				<div class="socialite-terms text-center">
					<a href="#">Contact</a> - 
					<a href="#">Create page</a> - 
					<a href="#">Create group</a> -
					<a href="#">about</a> -       
			    	<a href="#">privacy</a> -        
			    	<a href="#">disclaimer</a> -	        
			    	<a href="#">terms</a>		        
					<a href="#"> - Contact</a>
				</div>
				<div class="socialite-terms text-center">
					Copyright &copy; 2016 PrayVine. All Rights Reserved
				</div>
			</div>
		</div>
		<script src="{{ URL::asset('js/app.js') }}"></script>
	    <script src="{{ URL::asset('js/lightbox.min.js') }}"></script>
		<script> var SessionToken = "{{ csrf_token() }}"; </script>
		@yield('js')
	    <script src="{{ URL::asset('js/ajax.js') }}"></script>
		<script src="{{ URL::asset('js/scripts.js') }}"></script>
		<script type="text/javascript">

     var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-41951928-1']);
    _gaq.push(['_trackPageview']);
    
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  
  </script>
    </body>
</html>