
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf_token" content="lhcxfEpMy2Q7hTCjz34DD5BIqGkz7uJuP7Lzrlca"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
        <meta property="og:image" content="https://socialite.laravelguru.com/setting/logo.jpg" />
        <meta property="og:title" content="Socialite" />
        <meta property="og:type" content="Social Network" />
        <meta name="keywords" content="facebook clone, laravel, live chat, message, news feed, php social network, php social platform, php socialite, post, social, social network, social networking, social platform, social script, socialite">
        <meta name="description" content="">
        <link rel="icon" type="image/x-icon" href="{{ URL::asset('/images/favicon.png') }}">
        
        <title>{{ $title }}</title>
        <link media="all" type="text/css" rel="stylesheet" href="{{ URL::asset('css/style.css') }}">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="https://socialite.laravelguru.com/themes/default/assets/js/main-9fd1c6f9d5.js"></script>

            </head>
    <body>
        <div class="">
            <nav class="navbar socialite navbar-default no-bg">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-4" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand socialite" href="{{ URL('/') }}">
                <img class="socialite-logo" src="{{ URL::asset('/frontend/images/branding.png') }}" alt="PrayVine" title="PrayVine" style="padding:8px;">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-4">
            
                            
                        <ul class="nav navbar-nav navbar-right">
                <li class="logout">
                    <a href="{{ URL('/register') }}" style="color:white !important;"><i class="fa fa-sign-in" aria-hidden="true"></i> Join</a>
                </li>
            </ul>
                        </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>  
        
        </div>
        
        <div class="main-content">
    <div class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            @yield('content')
        </div>
    </div>
</div>
</div>
        
        <!-- Modal starts here-->
<div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
    <div class="modal-dialog modal-likes" role="document">
        <div class="modal-content">
            <i class="fa fa-spinner fa-spin"></i>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="footer-description">
        <div class="socialite-terms text-center">
                            <a href="{{ URL('/') }}">Home</a> 
                            - <a href="{{ URL('/register') }}">Register</a>
                            - <a href="{{ URL('/about') }}">About PrayVine</a>   
                            - <a href="{{ URL('/about') }}">Donate</a>              
                            - <a href="{{ URL('/privacy') }}">Privacy</a>               
                            - <a href="{{ URL('/terms') }}">Terms of Use</a>
        </div>
        <div class="socialite-terms text-center">
            Copyright &copy; 2016 PrayVine. All Rights Reserved. <a href="https://payments.paysimple.com/Buyer/CheckOutFormPay/W0-l9FxrpeWVa6gK1VLDqCY5ja0-" target="_blank">Click here to donate to Prayvine</a> Prayvine is a 501(c)(3) not-for-profit organization; all donations are tax-deductible to the full extent of the law.
        </div>
    </div>
</div>



        <script src="https://socialite.laravelguru.com/themes/default/assets/js/app.js"></script>
        <script type="text/javascript">

     var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-41951928-1']);
    _gaq.push(['_trackPageview']);
    
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  
  </script>

    </body>
</html>
