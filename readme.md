# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).


## App Info

FEATURES



Admin Users:

All the same privileges as lower-access users, and:
Ability to approve, suspend, mark as spam, and deny users access
Access to all groups and user accounts
Site wide Member management
Post, comment, and content management
Clone and render inactive user-type for future use as we differentiate user community applications

Missionary Users: (1)

All the same privileges as lower-access users, and:
Ability to create prayer groups
Ability to create fundraising campaigns and request admin approval within prayer groups
Ability to communicate with the entire prayer group at once
Ability to invite new members to prayer groups
Realtime notifications after auto-batch after 5 a day
Ability to publish media-rich ministry updates/newsletters
Group member management and moderation tools?
Ability to assign moderators to prayer group from within prayer group?
Ability to notify admin of member issues and functionality issues?
Ability to track email invites 
Ability to post Prayer Requests and Responses from within user account or via email 
Clone and render inactive user-type for future use as we differentiate user community applications


“Supporter” Users: (2)

Ability to login upon admin account approval
Add to and change user information
Invite new users for admin approval
Respond to prayer requests
Receive notification of responses to prayer requests
Create prayer requests
Comment on prayer requests
Responsive email engines to assist in email based platform participation
Upload files and photos in comments. What about links? Videos?
Receive site alerts and updates via email and respond via email
Receive prayer circle alerts and updates via email
Clone and render inactive user-type for future use as we differentiate user community applications
Site Additions: 

Revamped homepage for not-logged-in or signed-up visitor
We will use a HomePage Profile fork... Logged in users will see their account page. Logged out users will see the public landing page.
Learn More page for new, not-logged-in or signed-up visitors
About page as an in-app menu bar item 


Notifications: 

Ability for missionary to be reminded to update a prayer request that hasn't been updated in a while
Notifications: Group last post notify (30 days since last post), 
Group notify
Missionary post notify (engagement notifications) 
Posts:

Posts will now have 5 post categories. Those categories will be listed in a 6 tabbed (All and then 1-5. Ian is still deciding on the Category titles. Scroll view and color coded to reflect the category visually. This will take the place of the updates area on the app.

Foundational Framework Elements (will be currently inactive) to be fully developed into features later:

Newsletter framework

Future User-types template framework

Building for App-ification in the future

Text notifications Framework

User Content Delivery Network and file bays

#reporting 

cat 1 = newsfeed post
cat 2 = profile page post
cat 3 = post comment
cat 4 = group post
cat 5 = group post comment