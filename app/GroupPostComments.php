<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupPostComments extends Model
{
    protected $table = 'pv_group_comments';
}