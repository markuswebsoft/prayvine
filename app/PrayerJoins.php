<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrayerJoins extends Model
{
    protected $table = 'pv_prayer_joins';
}