<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupPosts extends Model
{
    protected $table = 'pv_group_posts';
}