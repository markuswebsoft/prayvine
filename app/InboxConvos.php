<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InboxConvos extends Model
{
    protected $table = 'pv_inbox_convos';
}