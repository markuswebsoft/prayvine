<?php

namespace App\Services\Mailing;

use Illuminate\Support\Facades\Facade;

class MailingFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Mailing';
    }
}