<?php
namespace App\Services\Mailing;

use Mailgun\Mailgun;
use App\Prices as Prices;
use App\User as Users;

define("MAILGUN_DOMAIN", "memtrav.com");
define("MAILGUN_APIKEY", "key-b731fee150343d3ab6422851da066c53");

class Mailing
{

	public function sendMail($to, $subject, $message){

		$mg = new Mailgun(MAILGUN_APIKEY);
        $domain = MAILGUN_DOMAIN;

        $mg->sendMessage($domain, array('from'    => 'groups@memtrav.com', 
                                        'to'      => $to, 
                                        'subject' => $subject, 
                                        'text'    => $message));
    }

    public function sendInvite($to, $plan, $access, $from){

        $mg = new Mailgun(MAILGUN_APIKEY);
        $domain = MAILGUN_DOMAIN;

        if($access == 1){ $plan = "Vacationer";}
        if($access == 3){ $plan = "Traveler";}
        if($access == 4){ $plan = "Business Professional";}
        if($access == 5){ $plan = "Enterprise";}

        $invitedBy = json_decode(Users::where("email","=",$from)->pluck("name"))[0];
        $price = json_decode(Prices::where("a","=",$access)->where("l","=",1)->pluck("p"))[0];

        $message = "
Hello! \n
You have been invited by $invitedBy to join Membership Travel. We offer our members unprecedented access to Unpublished Hotel Rates. These rates are so low, we can't legaly show them to you unless you join as a member. Start saving up to 30% on hotel rates by subscibing to our $plan plan starting as low as $" . $price . " per month by visiting https://membershiptravel.com/register \n \n

Yours truly, \n
$invitedBy \n
Membership Travel
        ";

        $mg->sendMessage($domain, array('from'    => "$invitedBy <$from>", 
                                        'to'      => $to, 
                                        'subject' => "You're Invited to Membership Travel", 
                                        'text'    => $message));
    }
}