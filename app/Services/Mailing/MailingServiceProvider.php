<?php
namespace App\Services\Mailing;

use Illuminate\Support\ServiceProvider;

class MailingServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Mailing', function($app) {
            return new Mailing();
        });
    }
}