<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupInbox extends Model
{
    protected $table = 'pv_group_inbox';
}