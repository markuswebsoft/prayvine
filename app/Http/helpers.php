<?php

use App\User as User;
use App\Posts as Posts;
use App\Comments as Comments;
use App\Groups as Groups;
use App\GroupMembers as Members;
use App\GroupPosts as GroupPosts;
use App\GroupPostComments as GroupComments;

function convertHTMLTime($time)
{
    return date("m-d-Y", strtotime($time));
}

function convertTimestamp($stamp)
{
	return date('M j Y g:i A', strtotime($stamp));
}

function getTotalPosts($user){
	$posts = Posts::where('author','=', $user)->count();
	$comments = Comments::where('author','=', $user)->count();
	$groupcomments = GroupComments::where('author','=', $user)->count();
	$total = $posts + $comments;
	return $total;
}

function getTotalGroups($user){
	$mygroups = Groups::where('admin','=', $user)->count();
	$joinedgroups = Members::where('user','=', $user)->count();
	$total = $mygroups + $joinedgroups;
	return $total;
}

function getCat($cat){
	switch ($cat) {
	    case $cat == 1:
	        return "Newsfeed";
	        break;
	    case $cat == 2:
	        return "Profile Page";
	        break;
	    case $cat == 3:
	        return "Post Comment";
	        break;
	    case $cat == 4:
	        return "Group Post";
	        break;
	    case $cat == 5:
	        return "Group Post Comment";
	        break;
	    default:
	}
}

function getPost($cat, $post){
	switch ($cat) {
	    case $cat == 1:
	        return Posts::where("id",$post)->value("content");
	        break;
	    case $cat == 2:
	        return Posts::where("id",$post)->value("content");
	        break;
	    case $cat == 3:
	        return Comments::where("id",$post)->value("comment");
	        break;
	    case $cat == 4:
	        return GroupPosts::where("id",$post)->value("content");
	        break;
	    case $cat == 5:
	        return GroupComments::where("id",$post)->value("comment");
	        break;
	    default:
	}
}

function getAccountType($type){
	switch ($type) {
	    case $type == 1:
	        return "Missionary";
	        break;
	    case $type == 2:
	        return "Supporter";
	        break;
	    default:
	}
}

function sorter($key){
    return function ($a, $b) use ($key) {
        return $b[$key] - $a[$key];
    };
}

function sortHightoLow($key){
    return function ($a, $b) use ($key) {
        return $b[$key] - $a[$key];
    };
}