<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use DB;
use Redirect;
use App\User as User;
use App\Profiles as Profiles;
use App\Groups as Groups;
use App\GrouPosts as Posts;
use App\GroupMembers as Members;
use App\InboxConvos as Inbox;
use Session;

class InboxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $data = array(
            'title' => 'Pray Vine',
            'page' => 'index',
            'description' => 'sample',
            'username' => \App\Profiles::where("user","=",Auth::user()->id)->value("username")
            );

       return view('/inbox/index', $data);
    }

    public function draft(){
        $data = array(
            'title' => 'Pray Vine',
            'page' => 'draft',
            'description' => 'sample',
            'username' => \App\Profiles::where("user","=",Auth::user()->id)->value("username"),
            'to' => null,
            'subject' => null
            );

        return view('/inbox/draft', $data);
    }
    public function draftByUsername($username, $subject){
        $data = array(
            'title' => 'Pray Vine',
            'page' => 'draft',
            'description' => 'sample',
            'username' => \App\Profiles::where("user","=",Auth::user()->id)->value("username"),
            'to' => $username,
            'subject' => $subject
            );

        return view('/inbox/draft', $data);
    }
    public function outbox(){
        $data = array(
            'title' => 'Pray Vine',
            'page' => 'sent',
            'description' => 'sample',
            'username' => \App\Profiles::where("user","=",Auth::user()->id)->value("username")
            );

        return view('/inbox/sent', $data);
    }
    public function invitations(){
        $data = array(
            'title' => 'Pray Vine',
            'page' => 'invites',
            'description' => 'sample',
            'username' => \App\Profiles::where("user","=",Auth::user()->id)->value("username")
            );

        return view('/inbox/invitations', $data);
    }

    public function sendMessage(Request $request){
        $to = $request->to;
        $content = $request->content;
        $author= $request->user()->id;
        $subject = $request->subject;
        if($this->verifyTo($to)){
            $to = Profiles::where('username','=',$to)->value("user");
            DB::table('pv_inbox_convos')->insert(['author' => $author, 'content' => $content, 'to' => $to, 'thread' => $author, 'subject' => $subject]);

            Session::flash('success', 'message sent successfully');
            return Redirect::to("/outbox");
        }
    }
    public function sendReply(Request $request){
        $to = $request->thread;
        $thread = $to;
        $content = $request->content;
        $author= $request->user()->id;
        if($this->verifyRecipient($to)){
            DB::table('pv_inbox_convos')->insert(['author' => $author, 'content' => $content, 'to' => $to, 'thread' => $to]);

            Session::flash('success', 'message sent successfully');
            return Redirect::to("/inbox/read/" . $to . "#convo");
        }
    }

    public function readThread($id){
        if($this->verifyRecipient($id)){
            $this->markRead($id);
            $data = array(
                'title' => 'Pray Vine',
                'page' => 'index',
                'description' => 'sample',
                'thread' => $id,
                'username' => \App\Profiles::where("user","=",Auth::user()->id)->value("username")
                );

            return view('/inbox/read', $data);
        }
    }

    public function decideInvitation($decision, $id){
        if($decision == "accept"){
            echo "joined";
            DB::table('pv_invites')->where('id', $id)->where('user', Auth::user()->id)->orWhere('invitee', Auth::user()->email)->update(array('status' => 1));
        } else {
            DB::table('pv_invites')->where('id', $id)->where('user', Auth::user()->id)->orWhere('invitee', Auth::user()->email)->delete();
        }
    }

    public function verifyTo($username){
        if(Profiles::where('username','=',$username)->count() == 1){
            return true;
        }
        return false;
    }
    public function verifyRecipient($convo){
    	// verify that the message still exists and the user is in the to column
        $to = Auth::user()->id;
        if(Inbox::where('thread', $convo)->where('to', $to)->count() > 0){
            return true;
        }
        return false;
    }
    public function markRead($convo){
        DB::table('pv_inbox_convos')->where('thread', $convo)->where('author','!=', Auth::user()->id)->where('read',0)->update(array('read' => 1));
    }
    public function deleteConversation($convo){
            DB::table('pv_inbox_convos')->where('id', $convo)->where('to', Auth::user()->id)->delete();
    }


}