<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use DB;
use Auth;
use User;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $data = array(
        'title' => 'Pray Vine',
        'page' => 'users',
        'description' => 'sample'
        );
        return view('/admin/index', $data);
    }

    public function groups(){
        $data = array(
        'title' => 'Pray Vine',
        'page' => 'groups',
        'description' => 'sample'
        );
        return view('/admin/groups', $data);
    }

    public function flags(){
        $data = array(
        'title' => 'Pray Vine',
        'page' => 'flags',
        'description' => 'sample'
        );
        return view('/admin/flags', $data);
    }

    public function stats(){
        $data = array(
        'title' => 'Pray Vine',
        'page' => 'stats',
        'description' => 'sample'
        );
        return view('/admin/stats', $data);
    }

    public function activateUser($user){
    	if($this->verifyAdmin(Auth::user()->id)){
    		DB::table('users')->where('id', $user)->update(array('status' => 1));
    	}
    }

    public function suspendUser($user){
    	if($this->verifyAdmin(Auth::user()->id)){
    		DB::table('users')->where('id', $user)->update(array('status' => 3));
    	}
    }

    public function blockUser($user){
    	if($this->verifyAdmin(Auth::user()->id)){
    		DB::table('users')->where('id', $user)->update(array('status' => 4));
    	}
    }

    public function deleteUser($user){
    	if($this->verifyAdmin(Auth::user()->id)){
    		DB::table('users')->where('id', $user)->delete();
    	}
    }

    public function verifyAdmin($user){
    	if(\App\User::where("id","=",$user)->value("admin") == 1){
    		return true;
    	}
    	return false;
    }

}
