<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Flags as Flags;
use Auth;
use DB;

class FlagController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($pid, $cat){
    	if(Flags::where("pid","=", $pid)->where("cat","=", $cat)->where("author","=", Auth::user()->id)->count() == 0){
	    	DB::table('pv_flags')->insert(['author' => Auth::user()->id, 'pid' => $pid, 'cat' => $cat]);
	    	echo "done";
	    }
    }
}
