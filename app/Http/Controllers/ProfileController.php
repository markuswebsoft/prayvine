<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use DB;
use App\User as User;
use App\Profiles as Profiles;
use App\Invites as Invites;
use Redirect;
use Input;
use Validator;
use Session;
use Hash;
use Mail;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function finish(Request $request){
    	$type = $request->type;
    	$pic = $request->pic;
    	$background = $request->background;
    	$bio = $request->bio;
    	$username = $request->username;
    	$dob = $request->dob;
    	$userID = $request->user()->id;
    	$fileName1 = $fileName2 = "";
        $supporter = "";
        $missionary = json_encode(array("missionary" => $request->missionary, "mission" => $request->mission, "communicate" => $request->communicate, "frequency" => $request->frequency));
        $skype = $request->skype;

        // check username availability
        if($this->checkUsername($username) != "ok" || strlen($username) < 6){
            Session::flash('error', 'This username is not available'); 
            return Redirect::back()->withInput();
            exit();
        }
        if($type == 1){
            $status = 1;
        }
        else {
            $status = 2;
        }

        // create new profile
    	DB::table('pv_profiles')->insert([
    		'user' => $userID,
    		'dob' => $dob,
    		'username' => $username,
            'missionary' => $missionary,
            'supporter' => $supporter,
            'skype' => $skype]);

    	// update user type
    	DB::table('users')->where('id', $userID)->update(array('type' => $type, 'status' => $status));

        // check if user was invited
        $invites = Invites::where("invitee","=",Auth::user()->email)->get();
        foreach ($invites as $invite) {
            // change invite status in database
            DB::table('pv_invites')->where('invitee', Auth::user()->email)->where('group', $invite->group)->update(array('status' => 1));
            // enroll user into group
            DB::table('pv_group_joins')->insert([
            'group' => $invite->group,
            'user' => $userID,
            'status' => 1]);
        }
        
        // send welcome email
        $this->sendWelcome(Auth::user()->email, Auth::user()->name, "Welcome to Prayvine");

    	return Redirect::to("/feed");
    }

    public function checkUsername($username){
    	if(Profiles::where('username','=', $username)->count() == 0){
    		return "ok";
    	} else {
    		return "error";
    	}
    }
    public function checkEmail($email){
        if(User::where('email','=', $email)->count() == 0){
            return "ok";
        } else {
            return "error";
        }
    }

    public function getProfile($username){
        $user = Profiles::where("username","=",$username)->value("user");
        $c = Profiles::where("username","=",$username)->count();

        $data = array(
            'title' => 'Pray Vine',
            'page' => 'login',
            'description' => 'sample',
            'profile' => Profiles::where("username","=",$username)->first(),
            'user' => User::where("id","=",$user)->first()
            );
        if($c > 0){
            return view('/profile/index', $data);
        } else {
            return Redirect::to('/feed');
        }
    }

    public function editPic(Request $request){
        $pic = $request->pic;
        $username = \App\Profiles::where("user","=",$request->user()->id)->value("username");

        if(!empty($pic)){
            $destinationPath = 'uploads'; // upload path
            $extension = $request->file('pic')->getClientOriginalExtension(); // getting image extension
            $fileName1 = rand(11111,99999).'.'.$extension; // renameing image
            $path = $request->file('pic')->move(public_path('/uploads/pics'), $fileName1);
            DB::table('pv_profiles')->where('user', $request->user()->id)->update(array('pic' => $fileName1));
            Session::flash('success', 'Profile picture updated successfully');
            return Redirect::to("edit/profile");
        }
    }
    public function editBackground(Request $request){
        $background = $request->background;
        $username = \App\Profiles::where("user","=",$request->user()->id)->value("username");

        if(!empty($background)){
            $destinationPath = 'uploads'; // upload path
            $extension = $request->file('background')->getClientOriginalExtension(); // getting image extension
            $fileName2 = rand(11111,99999).'.'.$extension; // renameing image
            $savePath = public_path() . "/uploads/backgrounds";
            $path = $request->file('background')->move($savePath, $fileName2);
            DB::table('pv_profiles')->where('user', $request->user()->id)->update(array('background' => $fileName2));
            Session::flash('success', 'Background updated successfully');
            return Redirect::to("edit/profile");
        }
    }
    
    public function editGeneral(Request $request){
        $newUsername = $request->username;
        $username = \App\Profiles::where("user","=",$request->user()->id)->value("username");
        $name = $request->name;
        $bio = $request->bio;
        $email = Auth::user()->email;
        $newEmail = $request->email;
        $dob = $request->dob;

        if($email != $newEmail){
            if($this->checkEmail($newEmail) != "ok"){
                Session::flash('error', 'This email is in use'); 
                return Redirect::back()->withInput();
                exit();
            } 
        }

        if($username != $newUsername){
            if($this->checkUsername($newUsername) != "ok" || strlen($username) < 6){
                Session::flash('error', 'This username is not available'); 
                return Redirect::back()->withInput();
                exit();
            } 
        } 

        
        DB::table('pv_profiles')->where('user', $request->user()->id)->update(array('dob' => $dob, 'bio' => $bio));
        DB::table('users')->where('id', $request->user()->id)->update(array('name' => $name, 'email' => $email ));

        Session::flash('success', 'Profile updated successfully');
        return Redirect::to("edit/profile");
    }

    public function editPassword(Request $request){
        $user = User::find($request->user()->id);
        $current_password = $request->current_password;
        if(Hash::check($current_password, $user->password)){
            $hashed = Hash::make($request->new_password);
            DB::table('users')->where('id', $request->user()->id)->update(array('password' => $hashed));
            Session::flash('success', 'Password updated successfully');
            return Redirect::to("edit/profile");
            exit();
        } else {
            Session::flash('error', 'The password you entered does not match your account'); 
            return Redirect::back()->withInput();
            exit();
        }
    }
    public function checkUser($user){
        if($this->checkUsername($user) != "ok"){
            echo "error";
        }
    }

    // mailing routes

    public function sendWelcome($email, $name, $subject){
        $data = array(
            "name" => $name,
            "title" => "Welcome to Prayvine",
            "p1" => "Hi, $name, thank you for joining Prayvine!"
        );
        
        Mail::send(['html' => 'emails.generic'], $data, function($message){
            $message->to(Auth::user()->email, Auth::user()->name)
            ->subject("Welcome to Prayvine");
            $message->from('ian@prayvine.com','Ian from Prayvine');
        });
    }
}
