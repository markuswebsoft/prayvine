<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Session;
use Redirect;
use App\Posts as Posts;
use App\Likes as Likes;
use App\Comments as Comments;

class PostsController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function newPost(Request $request){
        $r = $request->r;
    	$author = $request->user()->id;
    	$content = $request->content;
        $cat = $request->cat;
        $title = $request->title;
        $link = $request->link;
        $group = $request->group;
        $pic = $request->pic;
        $fileName1 = "";

        if(!empty($pic)){
            $destinationPath = 'uploads'; // upload path
            $extension = $request->file('pic')->getClientOriginalExtension(); // getting image extension
            $fileName1 = rand(11111,99999).'.'.$extension; // renameing image
            $path = $request->file('pic')->move(public_path('/uploads/posts'), $fileName1);

        }

    	DB::table('pv_posts')->insert(['author' => $author, 'content' => $content, 'cat' => $cat, 'title' => $title, 'link' => $link, 'pic' => $fileName1, 'group' => $group]);

    	return Redirect::to("/$r");
    }

    public function editPost(Request $request){
    	
        $postID = $request->postID;
        $content = $request->content;
        $author = $request->user()->id;
        $title = $request->title;
        $link = $request->link;
        $r = $request->r;

        if($this->verifyAuthor($author, $postID)){
            DB::table('pv_posts')->where('id', $postID)->update(array('content' => $content, 'title' => $title, 'link' => $link));
        }

        return Redirect::to("/$r");
    }

    public function deletePost(Request $request, $id){
        $author = Auth::user()->id;
        DB::table('pv_posts')->where('id','=',$id)->where('author','=', $author)->delete();
    }

    public function newComment(Request $request){
        $post = $request->post;
        $comment = $request->comment;
        $r = $request->r;
        DB::table('pv_comments')->insert(['author' => $request->user()->id, 'post' => $post, 'comment' => $comment]);
        return Redirect::to("/$r");
    }

    public function likePost(Request $request, $post){
        // check if user has already voted on this post      
        $count = Likes::where('post','=',$post)->where('author','=', $request->user()->id)->count();
        if($count == 0){
            DB::table('pv_likes')->insert(['author' => $request->user()->id, 'post' => $post]);
            $newCount = Likes::where('post','=',$post)->count();
            return json_encode(array("newCount" => $newCount, "newDirection" => "down"));
        } else {
            //DB::table('pv_likes')->where('post','=',$post)->where('author','=', $request->user()->id)->delete();
            //$newCount = Likes::where('post','=',$post)->count();
            //return json_encode(array("newCount" => $newCount, "newDirection" => "up"));
        }
    }

    public function verifyAuthor($author, $post){
        if(Posts::where('author','=',$author)->where('id','=',$post)->count() == 1){
            return true;
        }
        return false;
    }
}
