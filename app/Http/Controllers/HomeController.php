<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\User as User;
use App\GroupMembers as GroupMembers;
use App\Group as Groups;
use App\Posts as Posts;
use Session;
use App\Invites as Invites;
use App\InboxConvos as Inbox;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function feed(){
        $s = $this->validateAccess(Auth::user()->id);
        if(!empty($s)){ Session::flush(); Session::flash('error', "This account is $s."); return Redirect::back()->withInput(); }

        if(Auth::user()->type == 0){

            $data = array(
            'title' => 'Pray Vine',
            'page' => 'login',
            'description' => 'sample'
            );

            return view('/profile/finish', $data);
        } else {
            $feed = array();
            // get all groups that the user belongs to
            $groups = GroupMembers::where('user','=',Auth::user()->id)->where('status',1)->get();

            foreach ($groups as $group) {
                $posts = Posts::where('group', $group->group)->orderBy('id', 'DESC')->get();
                foreach ($posts as $post) {
                    array_push($feed, $post);
                }
            }

            usort($feed, sorter('id'));

            $data = array(
            'title' => 'Pray Vine',
            'page' => 'login',
            'description' => 'sample',
            'category' => "",
            'username' => \App\Profiles::where("user","=",Auth::user()->id)->value("username"),
            'feed' => $feed,
            'unread' => $this->getUnread()
            );

            return view('/feed', $data);
        }
    }
    public function cat($cat){
        $s = $this->validateAccess(Auth::user()->id);
        if(!empty($s)){ Session::flush(); Session::flash('error', "This account has been $s."); return Redirect::back()->withInput(); }

        if(Auth::user()->type == 0){

            $data = array(
            'title' => 'Pray Vine',
            'page' => 'login',
            'description' => 'sample'
            );

            return view('/profile/finish', $data);
        } else {
            $this->validateAccess(Auth::user()->id);
            $data = array(
            'title' => 'Pray Vine',
            'page' => 'login',
            'description' => 'sample',
            'category' => $cat,
            'username' => \App\Profiles::where("user","=",Auth::user()->id)->value("username")
            );

            return view('/feed', $data);
        }
    }
    public function prayers(){
        $s = $this->validateAccess(Auth::user()->id);
        if(!empty($s)){ Session::flush(); Session::flash('error', "This account has been $s."); return Redirect::back()->withInput(); }
        if(Auth::user()->type == 0){

            $data = array(
            'title' => 'Pray Vine',
            'page' => 'home',
            'description' => 'sample'
            );

            return view('/profile/finish', $data);
        } else {
            $this->validateAccess(Auth::user()->id);

             $feed = array();
            // get all groups that the user belongs to
            $groups = GroupMembers::where('user','=',Auth::user()->id)->where('status',1)->get();

            foreach ($groups as $group) {
                $posts = Posts::where('group', $group->group)->orderBy('id', 'DESC')->get();
                foreach ($posts as $post) {
                    array_push($feed, $post);
                }
            }

            usort($feed, sorter('id'));

            $data = array(
            'title' => 'Pray Vine',
            'page' => 'login',
            'description' => 'sample',
            'feed' => $feed,
            'username' => \App\Profiles::where("user","=",Auth::user()->id)->value("username")
            );

            return view('/prayers', $data);
        }
    }

    public function validateAccess($userID){
        $status = User::where("id","=",$userID)->value("status");
        if($status > 1){
            if($status == 2){ $s = "pending admin approval";}
            if($status == 3){ $s = "suspended";}
            if($status == 4){ $s = "banned";}
            return $s;
        }
        return false;
    }

    public function getUnread(){
        $unread = Inbox::where('to',Auth::user()->id)->where('read', 0)->where('author', '!=', Auth::user()->id)->count();
        $invites = Invites::where('invitee', Auth::user()->email)->orWhere('user',Auth::user()->id)->where('status', 0)->count();
        return $unread + $invites;
    }
}
