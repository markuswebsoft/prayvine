<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use DB;
use Redirect;
use App\User as User;
use App\Groups as Groups;
use App\GrouPosts as Posts;
use App\GroupMembers as Members;
use Session;

class GroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        if(Auth::user()->type == 0){

            $data = array(
            'title' => 'Pray Vine',
            'page' => 'home',
            'description' => 'sample'
            );

            return view('/finish', $data);
        } else {

            $data = array(
            'title' => 'Pray Vine',
            'page' => 'login',
            'description' => 'sample',
            'username' => \App\Profiles::where("user","=",Auth::user()->id)->value("username")
            );

            return view('/group/index', $data);
        }
    }

    public function createGroup(Request $request){
    	$name = $request->name;
    	$description = $request->description;
    	$privacy = $request->privacy;
    	$admin = $request->user()->id;
        $inviteToken = str_random(9);
    	DB::table('pv_groups')->insert(['name' => $name, 'admin' => $admin, 'description' => $description, 'privacy' => $privacy, 'admin' => $admin, 'token' => $inviteToken]);
        $group = Groups::where('token',$inviteToken)->value("id");
        DB::table('pv_group_joins')->insert(['user' => $admin, 'group' => $group, 'status' => 1, 'role' => 3]);
    	return Redirect::to("/groups");
    }

    public function getGroup($group){
        $group = Groups::where('id','=', $group)->first();
        $data = array(
            'title' => 'Pray Vine',
            'page' => 'login',
            'description' => 'sample',
            'group' => $group
            );
        return view('/group/profile', $data);
    }

    public function joinGroup(Request $request, $group){
        // validate group ID
        $Group = Groups::where('id','=', $group)->first();
        DB::table('pv_group_joins')->insert(['user' => $request->user()->id, 'group' => $Group->id]);
        return Redirect::to("/groups/". $Group->id);
    }

    public function leaveGroup(Request $request, $group){
        // validate group ID
        $Group = Groups::where('id','=', $group)->first();
        DB::table('pv_group_joins')->where('user',$request->user()->id)->where('group',$Group->id)->delete();
        return Redirect::to("/groups/". $Group->id);
    }

    public function createPost(Request $request){
        $r = $request->r;
        $author = $request->user()->id;
        $content = $request->content;
        $cat = $request->cat;
        $group = $request->group;
        $title = $request->title;
        $link = $request->link;
        $pic = $request->pic;
        $fileName1 = "";
        if(!empty($pic)){
            $destinationPath = 'uploads'; // upload path
            $extension = $request->file('pic')->getClientOriginalExtension(); // getting image extension
            $fileName1 = rand(11111,99999).'.'.$extension; // renameing image
            

        }

        if($this->verifyMember($group) || $this->verifyGroupAdmin($group)){
            $path = $request->file('pic')->move(public_path('/uploads/posts'), $fileName1);
            DB::table('pv_posts')->insert(['author' => $author, 'content' => $content, 'cat' => $cat, 'title' => $title, 'link' => $link, 'pic' => $fileName1, 'group' => $group]);
        }
        return Redirect::to("/$r");
    }

    public function postComment(Request $request){
        $group = $request->group;
        $post = $request->post;
        $comment = $request->comment;
        $r = $request->r;
        if($this->verifyMember($group)){
        DB::table('pv_group_comments')->insert(['author' => $request->user()->id, 'group' => $group, 'post' => $post, 'comment' => $comment]);
        }
        return Redirect::to("/$r");
    }

    public function adminGroup($group){
        $Group = Groups::where('id','=', $group)->first();
        $data = array(
            'title' => 'Pray Vine',
            'page' => 'login',
            'description' => 'sample',
            'group' => Groups::where('id','=', $group)->first()
            );
        return view('/group/profile', $data);
    }

    public function deletePost(Request $request, $id, $group){
        $author = Auth::user()->id;
        DB::table('pv_group_posts')->where('id','=',$id)->where('author','=', $author)->where('group','=',$group)->delete();
    }

    public function likePost(Request $request, $post, $group){
        // check if user has already voted on this post      
        $count = Likes::where('post','=',$post)->where('author','=', $request->user()->id)->where('group','=',$group)->count();
        if($count == 0){
            DB::table('pv_likes')->insert(['author' => $request->user()->id, 'post' => $post]);
            $newCount = Likes::where('post','=',$post)->count();
            return json_encode(array("newCount" => $newCount, "newDirection" => "down"));
        } else {
            DB::table('pv_likes')->where('post','=',$post)->where('author','=', $request->user()->id)->delete();
            $newCount = Likes::where('post','=',$post)->count();
            return json_encode(array("newCount" => $newCount, "newDirection" => "up"));
        }
    }

    public function membersGroup($group){
        $data = array(
        'title' => 'Pray Vine',
        'page' => 'members',
        'description' => "",
        'group' => Groups::where('id','=', $group)->first()
        );
        return view('/group/members', $data);
    }

    public function manageGroup($group){
        if($this->verifyGroupAdmin($group)){
        $data = array(
            'title' => 'Pray Vine',
            'page' => 'settings',
            'description' => "",
            'group' => Groups::where('id','=', $group)->first()
            );
        return view('/group/edit', $data);
        } else {
            return Redirect::to('/groups');
        }
    }

    public function invites($group){
        if($this->verifyGroupAdmin($group)){
        $data = array(
            'title' => 'Pray Vine',
            'page' => 'invites',
            'description' => "",
            'group' => Groups::where('id','=', $group)->first()
            );
        return view('/group/invites', $data);
        } else {
            return Redirect::to('/groups');
        }
    }

    public function changeIcon(Request $request){
        $pic = $request->pic;
        $group = $request->group;

        if($this->verifyGroupAdmin($group)){
            if(!empty($pic)){
                $extension = $request->file('pic')->getClientOriginalExtension(); // getting image extension
                $fileName1 = rand(11111,99999).'.'.$extension; // renameing image
                $path = $request->file('pic')->move(public_path('/uploads/pics'), $fileName1);
                DB::table('pv_groups')->where('id', $group)->update(array('icon' => $fileName1));
                Session::flash('success', 'Prayer circle icon updated successfully');
                return Redirect::to("/edit/group/" . $group);
            }
        }
    }
    public function changeProfile(Request $request){
        $group = $request->group;
        $groupname = $request->groupname;
        $description = $request->description;
        $active = $request->active;

        if($this->verifyGroupAdmin($group)){  
            DB::table('pv_groups')->where('id', $group)->update(array('name' => $groupname, 'description' => $description, 'active' => $active));

            Session::flash('success', 'Prayer circle updated successfully');
            return Redirect::to("/edit/group/" . $group);
        }
    }

    public function decideUser($decide, $user, $group){
        if($this->verifyGroupAdmin($group)){
            if($decide == "approve"){
                DB::table('pv_group_joins')->where('group','=',$group)->where('user','=', $user)->update(array('status' => 1));
            } else {
                echo $decide;
                DB::table('pv_group_joins')->where('group','=',$group)->where('user','=', $user)->delete();
            }
        }
    }
    public function inbox($group){
        if($this->verifyGroupAdmin($group)){
            $data = array(
            'title' => 'Pray Vine',
            'page' => 'inbox',
            'description' => "",
            'group' => Groups::where('id','=', $group)->first()
            );
            return view('/group/inbox', $data);
        } else {
            return Redirect::to('/groups');
        }
    }
    public function sendInvite(Request $request){
        $group = $request->group;
        if($this->verifyGroupAdmin($group)){
            $emails = explode(",", $request->invitees);
            $message = $request->message;
            
            $userID = $request->user()->id;
            foreach ($emails as $to) {
                ///Mailing::sendInvite($to, $plan, $access, $from);
                if(sizeof(\App\Invites::where('invitee','=',$to)->where('group','=',$group)->get()) < 1){
                    if(User::where('email',$to)->count() == 0) {
                        DB::table('pv_invites')->insert(['inviter' => $userID, 'invitee' => $to, 'group' => $group]);
                    } else {
                        $to = User::where('email',$to)->value('id');
                        DB::table('pv_invites')->insert(['inviter' => $userID, 'user' => $to, 'group' => $group]);
                    }
                }
            }
            Session::flash('success', 'Invites to prayer circle were sent successfully');
            return Redirect::to("/invites/group/" . $group);
        }
        else {
            return Redirect::to('/groups');
        }
    }
    public function decideRole($role, $user, $group){
        if($this->verifyGroupAdmin($group)){
                DB::table('pv_group_joins')->where('group','=',$group)->where('user','=', $user)->update(array('role' => $role));
        }
    }

    public function verifyGroupAdmin($group){
        if(Groups::where('id','=',$group)->where('admin','=',Auth::user()->id)->count() == 1){
            return true;
        }
        return false;
    }

    public function verifyMember($group){
        if(Members::where('group',$group)->where('user','=',Auth::user()->id)->where('status','=', 1)->where('role','>=', 1)->count() == 1){
            return true;
        }
        return false;
    }
    public function verifyAdmin($author, $post){
        if(Posts::where('author','=',$author)->where('id','=',$post)->count() == 1){
            return true;
        }
        return false;
    }
}
