<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Groups as Groups;
use App\User as User;
use App\Profiles as Profiles;
use DB;

class SearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($query){
    	$users = User::where('name', 'LIKE', "%$query%")->get();
    	$groups = Groups::where('name', 'LIKE', "%$query%")->get();
        $usersCount = count($users);
        $groupsCount = count($groups);
        $total = $usersCount + $groupsCount;
        if($total > 0 && strlen($query) > 3){
            if($usersCount > 0){
            	echo '<div data-group="user" class="optgroup"><div class="optgroup-header">Users</div></div>';
            	foreach ($users as $user) {
            		$profile = Profiles::where('user','=', $user->id)->first();
            		echo '<div data-value="' . $profile->username . '" data-selectable="" class="media big-search-dropdown"><a class="media-left searchlink" href="/profile/' . $profile->username . '"><img src="/uploads/pics/' . $profile->pic . ' " alt="..."><div class="media-body"><h4 class="media-heading">' . $user->name .  '</h4><p>' . $profile->bio .' </p></a></div></div>';
            	}
            }
            if($groupsCount > 0){
            	echo '<div data-group="group" class="optgroup"><div class="optgroup-header">Groups</div></div>';
            	foreach ($groups as $group) {
            		echo '<div data-value="' . $group->name . '" data-selectable="" class="media big-search-dropdown"><a class="media-left searchlink" href="/groups/' . $group->id . '"><img src="/uploads/pics/' . $group->icon . ' " alt="..."><div class="media-body"><h4 class="media-heading">' . $group->name .  '</h4><p>' . $group->description .' </p></a></div></div>';
            	}
            }
        } else {
            echo '<div data-group="user" class="optgroup"><div class="optgroup-header">No Results</div></div>';
        }
    }
        public function users($query){
        $users = User::where('name', 'LIKE', "%$query%")->get();
        $usersCount = count($users);
        if($usersCount > 0 && strlen($query) >= 6){
            if($usersCount > 0){
                echo '<div data-group="user" class="optgroup"><div class="optgroup-header">Users</div></div>';
                foreach ($users as $user) {
                    $profile = Profiles::where('user','=', $user->id)->first();
                    echo '<div data-value="' . $profile->username . '" data-selectable="' . $user->name . '" class="usersearchresults media big-search-dropdown"><a class="media-left searchlink" href="#"><img src="/uploads/pics/' . $profile->pic . ' " alt="..."><div class="media-body"><h4 class="media-heading">' . $profile->username .  ' (' . $user->name .')</h4></a></div></div>';
                }
            }
        } else {
            echo '<div data-group="user" class="optgroup"><div class="optgroup-header">No Results</div></div>';
        }
    }
}
