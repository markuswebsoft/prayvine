<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Mail;

class MailController extends Controller
{
    public function mailgun(){
        $subject = "This is a test";
    	$data = array(
    		"name" => "cheap",
    		"title" => "this is a sample title",
    		"p1" => "");
    	Mail::send(['html' => 'emails.generic'], $data, function($message){
            $message->to(Auth::user()->email, Auth::user()->name)
            ->subject("This is a Test");
            $message->from('ian@prayvine.com','Ian from Prayvine');
        });
    }
}
