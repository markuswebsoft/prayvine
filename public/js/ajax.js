$( document ).ready(function() {
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': SessionToken
        }
    });
	$(".like-post").click(function(e){
		var id = $(this).attr("data-post-id");
		var likeButton = $(this).find("i");
		$.ajax({
			url: "/like/" + id,
			type: "POST"
		}).done(function(msg){ //on Ajax success
			// msg will be a json object
			var obj = jQuery.parseJSON(msg);
			$(".count-" + id).text(obj.newCount);
			$('#prayModal').modal('hide');
			// new position of icon
		});
	});
	$(".delete-post").click(function(e){
		var id = $(this).attr("data-post-id");
		
	    $.ajax({
	        url: "/post/delete/" + id,
	        type: "POST"
	    }).done(function(msg){ //on Ajax success
	      	$("#c-" + id).remove();
		});
	});
	$(".delete-convo").click(function(e){
		var id = $(this).attr("convo-id");
		
	    $.ajax({
	        url: "/inbox/delete/convo/" + id,
	        type: "DELETE"
	    }).done(function(msg){ //on Ajax success
	      	$("#c-" + id).remove();
		});
	});
	$(".decide-member").click(function(e){
		var group = $(this).attr("data-group");
		var user = $(this).attr("data-group-user");
		var decision = $(this).attr("data-decision");
	    $.ajax({
	        url: "/group/decide/" + decision + "/" + user + "/" + group,
	        type: "POST"
	    }).done(function(msg){ //on Ajax success
	      	$("#u-" + user).remove();
		});
	});
	$(".invite-decide").click(function(){
		var invite = $(this).attr("invite-id");
		var decision = $(this).attr("decide");
		$.ajax({
			url: "/inbox/decide/" + decision + "/" + invite,
			type: "POST"
		}).done(function(msg){ //on Ajax success
			$("#u-" + invite).remove();
		});

	});
	$(".like-post-group").click(function(e){
		var id = $(this).attr("data-post-id");
		var group = $(this).attr("data-group-id");
		var likeButton = $(this).find("i");
		$.ajax({
			url: "/group/like/" + id + "/" + group,
			type: "POST"
		}).done(function(msg){ //on Ajax success
			// msg will be a json object
			var obj = jQuery.parseJSON(msg);
			$(".count-" + id).text(obj.newCount);
			// new position of icon
			if(obj.newDirection == "down"){ 
				$(likeButton).removeClass("fa-thumbs-o-up").addClass("fa-thumbs-up");
				 } else { 
				$(likeButton).removeClass("fa-thumbs-up").addClass("fa-thumbs-o-up");
			}
		});
	});
	$(".delete-post-group").click(function(e){
		var id = $(this).attr("data-post-id");
		var group = $(this).attr("data-group-id");
	    $.ajax({
	        url: "/group/delete/" + id + "/" + group,
	        type: "POST"
	    }).done(function(msg){ //on Ajax success
	      	$("#c-" + id).remove();
		});
	});
	$(".reporting").click(function(e){
		var id = $(this).attr("data-post-id");
		var cat = $(this).attr("data-post-cat");
	    $.ajax({
	        url: "/report/" + id + "/" + cat,
	        type: "POST"
	    }).done(function(msg){ //on Ajax success
	      	alert("This post has been reported");
		});
		$(this).find(".report").addClass("active");
	});
	$("#navbar-search").click(function(){
		if($("#searches >.selectize-dropdown-content").is(":hidden")){
	  		$("#searches > .selectize-dropdown-content").show();
	  		$("#searches").show();
		}
	});

	$( "#navbar-search" ).keyup(function() {
		var query = $(this).val();
		if(query.length > 3){
			$.ajax({
	        url: "/search/" + query,
	        type: "POST"
			    }).done(function(response){ //on Ajax success
			    	$("#searches > .selectize-dropdown-content").empty()
			      	$("#searches > .selectize-dropdown-content").append(response);
			      	$("#searches").show();
				});
		} else {
			$("#searches > .selectize-dropdown-content").empty();
			$("#searches").hide();
		}
	});

	$("#username-search").click(function(){
		$("#users").toggle();
		$("#users > .selectize-dropdown-content").toggle();
	});

	$("body").on("click", ".usersearchresults", function(){
		var username = $(this).attr("data-value");
		var name = $(this).attr("data-selectable");
		$("#to").val(username);
		$("#username-search").val(name);
	});

	$( "#username-search" ).keyup(function() {
		var query = $(this).val();
		if(query.length > 5){
			$.ajax({
	        url: "/searchusers/" + query,
	        type: "POST"
			    }).done(function(response){ //on Ajax success
			    	$("#usersearch .selectize-dropdown-content").empty()
			      	$("#users > .selectize-dropdown-content").append(response);
			      	$("#users > .selectize-dropdown-content").show();
			      	$("#users").show();
				});
		} else {
			$("#usersearch .selectize-dropdown-content").empty();
			$("#users").hide();
		}
	});

	$('.main-content').click(function() {
		if($("#searches > .selectize-dropdown-content").is(":visible")){
	  		$("#searches > .selectize-dropdown-content").hide();
	  		$("#searches").hide();
		}
	  		$("#users > .selectize-dropdown-content").hide();
	  		$("#users").hide();
	});

	$(".checkUsername").blur(function(){
		if($(this).val().length > 5){
			if($(this).val() != myusername){
			$(".er").remove();
			$.ajax({
		        url: "/profile/check/" + $(this).val(),
		        type: "POST"
		    }).done(function(msg){ //on Ajax success
		    	if(msg == "error"){
		      		$(".usercheck").addClass("has-error");
		      		$(".usercheck").removeClass("has-success");
		      		$(".usercheck").prepend("<p class='er er1'>This username is not available</p>");
		      	} else {
		      		$(".usercheck").removeClass("has-error");
		      		$(".usercheck").addClass("has-success");
		      	}
			});
			} else {
				$(".er").remove();
				$(".usercheck").removeClass("has-error");
			}
		} else {
			$(".usercheck").addClass("has-error");
			$(".er").remove();
      		$(".usercheck").removeClass("has-success");
      		$(".usercheck").prepend("<p class='er er2'>Usernames must be at least 6 characters long</p>");
		}
	});
});