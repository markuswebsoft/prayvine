$( document ).ready(function() {
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': SessionToken
	        }
	    });
		$(".status").change(function(e){
			var status = $(this).val();
			var user = $(this).attr("user-id");
			var url;
			if(status == 1){
				url = "/admin/activate/user/" + user;
			}
			if(status == 3){
				url = "/admin/suspend/user/" + user;
			}
			if(status == 4){
				url = "/admin/block/user/" + user;
			}
	      $.ajax({
	        url: url,
	        type: "POST"
		      }).done(function(msg){ //on Ajax success
		    });
		});	
});
$(document).on("click", ".delete", function(e){
			var user = $(this).attr("user-id");
	      $.ajax({
	        url: "/admin/delete/user/" + user,
	        type: "POST"
		      }).done(function(msg){ //on Ajax success
		      	
		    });
		      alert("User deleted successfully");
		      $(this).parent().parent().remove();
		});