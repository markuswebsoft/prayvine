$("body").on("click", ".edit-post", function(){
            var previousPost = $(this).closest(".panel").find(".post-content").text();
            var previousTitle = $(this).closest(".panel").find(".post-title").text();
            var previousLink = $(this).closest(".panel").find(".post-link").attr("href");
            $("#editModal").find("#postID").val($(this).attr("data-post-id"));
            $("#editModal").find("#r").val("feed/#c-" + $(this).attr("data-post-id"));
            $("#editModal").find("#previousPost").text(previousPost);
            $("#editModal").find("#previousTitle").val(previousTitle);
            $("#editModal").find("#previousLink").val(previousLink);
        });
        $("body").on("click", ".pray-post", function(){
            var previousPost = $(this).closest(".panel").find(".post-content").text();
            var previousTitle = $(this).closest(".panel").find(".post-title").text();
            var previousLink = $(this).closest(".panel").find(".post-link").attr("href");
            var previousPic = $(this).closest(".panel").find(".post-pic").attr("src");
            $("#prayModal").find("#postID").val($(this).attr("data-post-id"));
            $("#prayModal").find("button").attr("data-post-id", $(this).attr("data-post-id"));
            $("#prayModal").find("#r").val("feed/#c-" + $(this).attr("data-post-id"));
            $("#prayModal").find("#prayPost").text(previousPost);
            $("#prayModal").find("#prayTitle").text(previousTitle);
            $("#prayModal").find("#previousLink").val(previousLink);
            $("#prayModal").find("#prayPic").attr('src', previousPic);
            if(previousPic != "" && previousPic){
              $("#prayPic").show();
            } else {
              $("#prayPic").hide();
            }
        });

$("body").on("click", ".navbar-toggle", function(){
    if($(this).hasClass("collapsed")){
        $(this).removeClass("collapsed");
        $(".navbar-collapse").addClass("in");
    } else {
        $(".navbar-collapse").removeClass("in");
        $(this).addClass("collapsed");
    }
});